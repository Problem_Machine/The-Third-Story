package;

import com.problemmachine.tools.keyboard.KeyboardTools;
import flash.display.Sprite;
import game.Game;

class Main extends Sprite 
{
	private var mGame:Game;
	
	static private function main():Void
	{
		// only necessary if building into AIR
		flash.Lib.current.stage.align = flash.display.StageAlign.TOP_LEFT;
		flash.Lib.current.stage.scaleMode = flash.display.StageScaleMode.NO_SCALE;
		flash.Lib.current.stage.addChild(new Main());
	}
	
	public function new() 
	{
		super();
		KeyboardTools.init();
		mGame = new Game();
		addChild(mGame);
		
	}
}
