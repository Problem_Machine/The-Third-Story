﻿package game.animation;

import com.problemmachine.tools.math.IntMath;
import com.problemmachine.util.color.colormatrix.ColorMatrix;
import flash.display.Bitmap;
import flash.display.Sprite;
import flash.filters.BitmapFilter;
import game.animation.event.AnimationEvent;
import com.problemmachine.bitmap.bitmapmanager.BitmapManager;
import com.problemmachine.tools.file.FileTools;
import flash.display.BlendMode;
import flash.errors.Error;
import flash.events.EventDispatcher;
import flash.filters.ColorMatrixFilter;
import flash.geom.Point;
import flash.geom.Rectangle;
import haxe.xml.Fast;
import openfl.display.Tile;
import openfl.display.Tilemap;

class Animation extends EventDispatcher
{
	static public var verbose:Bool = false;
	
	private var mFrames:Array<Frame>;
	private var mColorMatrixFilter:ColorMatrixFilter;
	
	public var x:Float;
	public var y:Float;
	public var blendMode:BlendMode;
	public var smoothing:Bool;
	public var sleeping(default, set):Bool;	
	public var ready(get, null):Bool;
	public var complete(default, null):Bool;
	public var currentFrame(default, set):Int;
	public var animationSpeed(default, default):Float;
	public var animationMode(default, default):AnimationMode;
	public var red:Float;
	public var green:Float;
	public var blue:Float;
	public var alpha:Float;
	public var hue(default, set):Float;
	public var saturation(default, set):Float;
	public var brightness(default, set):Float;
	public var scale:Float;
	public var flipHorizontal:Bool;
	public var flipVertical:Bool;
	
	public var width(get, never):Float;
	public var height(get, never):Float;
	public var numFrames(get, never):Int;
	public var radianRotation(get, set):Float;
	public var degreeRotation(get, set):Float;
	public var unitRotation:Float;
	
	private var mCurrentDelay:Float;
	private var mEnteringFrame:Bool;
	
	public function new(startSleeping:Bool = true, ?startMode:AnimationMode, smoothing:Bool = true) 
	{			
		super();
		x = y = 0;
		mFrames = [];
		sleeping = startSleeping;
		currentFrame = 0;
		animationSpeed = 1;
		animationMode = startMode != null ? startMode : AnimationMode.Stop;
		scale = 1;
		unitRotation = 0;
		
		flipHorizontal = false;
		flipVertical = false;
		mCurrentDelay = 0;
		ready = false;
		complete = false;
		mEnteringFrame = false;
		
		mColorMatrixFilter = null;
		red = green = blue = alpha = 1;
		hue = 0;
		saturation = 1;
		brightness = 0;
	}
	
	public static function createFromXML(xml:Xml, startSleeping:Bool = true):Animation
	{
		var ba:Animation = new Animation(startSleeping);
		ba.loadInformationFromXML(xml);
		
		return ba;
	}
	
	@:access(game.animation.Frame.getBitmap)
	@:access(game.animation.Frame.mRegistrationPoints)
	public function toXML():Xml
	{
		var xml:Xml = Xml.parse(
			"<animation>\n" +
			"	<alpha>" + alpha + "</alpha>\n" +
			"	<red>" + red + "</red>\n" +
			"	<green>" + green + "</green>\n" +
			"	<blue>" + blue + "</blue>\n" +
			"	<hue>" + hue + "</hue>\n" +
			"	<saturation>" + saturation + "</saturation>\n" +
			"	<brightness>" + brightness + "</brightness>\n" +
			"	<scale>" + scale + "</scale>\n" +
			"	<rotation>" + unitRotation + "</rotation>\n" +
			"	<animationSpeed>" + animationSpeed + "</animationSpeed>\n" +
			"	<animationMode>" + Std.string(animationMode) + "</animationMode>\n" +
			"	<blendMode>" + Std.string(blendMode).toUpperCase() + "</blendMode>\n" + 
			//For some reason blendMode strings out in lower case even though it needs upper to construct the enum
			"	<flipHorizontal>" + flipHorizontal + "</flipHorizontal>\n" +
			"	<flipVertical>" + flipVertical + "</flipVertical>\n" +
			"	<smoothing>" + smoothing + "</smoothing>\n" +
			"</animation>").firstElement();
		for (f in mFrames)
		{
			var x:Xml = Xml.parse(
				"<frame>\n" +
				"	<assetPath>" + f.assetPath + "</assetPath>\n" +
				"	<delay>" + f.delay + "</delay>\n" +
				"</frame>").firstElement();
			for (i in 0...f.mRegistrationPoints.length)
			{
				if (f.mRegistrationPoints[i] != null)
					x.addChild(Xml.parse(
						"<point>\n" + 
						"	<index>" + Std.string(i) + "</index>\n" +
						"	<x>" + Std.string(f.mRegistrationPoints[i].x) + "</x>\n" +
						"	<y>" + Std.string(f.mRegistrationPoints[i].y) + "</y>\n" +
						"</point>").firstElement());
			}
			if (f.offset.x != 0)
				x.addChild(Xml.parse("<offsetX>" + f.offset.x + "</offsetX>").firstElement());
			if (f.offset.y != 0)
				x.addChild(Xml.parse("<offsetY>" + f.offset.y + "</offsetY>").firstElement());
			// if the asset's null we don't know whether clipRect is non-default or not so always save it
			if (f.assetPath != "")
				if (f.clipRect != null && 
					(f.clipRect.x != 0 || f.clipRect.y != 0 || 
					f.getBitmap() == null || f.clipRect.width != f.getBitmap().width || f.clipRect.height != f.getBitmap().height))
				{
					x.addChild(Xml.parse("<clipRectX>" + f.clipRect.x + "</clipRectX>").firstElement());
					x.addChild(Xml.parse("<clipRectY>" + f.clipRect.y + "</clipRectY>").firstElement());
					x.addChild(Xml.parse("<clipRectWidth>" + f.clipRect.width + "</clipRectWidth>").firstElement());
					x.addChild(Xml.parse("<clipRectHeight>" + f.clipRect.height + "</clipRectHeight>").firstElement());
				}
				
			xml.addChild(x);
		}
		return xml;
	}
	
	public static function createFromPath(path:String, startSleeping:Bool = true):Animation
	{
		var ext:String = path.substr(path.length - 3).toLowerCase();
		if (BitmapManager.isRecognizedType(ext))
		{
			var a:Animation = new Animation(startSleeping, AnimationMode.Stop, true);
			a.addFrame(path, 1);
			return a;
		}
		else
		{
			var a:Animation = new Animation(startSleeping);
			a.loadInformationFromPath(path);
			return a;
		}		
	}
	
	public static function isValidXML(xml:Xml):Bool
	{
		while (xml != null && xml.nodeName != "animation")
			xml = xml.firstElement();
		return (xml != null);
	}
	
	private inline function loadInformationFromPath(path:String):Void
	{	
		var xml:Xml = Xml.parse(cast FileTools.immediateReadFrom(path)).firstElement();
		while (xml != null && xml.nodeName != "animation")
			xml = xml.firstElement();
		if (xml == null)
			throw new Error("Animation.XMLListener() -- ERROR: Bad XML Data");
		loadInformationFromXML(xml); 	
	}

	private function loadInformationFromXML(xml:Xml):Void
	{
		if (mFrames == null) // If the animation is disposed before the load operation completes
			return; 
		var fxml:Fast = new Fast(xml);
		
		if (fxml.hasNode.alpha)
			alpha = Std.parseFloat(fxml.node.alpha.innerData);
		if (fxml.hasNode.red)
			red = Std.parseFloat(fxml.node.red.innerData);
		if (fxml.hasNode.green)
			green = Std.parseFloat(fxml.node.green.innerData);
		if (fxml.hasNode.blue)
			blue = Std.parseFloat(fxml.node.blue.innerData);
			
		if (fxml.hasNode.hue)
			hue = Std.parseFloat(fxml.node.hue.innerData);
		if (fxml.hasNode.saturation)
			saturation = Std.parseFloat(fxml.node.saturation.innerData);
		if (fxml.hasNode.brightness)
			brightness = Std.parseFloat(fxml.node.brightness.innerData);
		
		if (fxml.hasNode.scale)
			scale = Std.parseFloat(fxml.node.scale.innerData);
		if (fxml.hasNode.rotation)
			unitRotation = Std.parseFloat(fxml.node.rotation.innerData);
		if (fxml.hasNode.animationSpeed)
			animationSpeed = Std.parseFloat(fxml.node.animationSpeed.innerData);
		if (fxml.hasNode.animationMode)
			animationMode = Type.createEnum(AnimationMode, fxml.node.animationMode.innerData);
		
		#if openfl
		if (fxml.hasNode.blendMode)
			blendMode = fxml.node.blendMode.innerData.toLowerCase();
		#else
		if (fxml.hasNode.blendMode)
			blendMode = Type.createEnum(cast BlendMode, fxml.node.blendMode.innerData);
		#end
		
		if (fxml.hasNode.flipHorizontal)
			flipHorizontal = (fxml.node.flipHorizontal.innerData == Std.string(true));
		if (fxml.hasNode.flipVertical)
			flipVertical = (fxml.node.flipVertical.innerData == Std.string(true));
		if (fxml.hasNode.smoothing)
			smoothing = (fxml.node.smoothing.innerData == Std.string(true));
			
		for (f in fxml.nodes.frame)
		{
			if (!f.hasNode.assetPath || f.node.assetPath.innerData == "")
			{
				trace ("WARNING: No asset path or empty asset path in frame");
				continue;
			}
			
			var delay:Float = Std.parseFloat(f.node.delay.innerData);
			if (delay <= 0) 
				delay = 0.1;
			
			var offsetX:Float = f.hasNode.offsetX ? Std.parseFloat(f.node.offsetX.innerData) : 0;
			var offsetY:Float = f.hasNode.offsetY ? Std.parseFloat(f.node.offsetY.innerData) : 0;
			
			var clipRectX:Float = f.hasNode.clipRectX ? Std.parseFloat(f.node.clipRectX.innerData) : 0;
			var clipRectY:Float = f.hasNode.clipRectY ? Std.parseFloat(f.node.clipRectY.innerData) : 0;
			var clipRectWidth:Float = f.hasNode.clipRectWidth ? Std.parseFloat(f.node.clipRectWidth.innerData) : 0;
			var clipRectHeight:Float = f.hasNode.clipRectHeight ? Std.parseFloat(f.node.clipRectHeight.innerData) : 0;
			var nullClipRect:Bool = (clipRectWidth == 0 || clipRectHeight == 0);
			
			addFrame(f.node.assetPath.innerData, delay, new Point(offsetX, offsetY), 
				nullClipRect ? null : new Rectangle(clipRectX, clipRectY, clipRectWidth, clipRectHeight));
			for (x in f.nodes.point)
				mFrames[mFrames.length - 1].setRegistrationPoint(Std.parseInt(x.node.index.innerData), 
					new Point(Std.parseFloat(x.node.x.innerData), Std.parseFloat(x.node.y.innerData)));
		}
	}
	
	public function clone(?startSleeping:Bool):Animation
	{
		var a:Animation = new Animation();
		if (startSleeping != null)
			a.sleeping = startSleeping;
		else
			a.sleeping = sleeping;
		a.currentFrame = this.currentFrame;
		a.mCurrentDelay = this.mCurrentDelay;
		a.animationSpeed = this.animationSpeed;
		a.animationMode = this.animationMode;
		a.red = this.red;
		a.green = this.green;
		a.blue = this.blue;
		a.alpha = this.alpha;
		a.hue = this.hue;
		a.saturation = this.saturation;
		a.brightness = this.brightness;
		a.scale = this.scale;
		a.unitRotation = this.unitRotation;
		a.blendMode = this.blendMode;
		a.flipHorizontal = this.flipHorizontal;
		a.flipVertical = this.flipVertical;
		a.smoothing = this.smoothing;
		for (f in mFrames)
		{
			f = f.clone(a.sleeping);
			f.parent = a;
			a.mFrames.push(f);
		}
			
		return a;
	}
	
	public function progress(time:Float):Void
	{
		var mode:AnimationMode = animationMode;
		if (animationSpeed < 0)
		{
			mode = reverseMode(mode);
			animationSpeed = -animationSpeed;
		}
		
		mCurrentDelay += Math.abs(animationSpeed) * time;
		
		if (mFrames.length == 0)
			return;
		var delay:Float = getActiveFrame().delay;
		
		while (mCurrentDelay >= delay)
		{
			delay = getActiveFrame().delay;
			mCurrentDelay -= delay;
			mEnteringFrame = true;
			switch (mode)
			{
				case AnimationMode.ForwardLoop:
					currentFrame = (currentFrame + 1) % mFrames.length;
					if (currentFrame == 0)
						dispatchEvent(new AnimationEvent(AnimationEvent.ANIM_LOOP));
					
				case AnimationMode.ReverseLoop:
					if (--currentFrame < 0)
					{
						currentFrame = mFrames.length - 1;
						dispatchEvent(new AnimationEvent(AnimationEvent.ANIM_LOOP));
					}
					
				case AnimationMode.ForwardPingpong:
					if (++currentFrame >= mFrames.length)
					{
						animationMode = reverseMode(animationMode);
						dispatchEvent(new AnimationEvent(AnimationEvent.ANIM_BOUNCE));
					}
					
				case AnimationMode.ReversePingpong:
					if (--currentFrame <= 0)
					{
						animationMode = reverseMode(animationMode);
						dispatchEvent(new AnimationEvent(AnimationEvent.ANIM_BOUNCE));
					}
					
				case AnimationMode.ForwardStop:
					if (++currentFrame >= mFrames.length - 1)
					{
						currentFrame = mFrames.length - 1;
						mCurrentDelay = 0;
						if (!complete)
						{
							complete = true;
							dispatchEvent(new AnimationEvent(AnimationEvent.ANIM_FINISH));
						}
					}
					
				case AnimationMode.ReverseStop:
					if (--currentFrame <= 0)
					{
						currentFrame = 0;
						mCurrentDelay = 0;
						if (!complete)
						{
							complete = true;
							dispatchEvent(new AnimationEvent(AnimationEvent.ANIM_FINISH));
						}
					}
					
				case AnimationMode.Random:
					currentFrame = Std.random(mFrames.length);
				case AnimationMode.Stop:
					//nothing
			}
			mode = animationMode;
		}
	}		
	
	/*public function updateTiles():Void
	{
		mTilemap.removeTiles();
		if (mFrames.length == 0)
			return;
		var f:Frame = getActiveFrame();
		var t:Tile = f.tile;
		if (t == null)
			return;
			
		f.updateTile();
		var cos:Float = Math.cos((t.rotation / 180) * Math.PI);
		var sin:Float = Math.sin((t.rotation / 180) * Math.PI);
		var left:Float = -t.originX;
		var right:Float = f.clipRect.width - t.originX;
		if (f.flipHorizontal != flipHorizontal)
		{
			left = t.originX - f.clipRect.width;
			right = t.originX;
		}
		var top:Float = -t.originY;
		var bottom:Float = f.clipRect.height - t.originY;
		if (f.flipVertical != flipVertical)
		{
			top = t.originY - f.clipRect.height;
			bottom = t.originY;
		}
		var minX:Float = Math.min(Math.min(Math.min(
			left * cos - top * sin, 
			left * cos - bottom * sin), 
			right * cos - top * sin), 
			right * cos - bottom * sin) * f.scale * scale; 
		var minY:Float = Math.min(Math.min(Math.min(
			left * sin + top * cos, 
			left * sin + bottom * cos), 
			right * sin + top * cos), 
			right * sin + bottom * cos) * f.scale * scale; 
		var maxX:Float = Math.max(Math.max(Math.max(
			left * cos - top * sin, 
			left * cos - bottom * sin), 
			right * cos - top * sin), 
			right * cos - bottom * sin) * f.scale * scale; 
		var maxY:Float = Math.max(Math.max(Math.max(
			left * sin + top * cos, 
			left * sin + bottom * cos), 
			right * sin + top * cos), 
			right * sin + bottom * cos) * f.scale * scale; 
			
		var expandW:Int = 0;
		var expandH:Int = 0;
		
		if (maxX - minX > mTilemap.width)
			expandW = IntMath.max(0, Math.ceil(maxX - minX - mTilemap.width));
		if (maxY - minY > mTilemap.height)
			expandH = IntMath.max(0, Math.ceil(maxY - minY - mTilemap.height));
		
		if (mTilemap.tileset != getActiveFrame().tileset || expandH > 0 || expandW > 0)
		{
			removeChild(mTilemap);
			mTilemap = new Tilemap(Math.ceil(mTilemap.width + expandW), Math.ceil(mTilemap.height + expandH), getActiveFrame().tileset, smoothing);
			addChild(mTilemap);
		}
		mTilemap.addTile(t);
		mTilemap.x = 0;
		mTilemap.y = 0;
				
		t.x = t.y = 0;
		if (minX < 0)
		{
			mTilemap.x = minX;
			t.x = -minX;
		}
		if (minY < 0)
		{
			mTilemap.y = minY;
			t.y = -minY;
		}
	}*/
	
	private function updateColorMatrix():Void
	{
		if (mColorMatrixFilter == null)
		{
			if (hue == 0 && saturation == 1 && brightness == 0)
				return;
			mColorMatrixFilter = new ColorMatrixFilter();
		}
		if (hue == 0 && saturation == 1 && brightness == 0)
		{
			mColorMatrixFilter = null;
			return;
		}
		var cm:ColorMatrix = new ColorMatrix();
		cm.adjustHue(hue);
		cm.adjustSaturation(saturation);
		cm.adjustBrightness(brightness);
		mColorMatrixFilter.matrix = cm.matrix;
	}
	
	private inline function getActiveFrame():Frame
	{
		return mFrames[currentFrame];
	}
	
	public inline function dispose():Void
	{
		sleeping = true;
		while (mFrames.length > 0)
			mFrames.pop().dispose();
		mFrames = null;
	}
	
	public function setRegistrationPoint(frame:Int, index:Int, point:Point):Void
	{
		mFrames[frame].setRegistrationPoint(index, point);
	}
	
	public inline function getRegistrationPoint(frame:Int, index:Int, transform:Bool):Point
	{
		return mFrames[frame].getRegistrationPoint(index, transform);
	}
	
	public inline function getCurrentRegistrationPoint(index:Int, transform:Bool):Point
	{
		return getRegistrationPoint(currentFrame, index, transform);
	}
	
	public function reverse():Void
	{
		animationMode = reverseMode(animationMode);
	}
	
	private inline function reverseMode(mode:AnimationMode):AnimationMode
	{
		return switch(mode)
		{
			case AnimationMode.ForwardLoop: 		AnimationMode.ReverseLoop;
			case AnimationMode.ForwardStop: 		AnimationMode.ReverseStop;
			case AnimationMode.ForwardPingpong: 	AnimationMode.ReversePingpong;
			case AnimationMode.ReverseLoop: 		AnimationMode.ForwardLoop;
			case AnimationMode.ReverseStop: 		AnimationMode.ForwardStop;
			case AnimationMode.ReversePingpong: 	AnimationMode.ForwardPingpong;
			default: mode;
		}
	}
	
	public function reset():Void
	{
		mCurrentDelay = 0;
		complete = false;
		currentFrame = switch(animationMode)
		{
			case AnimationMode.ForwardLoop, AnimationMode.ForwardPingpong, AnimationMode.ForwardStop:
				0;
			case AnimationMode.ReverseLoop, AnimationMode.ReversePingpong, AnimationMode.ReverseStop:
				mFrames.length - 1;
			case AnimationMode.Random:
				Std.random(mFrames.length);
			case AnimationMode.Stop:
				currentFrame;
		}
	}
	
	public function getTime():Float
	{
		var time:Float = 0;
		switch(animationMode)
		{
			case AnimationMode.ForwardLoop, AnimationMode.ForwardPingpong, AnimationMode.ForwardStop:
				for (i in 0...currentFrame)
					time += mFrames[i].delay;
				time += mCurrentDelay;
				
			case AnimationMode.ReverseLoop, AnimationMode.ReversePingpong, AnimationMode.ReverseStop:
				for (i in 1...(mFrames.length - currentFrame))
					time += mFrames[mFrames.length - i].delay;
				time += mCurrentDelay;
				
			case AnimationMode.Stop, AnimationMode.Random:
				// also nothing
		}
		return time;
	}
	
	public function gotoTime(time:Float):Void
	{
		reset();
		progress(time);
	}
	
	public function gotoFirst():Void
	{
		currentFrame = 0;
		mCurrentDelay = 0;
		complete = false;
	}
	
	public function gotoLast():Void
	{
		currentFrame = Std.int(Math.max(0, mFrames.length - 1));
		mCurrentDelay = 0;
	}
	
	public function gotoFrame(i:Int):Void
	{
		currentFrame = Std.int(Math.max(0, Math.min(mFrames.length - 1, i)));
		mCurrentDelay = 0;
		if (currentFrame < mFrames.length - 1)
			complete = false;
	}
	
	public function addFrame(assetPath:String, delay:Float, ?offset:Point, ?clipRect:Rectangle):Void
	{	insertFrame(0xFFFFFF, assetPath, delay, offset, clipRect);					 }
	
	public function insertFrame(i:Int, assetPath:String, delay:Float, ?offset:Point, ?clipRect:Rectangle):Void
	{	insertRawFrame(i, new Frame(assetPath, delay, offset, clipRect, this, sleeping));	}
	public function addRawFrame(frame:Frame):Void
	{	insertRawFrame(0xFFFFFF, frame);					 }
	public function insertRawFrame(i:Int, frame:Frame):Void
	{
		frame.parent = this;
		frame.sleeping = sleeping;
		if (!sleeping && !frame.ready)
		{
			ready = false;
			frame.addEventListener(AnimationEvent.ANIM_READY, checkReady);
		}
		if (i < mFrames.length)
			mFrames.insert(i, frame);
		else
			mFrames.push(frame);
		dispatchEvent(new AnimationEvent(AnimationEvent.ANIM_ADD_FRAME));
	}
	private function checkReady(e:AnimationEvent)
	{
		cast (e.target, Frame).removeEventListener(AnimationEvent.ANIM_READY, checkReady);
		if (ready)
			dispatchEvent(new AnimationEvent(AnimationEvent.ANIM_READY));
	}
	
	public function removeLastFrame():Void
	{	
		mFrames.pop().dispose();		
		dispatchEvent(new AnimationEvent(AnimationEvent.ANIM_REMOVE_FRAME));
	}
	
	public function removeFrame(i:Int):Void
	{
		if (i >= mFrames.length)
			throw new Error("Animation.removeFrame(): Index " + i + " out of range " + mFrames.length);
		mFrames[i].dispose();
		mFrames.splice(i, 1);
		if (i <= currentFrame)
			--currentFrame;
		dispatchEvent(new AnimationEvent(AnimationEvent.ANIM_REMOVE_FRAME));
	}
	
	public function replaceFrame(i:Int, assetPath:String, delay:Float, offset:Point, clipRect:Rectangle = null):Void
	{
		if (i >= mFrames.length)
			throw new Error("BitmapFrameSet.replaceFrame(): Index " + i + " out of range " + mFrames.length);
		var f:Frame = new Frame(assetPath, delay, offset, clipRect, this, sleeping); 
		mFrames[i].dispose();
		mFrames[i] = f;
		if (!sleeping && !f.ready)
		{
			ready = false;
			f.addEventListener(AnimationEvent.ANIM_READY, checkReady);
		}
		dispatchEvent(new AnimationEvent(AnimationEvent.ANIM_REPLACE_FRAME));
	}				
	
	public function getFrame(i:Int):Frame
	{	return mFrames[i];				}
	
	public function getAllFrames():Array<Frame>
	{	return mFrames.copy();		}
	
	private function get_ready():Bool
	{
		if (ready)
			return true;
		if (sleeping)
			return false;
			
		for (f in mFrames)
			if (!f.ready)
				return false;
		return ready = true;
	}
	
	private inline function set_sleeping(val:Bool):Bool
	{
		if (val != sleeping)
		{
			if (val)
				ready = false;
			for (f in mFrames)
				f.sleeping = val;
		}
		return sleeping = val;
	}
	
	private inline function set_currentFrame(val:Int):Int
	{	return currentFrame = Std.int(Math.max(0, Math.min(mFrames.length - 1, val)));			}
	
	
	private inline function set_hue(val:Float):Float
	{	
		if (val != hue)
		{
			hue = val;
			updateColorMatrix();
		}
		return val;			
	}
	private inline function set_saturation(val:Float):Float
	{	
		if (val != saturation)
		{
			saturation = val;
			updateColorMatrix();
		}
		return val;			
	}
	private inline function set_brightness(val:Float):Float
	{	
		if (val != brightness)
		{
			brightness = val;
			updateColorMatrix();
		}
		return val;			
	}
	
	private function get_width():Float
	{	
		if (mFrames.length > 0)
			return mFrames[currentFrame].width;		
		else
			return 0;
	}
	
	private function get_height():Float
	{
		if (mFrames.length > 0)
			return mFrames[currentFrame].height;		
		else
			return 0;
	}
	
	private inline function get_numFrames():Int
	{	return mFrames.length;			}
	
	private inline function get_degreeRotation():Float
	{	return unitRotation * 360;	}	
	private inline function set_degreeRotation(val:Float):Float
	{	
		unitRotation = val / 360;	
		return val;	
	}
	private inline function get_radianRotation():Float
	{	return unitRotation * 2 * Math.PI;	}	
	private inline function set_radianRotation(val:Float):Float
	{	
		unitRotation = ((val / 2) / Math.PI);	
		return val;	
	}
}