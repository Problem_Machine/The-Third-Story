package game.animation;

enum AnimationMode
{
	Stop;
	ForwardStop;
	ReverseStop;
	ForwardLoop;
	ReverseLoop;
	ForwardPingpong;
	ReversePingpong;
	Random;
}