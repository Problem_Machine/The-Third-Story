package game.animation;

import flash.display.Sprite;
import flash.geom.Rectangle;
import game.animation.event.AnimationEvent;
import openfl.display.Tile;
import openfl.display.Tilemap;

class AnimationContainer extends Sprite
{
	public var renderBounds:Rectangle;
	
	private var mTilemaps:Array<Tilemap>;
	private var mAnimations:Array<Animation>;

	public function new() 
	{
		super();
		
		mAnimations = [];
		mTilemaps = [];
	}
	
	@:access(game.animation.Animation)
	@:access(game.animation.Frame)
	public function update(time:Float):Void
	{
		var i:Int = 0;
		for (tm in mTilemaps)
		{
			tm.removeTiles();
			tm.x = tm.y = 0;
		}
		for (a in mAnimations)
		{
			a.progress(time);
			if (a.numFrames == 0)
				continue;
			var f:Frame = a.getActiveFrame();
			var t:Tile = f.tile;
			if (t == null)
				continue;
			f.updateTile();
			if (t.scaleX == 0 || t.scaleY == 0 || t.alpha == 0)
				continue;
			
			var cos:Float = Math.cos((t.rotation / 180) * Math.PI);
			var sin:Float = Math.sin((t.rotation / 180) * Math.PI);
			var left:Float = -t.originX;
			var right:Float = f.clipRect.width - t.originX;
			if (f.flipHorizontal != a.flipHorizontal)
			{
				left = t.originX - f.clipRect.width;
				right = t.originX;
			}
			var top:Float = -t.originY;
			var bottom:Float = f.clipRect.height - t.originY;
			if (f.flipVertical != a.flipVertical)
			{
				top = t.originY - f.clipRect.height;
				bottom = t.originY;
			}
			var minX:Float = Math.min(Math.min(Math.min(
				left * cos - top * sin, 
				left * cos - bottom * sin), 
				right * cos - top * sin), 
				right * cos - bottom * sin) * f.scale * a.scale + a.x; 
			var minY:Float = Math.min(Math.min(Math.min(
				left * sin + top * cos, 
				left * sin + bottom * cos), 
				right * sin + top * cos), 
				right * sin + bottom * cos) * f.scale * a.scale + a.y; 
			var maxX:Float = Math.max(Math.max(Math.max(
				left * cos - top * sin, 
				left * cos - bottom * sin), 
				right * cos - top * sin), 
				right * cos - bottom * sin) * f.scale * a.scale + a.x; 
			var maxY:Float = Math.max(Math.max(Math.max(
				left * sin + top * cos, 
				left * sin + bottom * cos), 
				right * sin + top * cos), 
				right * sin + bottom * cos) * f.scale * a.scale + a.y;
			
			if (renderBounds != null)
				if ((minX > renderBounds.right || maxX < renderBounds.left) && (minY > renderBounds.bottom || maxY < renderBounds.top))
					continue;
				
			if (mTilemaps[i].tileset != f.tileset)
				++i;
			if (i == mTilemaps.length)
			{
				if (renderBounds != null)
					mTilemaps.push(new Tilemap(Math.ceil(renderBounds.width), Math.ceil(renderBounds.height), f.tileset, a.smoothing));
				else
					mTilemaps.push(new Tilemap(Math.ceil(maxX - minX), Math.ceil(maxY - minY), f.tileset, a.smoothing));
				addChild(mTilemaps[mTilemaps.length - 1]);
			}
			var tm:Tilemap = mTilemaps[i];
			if (tm.tileset != f.tileset)
			{
				var old:Tilemap = tm;
				if (renderBounds != null)
					tm = new Tilemap(Math.ceil(renderBounds.width), Math.ceil(renderBounds.height), f.tileset, tm.smoothing);
				else
					tm = new Tilemap(Math.ceil(Math.max(maxX - minX, tm.width)), Math.ceil(Math.max(maxY - minY, tm.height)), f.tileset, tm.smoothing);
				mTilemaps.splice(i, 1);
				mTilemaps.insert(i, tm);
				addChildAt(tm, getChildIndex(old));
				removeChild(old);
			}
			
			tm.addTile(t);
			if (renderBounds == null)
			{
				minX -= tm.x;
				minY -= tm.y;
				if (minX < 0)
				{
					tm.x += minX;
					minX = 0;
				}
				if (minY < 0)
				{
					tm.y += minY;
					minY = 0;
				}
				maxX -= tm.x;
				maxY -= tm.y;
			}
			else
			{
				tm.x = renderBounds.x;
				tm.y = renderBounds.y;
			}
			t.x = a.x - tm.x;
			t.y = a.y - tm.y;
			
			if ((renderBounds != null && (renderBounds.width > tm.width || renderBounds.height > tm.height)) || 
				(renderBounds == null && (maxX > tm.width || maxY > tm.height)))
			{
				var old:Tilemap = tm;
				if (renderBounds != null)
					tm = new Tilemap(Math.ceil(renderBounds.width), Math.ceil(renderBounds.height), f.tileset, tm.smoothing);
				else
					tm = new Tilemap(Math.ceil(Math.max(maxX, tm.width)), Math.ceil(Math.max(maxY, tm.height)), f.tileset, tm.smoothing);
				mTilemaps.splice(i, 1);
				mTilemaps.insert(i, tm);
				tm.x = old.x;
				tm.y = old.y;
				while (old.numTiles > 0)
					tm.addTile(old.removeTileAt(0));
				addChildAt(tm, getChildIndex(old));
				removeChild(old);
			}
		}
		if (i + 1 < mTilemaps.length)
			mTilemaps.splice(i + 1, mTilemaps.length - (i + 1));
	}
	
	@:access(game.animation.Animation.getActiveFrame)
	public function addAnimation(a:Animation):Int
	{
		if (mAnimations.indexOf(a) >= 0)
			mAnimations.remove(a);
		if (mTilemaps.length == 0)
		{
			if (!a.ready)
				a.addEventListener(AnimationEvent.ANIM_READY, animReadyListener);
			else
			{
				for (an in mTilemaps)
					an.removeEventListener(AnimationEvent.ANIM_READY, animReadyListener);
				mTilemaps.push(new Tilemap(Math.floor(a.width), Math.floor(a.height), a.getActiveFrame().tileset));
				addChild(mTilemaps[0]);
			}
		}
		return mAnimations.push(a);	
	}
	
	@:access(game.animation.Animation.getActiveFrame)
	private function animReadyListener(e:AnimationEvent):Void
	{
		if (mTilemaps.length == 0)
		{
			var a:Animation = cast e.target;
			mTilemaps.push(new Tilemap(Math.floor(a.width), Math.floor(a.height), a.getActiveFrame().tileset));
			addChild(mTilemaps[0]);
		}
		for (an in mTilemaps)
			an.removeEventListener(AnimationEvent.ANIM_READY, animReadyListener);
	}
	
	@:access(game.animation.Animation.getActiveFrame)
	public function addAnimationAt(a:Animation, index:Int):Void
	{
		if (mAnimations.indexOf(a) >= 0)
		{
			if (mAnimations.indexOf(a) < index)
				--index;
			mAnimations.remove(a);
		}
		if (mTilemaps.length == 0)
		{
			mTilemaps.push(new Tilemap(Math.floor(a.width), Math.floor(a.height), a.getActiveFrame().tileset));
			addChild(mTilemaps[0]);
		}
		if (index > mAnimations.length)
			mAnimations.push(a);
		else
			mAnimations.insert(index, a);	
	}
	
	public function removeAnimation(a:Animation):Bool
	{	return mAnimations.remove(a);	}
	
	public function removeAnimationAt(i:Int):Void
	{	mAnimations.splice(i, 1);	}
	
}