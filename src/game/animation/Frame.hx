﻿package game.animation;

import flash.geom.ColorTransform;
import game.animation.event.AnimationEvent;
import com.problemmachine.bitmap.bitmapmanager.BitmapManager;
import com.problemmachine.bitmap.bitmapmanager.BitmapManagerEvent;
import com.problemmachine.bitmap.bitmapmanager.BitmapManagerQuery;
import com.problemmachine.tools.file.FileTools;
import flash.display.BitmapData;
import flash.events.EventDispatcher;
import flash.geom.Point;
import flash.geom.Rectangle;
import openfl.display.Tile;
import openfl.display.Tileset;

class Frame extends EventDispatcher
{
	private static var sResources:Array<ResourceReference> = [];
	
	private var mRegistrationPoints:Array<Point>;
	private var mClipRect:Rectangle;
	
	public var assetPath(default, set):String;
	public var sleeping(default, set):Bool;
	public var parent(default, set):Animation;
	public var delay(default, default):Float;
	public var scale:Float;
	public var offset(default, set):Point;
	public var clipRect(get, set):Rectangle;
	public var flipHorizontal:Bool;
	public var flipVertical:Bool;
	public var ready(get, null):Bool;
	
	public var width(get, never):Int;
	public var height(get, never):Int;
	public var degreeRotation(get, set):Float;
	public var radianRotation(get, set):Float;
	public var unitRotation:Float;
	public var red:Float;
	public var green:Float;
	public var blue:Float;
	public var alpha:Float;
	public var hue(default, set):Float;
	public var saturation(default, set):Float;
	public var brightness(default, set):Float;
	
	public var tile(default, null):Tile;
	public var tileset(get, never):Tileset;
	
	public function new(assetPath:String, delay:Float, ?offset:Point, ?clipRect:Rectangle, 
		?parent:Animation, startAsleep:Bool = true)
	{		
		super(this);

		mRegistrationPoints = [];
		
		ready = false;
		if (offset != null)
			this.offset = offset.clone();
		else
			this.offset = new Point();
		this.assetPath = assetPath;
		this.parent = parent;
		this.delay = delay;
		this.sleeping = startAsleep;
		this.unitRotation = 0;
		this.scale = 1;
		this.offset = (offset == null ? new Point() : offset.clone());
		if (clipRect != null)
			this.clipRect = clipRect.clone();
		this.flipHorizontal = false;
		this.flipVertical = false;
		this.alpha = 1;
		this.red = this.green = this.blue = 1;
		this.hue = this.brightness = 0;
		this.saturation = 1;
	}
	
	private inline function get_ready():Bool
	{
		if (ready)
			return true;
		if (!sleeping && getBitmap() != null)
			return ready = true;
		else
			return false;
	}
	
	public function setRegistrationPoint(index:Int, point:Point):Void
	{
		if (index < 0)
			return;
		while (mRegistrationPoints.length + 1 < index)
			mRegistrationPoints.push(null);
		if (mRegistrationPoints[index] == null)
			mRegistrationPoints[index] = new Point();
		mRegistrationPoints[index].x = point.x;
		mRegistrationPoints[index].y = point.y;
	}
	
	public function getRegistrationPoint(index:Int, transform:Bool):Point
	{
		if (index < 0 || index >= mRegistrationPoints.length)
			return new Point();
		if (mRegistrationPoints[index] == null)
			return new Point();
		var p:Point = mRegistrationPoints[index].clone();
		if (transform)
		{
			var s:Float = scale * (parent != null ? parent.scale : 1);
			var r:Float = unitRotation + (parent != null ? parent.unitRotation : 0);
			p.x += clipRect.width * 0.5;
			p.y += clipRect.height * 0.5;
			if (r != 0)
			{
				r *= 2 * Math.PI;
				var sin:Float = Math.sin(r);
				var cos:Float = Math.cos(r);
				p.x = (p.x * cos) - (p.y * sin);
				p.y = (p.y * cos) + (p.x * sin);
			}
			p.x -= clipRect.width * 0.5 * s;
			p.y -= clipRect.height * 0.5 * s;
		}
		return p;
	}
	
	public inline function updateTile():Void
	{
		if (tile == null)
			return;
		
		tile.scaleX = tile.scaleY = scale * (parent != null ? parent.scale : 1);
		tile.rotation = degreeRotation + (parent != null ? parent.degreeRotation : 0);
		
		if ((parent == null && flipHorizontal) || (parent != null && flipHorizontal != parent.flipHorizontal))
			tile.scaleX *= -1;
		if ((parent == null && flipVertical) || (parent != null && flipVertical != parent.flipVertical))
			tile.scaleY *= -1;
		
		if (tile.colorTransform == null)
			tile.colorTransform = new ColorTransform();
		tile.colorTransform.redMultiplier = red * (parent != null ? parent.red : 1);
		tile.colorTransform.greenMultiplier = green * (parent != null ? parent.green : 1);
		tile.colorTransform.blueMultiplier = blue * (parent != null ? parent.blue : 1);
		tile.alpha = alpha * (parent != null ? parent.alpha : 1);
	}
	
	public function clone(?startSleeping:Bool):Frame
	{
		var f:Frame = new Frame(assetPath, delay, offset, clipRect, null, true);
		f.ready = this.ready;
		f.alpha = this.alpha;
		f.red = this.red;
		f.green = this.green;
		f.blue = this.blue;
		f.hue = this.hue;
		f.saturation = this.saturation;
		f.brightness = this.brightness;
		f.flipHorizontal = this.flipHorizontal;
		f.flipVertical = this.flipVertical;
		f.unitRotation = this.unitRotation;
		f.scale = this.scale;
		f.mRegistrationPoints = mRegistrationPoints.copy();
		for (i in 0...f.mRegistrationPoints.length)
			if (f.mRegistrationPoints[i] != null)
				f.mRegistrationPoints[i] = f.mRegistrationPoints[i].clone();
		if (startSleeping != null)
			f.sleeping = startSleeping;
		else
			f.sleeping = this.sleeping;
		return f;
	}
	
	public inline function dispose():Void
	{
		parent = null;
		releaseAsset(assetPath);
		ready = false;
	}
	
	private function requestAsset(path:String):Void
	{
		var ref:ResourceReference = getResource(path);
		
		if (ref == null)
		{
			ref = new ResourceReference(path);
			ref.addFrame(this);
			sResources.push(ref);
			if (ref.bitmap != null)
				ready = !sleeping;
			else
				ready = false;
		}
		else if (ref.frames.indexOf(this) < 0)
			ref.addFrame(this);
	}
	
	private function releaseAsset(path:String):Void
	{
		var ref:ResourceReference = getResource(path);
		ref.removeFrame(this);
		if (ref.frames.length == 0)
			sResources.remove(ref);
	}
	
	private function getResource(path:String):ResourceReference
	{
		for (r in sResources)
			if (r.path == path)
				return r;
		return null;
	}
	
	private inline function getBitmap():BitmapData
	{
		var r = getResource(assetPath);
		return (r != null) ? r.bitmap : null;
	}
	
	private inline function get_tileset():Tileset
	{
		var r:ResourceReference = getResource(assetPath);
		return (r != null ? r.tileset : null);
	}
	
	@:access(game.animation.Animation.ready)
	private inline function set_assetPath(val:String):String
	{
		if (val == null || FileTools.formatPath(val.toLowerCase()) != this.assetPath)
		{
			if (this.assetPath != null && this.assetPath != "")
				releaseAsset(FileTools.formatPath(assetPath.toLowerCase()));
			this.assetPath = val;
			if (this.assetPath == null)
				this.assetPath = "";
			else 
				this.assetPath = FileTools.formatPath(this.assetPath.toLowerCase());
				
			if (assetPath != "")
				requestAsset(assetPath);
				
			if (parent != null && getBitmap() == null)
				parent.ready = false;
		}
		return val;
	}
	
	private inline function set_sleeping(val:Bool):Bool
	{
		if (val != sleeping)
		{
			if (val)
			{
				releaseAsset(assetPath);
				ready = false;
			}
			else if (assetPath != "")
				requestAsset(assetPath);
		}
		return sleeping = val;
	}
	
	private inline function set_parent(p:Animation):Animation
	{	
		return parent = p;
	}
	
	private inline function set_offset(val:Point):Point
	{
		if (offset == null || val.x != offset.x && val.y != offset.y)
		{
			if (val == null)
				offset.x = offset.y = 0;
			else
				offset = val.clone();
			if (tile != null)
			{
				tile.originX = -offset.x;
				tile.originY = -offset.y;
			}
		}
		return val;
	}
		
	
	private function get_clipRect():Rectangle
	{
		if (mClipRect == null)
		{
			if (getBitmap() == null)
				return null;
			mClipRect = new Rectangle(0, 0, getBitmap().width, getBitmap().height);
		}
		return mClipRect;
	}
	private function set_clipRect(r:Rectangle):Rectangle
	{	
		return mClipRect = r;
	}

	private inline function get_degreeRotation():Float
	{	return unitRotation * 360;	}	
	private inline function set_degreeRotation(val:Float):Float
	{	
		unitRotation = val / 360;	
		return val;	
	}
	private inline function get_radianRotation():Float
	{	return unitRotation * 2 * Math.PI;	}	
	private inline function set_radianRotation(val:Float):Float
	{	
		unitRotation = ((val / 2) / Math.PI);	
		return val;	
	}
	
	private inline function set_hue(val:Float):Float
	{
		if (val != hue)
			hue = val;
		return val;
	}
	private inline function set_saturation(val:Float):Float
	{
		if (val != saturation)
			saturation = val;
		return val;
	}
	private inline function set_brightness(val:Float):Float
	{
		if (val != brightness)
			brightness = val;
		return val;
	}
	
	private function get_width():Int
	{
		if (clipRect == null)
			return 0;
		var s:Float = scale * (parent != null ? parent.scale : 1);
		var r:Float = radianRotation + (parent != null ? parent.radianRotation : 0);
		if (s == 1 && r == 0)
			return Math.ceil(clipRect.width);
		var cos:Float = Math.cos(r);
		var sin:Float = Math.sin(r);
		var maxX:Float = 
			Math.max(Math.abs((-clipRect.width * 0.5 * cos) - (-clipRect.height * 0.5 * sin)),
					Math.abs((clipRect.width * 0.5 * cos) - (-clipRect.height * 0.5 * sin)));
		//	because all rotations are rectangular and symmetric we only have to test half the points
		// 		and we can calculate width as maximum x offset * 2
		
		return Math.ceil(maxX * 2 * s);
	}
	
	private function get_height():Int
	{
		if (clipRect == null)
			return 0;
		var s:Float = scale * (parent != null ? parent.scale : 1);
		var r:Float = radianRotation + (parent != null ? parent.radianRotation : 0);
		if (s == 1 && r == 0)
			return Math.ceil(clipRect.height);
		var cos:Float = Math.cos(r);
		var sin:Float = Math.sin(r);
		var maxY:Float = 
			Math.max(Math.abs((-clipRect.height * 0.5 * cos) + (-clipRect.width * 0.5 * sin)),
					Math.abs((-clipRect.height * 0.5 * cos) + (clipRect.width * 0.5 * sin)));
		//	because all rotations are rectangular and symmetric we only have to test half the points
		// 		and we can calculate height as maximum y offset * 2
		
		return Math.ceil(maxY * 2 * s);
	}
}

@:access(game.animation.Frame)
private class ResourceReference
{
	public var path(default, null):String;
	public var frames:Array<Frame>;
	public var tileset:Tileset;
	public var bitmap(default, set):BitmapData;
	public function new (path:String)
	{
		frames = [];
		this.path = path;
		switch (BitmapManager.query(path))
		{
			case BitmapManagerQuery.Ready:
				BitmapManager.lock(path, this);
				bitmap = BitmapManager.request(path);
			case BitmapManagerQuery.NotReady:
				BitmapManager.startListeningForCompletion(path, bitmapListener); 
				BitmapManager.lock(path, this);
			case BitmapManagerQuery.NotActive, BitmapManagerQuery.NotFound:
				BitmapManager.startListeningForCompletion(path, bitmapListener); 
				BitmapManager.loadAndLock(path, this);
		}
	}
	
	private inline function set_bitmap(val:BitmapData):BitmapData
	{
		if (val != bitmap)
		{
			bitmap = val;
			tileset = new Tileset(bitmap);
			for (f in frames)
				f.tile = new Tile(tileset.addRect(f.clipRect), 0, 0, 1, 1, 0, -f.offset.x, -f.offset.y);
		}
		return val;
	}
	
	public function addFrame(frame:Frame):Void
	{
		frames.push(frame);
		if (tileset != null)
			frame.tile = new Tile(tileset.addRect(frame.clipRect), 0, 0, 1, 1, 0, -frame.offset.x, -frame.offset.y);
	}
	
	public function removeFrame(frame:Frame):Void
	{
		if (frames.length == 0)
		{
			BitmapManager.unlock(path, this);
			BitmapManager.stopListeningForCompletion(path, bitmapListener);
			bitmap = null;
			tileset = null;
			frames = null;
		}
	}
	
	private function bitmapListener(e:BitmapManagerEvent):Void
	{
		BitmapManager.stopListeningForCompletion(path, bitmapListener);
		bitmap = BitmapManager.request(path);
		for (f in frames)
			f.dispatchEvent(new AnimationEvent(AnimationEvent.ANIM_READY));
	}
}