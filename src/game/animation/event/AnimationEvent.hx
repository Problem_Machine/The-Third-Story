package game.animation.event;
import flash.events.Event;

class AnimationEvent extends Event
{
	public static inline var ANIM_FINISH:String = "finished animation";
	public static inline var ANIM_READY:String = "ready animation";
	public static inline var ANIM_LOOP:String = "looped animation";
	public static inline var ANIM_BOUNCE:String = "bounced animation";
	public static inline var ANIM_FRAME:String = "animation frame";
	public static inline var ANIM_ADD_FRAME:String = "add frame";
	public static inline var ANIM_REMOVE_FRAME:String = "remove frame";
	public static inline var ANIM_REPLACE_FRAME:String = "replace frame";

	public function new(type:String, bubbles:Bool = false, cancelable:Bool = false) 
	{
		super(type, bubbles, cancelable);
	}
	
}