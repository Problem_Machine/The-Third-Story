package game.gametile.manager;
import com.problemmachine.bitmap.bitmapmanager.BitmapManager;
import com.problemmachine.bitmap.bitmapmanager.BitmapManagerEvent;
import com.problemmachine.tools.collision.CollisionTools;
import flash.display.BitmapData;
import flash.events.EventDispatcher;
import flash.geom.Point;
import flash.geom.Rectangle;
import game.gametile.manager.event.TileManagerEvent;
import openfl.display.Tilemap;
import openfl.display.Tileset;

class TileManager
{
	static private inline var REPACK_THRESHOLD:Float = 0.8;
	static private inline var REPACK_PADDING:Float = 1.2;
	
	static private var sTileset:Tileset = new Tileset(new BitmapData(1, 1, true, 0x00000000));
	static private var sTileReferences:Array<TileReference> = [];
	static private var sPreviousEfficiency:Float = -1;
	
	static public function generateTilemap(width:Int, height:Int, smoothing:Bool):Tilemap
	{
		return new Tilemap(width, height, sTileset, smoothing);
	}
	
	static public function addTilesByPath(path:String, rects:Array<Rectangle>):TileReference
	{
		// TODO: If BitmapData is used in one tileset already, don't duplicate it
		var tr:TileReference = integrateNewTileData(BitmapManager.immediateLoad(path, null), rects, path);
		sTileReferences.push(tr);
		return tr;
	}
	
	static public function addTilesByData(data:BitmapData, rects:Array<Rectangle>):TileReference
	{
		var tr:TileReference = integrateNewTileData(data, rects, null);
		sTileReferences.push(tr);
		return tr;
	}
	
	static private function integrateNewTileData(data:BitmapData, rects:Array<Rectangle>, path:String):TileReference
	{
		var rect:Rectangle = new Rectangle();
		for (r in rects)
		{
			rect.left = Math.min(r.left, rect.left);
			rect.right = Math.max(r.right, rect.right);
			rect.top = Math.min(r.top, rect.top);
			rect.bottom = Math.max(r.bottom, rect.bottom);
		}
		var target:Point = findUnoccupiedSpace(Math.ceil(rect.width), Math.ceil(rect.height));
		if (target == null)
			target = expandToFit(Math.ceil(rect.width), Math.ceil(rect.height));
		sTileset.bitmapData.copyPixels(data, rect, target);
		var newRects:Array<Int> = [];
		for (r in rects)
		{
			var newRectangle:Rectangle = r.clone();
			newRectangle.x = r.x - rect.left + target.x;
			newRectangle.y = r.y - rect.top + target.y;
			newRects.push(sTileset.addRect(newRectangle));
		}
		rect.x = target.x;
		rect.y = target.y;
		var tr:TileReference = new TileReference(rect, newRects, new Point(rect.left, rect.top), path);
			
		if (getEfficiency() < sPreviousEfficiency * REPACK_THRESHOLD)
			repackTiles();
		
		return tr;
	}
	
	static public function updateTilesByPath(reference:TileReference, path:String):Void
	{
		integrateUpdatedTileData(reference, BitmapManager.immediateLoad(path, true));
	}
	
	static public function updateTilesByData(reference:TileReference, data:BitmapData):Void
	{
		integrateUpdatedTileData(reference, data);
	}
	
	@:access(game.gametile.manager.TileReference.mBitmapOffset)
	static public function integrateUpdatedTileData(reference:TileReference, data:BitmapData):Void
	{
		var target:Point = new Point(reference.bitmapRect.x, reference.bitmapRect.y);
		sTileset.bitmapData.fillRect(reference.bitmapRect, 0x00000000);
		var rect:Rectangle = reference.bitmapRect.clone();
		rect.x = reference.mBitmapOffset.x;
		rect.y = reference.mBitmapOffset.y;
		sTileset.bitmapData.copyPixels(data, rect, target);
	}
	
	static private function findUnoccupiedSpace(width:Int, height:Int):Point
	{
		var target:Point = null;
		for (i in 0...sTileReferences.length)
		{
			var r:TileReference = sTileReferences[i];
			var rightTest:Rectangle = new Rectangle(r.bitmapRect.right, r.bitmapRect.top, width, height);
			var bottomTest:Rectangle = new Rectangle(r.bitmapRect.left, r.bitmapRect.bottom, width, height);
			for (tr in sTileReferences)
			{
				if (tr == r)
					continue;
				if (!rightTest.intersects(tr.bitmapRect) && 
						rightTest.right < sTileset.bitmapData.width && rightTest.bottom < sTileset.bitmapData.height)
					target = new Point(rightTest.x, rightTest.y);
				if (!bottomTest.intersects(tr.bitmapRect) && 
						bottomTest.right < sTileset.bitmapData.width && bottomTest.bottom < sTileset.bitmapData.height)
					target = new Point(bottomTest.x, bottomTest.y);
			}
			if (target != null)
				break;
		}
		return target;
	}
	
	static private function expandToFit(width:Int, height:Int):Point
	{
		// NOTE: Perhaps add padding when expanding the bitmap to cut down on expensive memory operations
		var candidates:Array<Point> = [];
		if (sTileReferences.length == 0)
			candidates.push(new Point());
		for (i in 0...sTileReferences.length)
		{
			var r:TileReference = sTileReferences[i];
			var rightTest:Rectangle = new Rectangle(r.bitmapRect.right, r.bitmapRect.top, width, height);
			var bottomTest:Rectangle = new Rectangle(r.bitmapRect.left, r.bitmapRect.bottom, width, height);
			var rInvalid:Bool = false;
			var bInvalid:Bool = false;
			for (tr in sTileReferences)
			{
				if (tr == r)
					continue;
				if (!rInvalid && rightTest.intersects(tr.bitmapRect))
					rInvalid = true;
				if (!bInvalid && bottomTest.intersects(tr.bitmapRect))
					bInvalid = true;
				if (rInvalid && bInvalid)
					break;
			}
			if (!rInvalid)
				candidates.push(new Point(rightTest.x, rightTest.y));
			if (!bInvalid)
				candidates.push(new Point(bottomTest.x, bottomTest.y));
		}
		var target:Point = null;
		var expansionArea:Int = 0xFFFFFF;
		for (p in candidates)
		{
			var a:Float = (Math.max(sTileset.bitmapData.width, p.x + width) *
						Math.max(sTileset.bitmapData.height, p.y + height)) - 
						(sTileset.bitmapData.width * sTileset.bitmapData.height);
			if (a < expansionArea)
			{
				expansionArea = Math.ceil(a);
				target = p;
			}
		}
		var newBitmapData:BitmapData = new BitmapData(Math.ceil(Math.max(sTileset.bitmapData.width, target.x + width)),
						Math.ceil(Math.max(sTileset.bitmapData.height, target.y + height)), true, 0x00000000);
		newBitmapData.copyPixels(sTileset.bitmapData, sTileset.bitmapData.rect, new Point());
		sTileset.bitmapData.dispose();
		sTileset.bitmapData = newBitmapData;
		
		return target;
	}
	
	static private function repackTiles():Void
	{
		var refs:Array<TileReference> = sTileReferences.copy();
		refs.sort(function(a:TileReference, b:TileReference):Int
			{
				if (a.bitmapRect.width * a.bitmapRect.height > b.bitmapRect.width * b.bitmapRect.height)
					return 1;
				else if (a.bitmapRect.width * a.bitmapRect.height < b.bitmapRect.width * b.bitmapRect.height)
					return -1;
				else
					return 0;
			});
		var newRects:Array<Rectangle> = [];
		for (tr in refs)
			newRects.push(tr.bitmapRect.clone());
		
		newRects[0].x = 0;
		newRects[0].y = 0;
		var newWidth:Int = Math.floor(newRects[0].width);
		var newHeight:Int = Math.floor(newRects[0].height);
		
		for (i in 1...newRects.length)
		{
			var r:Rectangle = newRects[i];
			var candidates:Array<Point> = [];
			//	find which spots to the immediate right/bottom of the block are UNOCCUPIED
			for (j in 0...i)
			{
				var r2:Rectangle = newRects[j];
				var rightTest:Rectangle = new Rectangle(r2.right, r2.top, r.width, r.height);
				var bottomTest:Rectangle = new Rectangle(r2.left, r2.bottom, r.width, r.height);
				
				var rInvalid:Bool = false;
				var bInvalid:Bool = false;
				for (k in 0...j)
				{
					if (!rInvalid && rightTest.intersects(newRects[k]))
						rInvalid = true;
					if (!bInvalid && bottomTest.intersects(newRects[k]))
						bInvalid = true;
					if (rInvalid && bInvalid)
						break;
				}
				if (!rInvalid)
					candidates.push(new Point(rightTest.x, rightTest.y));
				if (!bInvalid)
					candidates.push(new Point(bottomTest.x, bottomTest.y));
			}
			
			var expWidth:Int = 0xFFFFFF;
			var expHeight:Int = 0xFFFFFF;
			for (p in candidates)
			{
				var confirm:Bool = false;
				var w = Math.max(newWidth, p.x + r.width);
				var h = Math.max(newHeight, p.y + r.height);
				var a = w * h;
				if (a < expWidth * expHeight)
					confirm = true;
				else if (a == expWidth * expHeight)
				{
					if (w > h && newHeight > newWidth)
						confirm = true;
					else if (h > w && newWidth > newHeight)
						confirm = true;
				}
				
				if (confirm)
				{
					expWidth = Math.ceil(w);
					expHeight = Math.ceil(h);
					r.x = p.x;
					r.y = p.y;
				}
			}
			newWidth = expWidth;
			newHeight = expHeight;
		}
		
		var newBitmapData:BitmapData = new BitmapData(Math.ceil(newWidth * REPACK_PADDING), 
			Math.ceil(newHeight * REPACK_PADDING), true, 0x00000000);
		for (i in 0...sTileReferences.length)
		{
			newBitmapData.copyPixels(sTileset.bitmapData, sTileReferences[i].bitmapRect, 
				new Point(newRects[i].x, newRects[i].y));
			sTileReferences[i].bitmapRect.copyFrom(newRects[i]);
		}
		sTileset.bitmapData.dispose();
		sTileset.bitmapData = newBitmapData;
		sPreviousEfficiency = getEfficiency();
	}
	
	static private function getEfficiency():Float
	{
		if (sTileReferences.length == 0)
			return 1;
		var area:Float = 0;
		for (tr in sTileReferences)
			area += tr.bitmapRect.width * tr.bitmapRect.height;
		return area / (sTileset.bitmapData.width * sTileset.bitmapData.height);
	}
}