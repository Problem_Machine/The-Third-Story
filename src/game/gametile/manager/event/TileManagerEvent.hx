package game.gametile.manager.event;
import flash.events.Event;
import game.gametile.manager.TileReference;

class TileManagerEvent extends Event
{
	static public inline var NEW_TILESET_BY_PATH:String = "TileManagerEvent.NewTilesetByPath";
	static public inline var NEW_TILESET_BY_DATA:String = "TileManagerEvent.NewTilesetByData";
	static public inline var UPDATED_TILESET_BY_PATH:String = "TileManagerEvent.UpdatedTilesetByPath";
	static public inline var UPDATED_TILESET_BY_DATA:String = "TileManagerEvent.UpdatedTilesetByData";
	
	public var tileReference:TileReference;
	public var path:String;

	public function new(type:String, tileReference:TileReference, path:String) 
	{
		super(type, bubbles, false);
		this.tileReference = tileReference;
		this.path = path;
	}
	
}