package game.gametile.manager;
import flash.geom.Point;
import flash.geom.Rectangle;

class TileReference
{
	public var bitmapRect(get, never):Rectangle;
	public var tileIDs(get, never):Array<Int>;
	
	private var mBitmapRect:Rectangle;
	private var mTileIDs:Array<Int>;
	private var mBitmapOffset:Point;
	private var mPath:String;

	public function new(bitmapRect:Rectangle, tileIDs:Array<Int>, bitmapOffset:Point, path:String) 
	{
		mBitmapRect = bitmapRect;
		mTileIDs = tileIDs;
		mBitmapOffset = bitmapOffset.clone();
		mPath = path;
	}
	
	public function tileX(id:Int, tileSize:Int):Int
	{
		return (mTileIDs.indexOf(id) % Math.floor(bitmapRect.width / tileSize));
	}
	public function tileY(id:Int, tileSize:Int):Int
	{
		return Math.floor(mTileIDs.indexOf(id) / Math.floor(bitmapRect.width / tileSize));
	}
	
	private inline function get_bitmapRect():Rectangle
	{	return mBitmapRect.clone();		}
	private inline function get_tileIDs():Array<Int>
	{	return mTileIDs.copy();		}
}