package game.gametile;
import com.problemmachine.tools.xml.XMLTools;
import game.gametile.manager.TileReference;

class GameTile
{
	public var reference(default, null):TileReference;
	public var id:Int;
	public var hFlip:Bool;
	public var vFlip:Bool;
	public var rot90(default, set):Int;

	public function new(id:Int, hFlip:Bool = false, vFlip:Bool = false, rot90:Int = 0) 
	{
		this.id = id;
		this.hFlip = hFlip;
		this.vFlip = vFlip;
		this.rot90 = rot90;
	}
	
	public inline function set_rot90(val:Int):Int
	{
		if (val % 4 != rot90)
		{
			rot90 = val % 4;
			while (rot90 < 0)
				rot90 += 4;
		}
		return val;
	}
	
	public inline function clone():GameTile
	{
		return new GameTile(id, hFlip, vFlip, rot90);
	}
	
	public inline function equals(comp:GameTile):Bool
	{
		return id == comp.id && hFlip == comp.hFlip && vFlip == comp.vFlip && rot90 == comp.rot90;
	}
	
	public function toString():String
	{
		return toXML().toString();
	}
	
	public function toXML():Xml
	{
		return Xml.parse(
			"<tile>\n" +
			"	<id>" + StringTools.hex(id, 6) + "</id>\n" +
			"	<hFlip>" + Std.string(hFlip) + "</hFlip>\n" +
			"	<vFlip>" + Std.string(vFlip) + "</vFlip>\n" +
			"	<rot90>" + Std.string(rot90) + "</rot90>\n" +
			"</tile>").firstElement();
	}
	
	static public function fromXML(xml:Xml):GameTile
	{
		return new GameTile(Std.parseInt(XMLTools.getVal(xml, "id")), XMLTools.getVal(xml, "hFlip") == Std.string(true),
			XMLTools.getVal(xml, "vFlip") == Std.string(true), Std.parseInt(XMLTools.getVal(xml, "rot90")));
	}
	
}