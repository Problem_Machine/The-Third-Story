package game.gametile.data;

enum SideType 
{
	NWall;
	NExit;
	NEDiagonal;
	NEDiagonalUpper;
	NEDiagonalCeiling;
	EWall;
	EExit;
	SEDiagonal;
	SEDiagonalUpper;
	SEDiagonalCeiling;
	SWall;
	SExit;
	SWDiagonal;
	SWDiagonalUpper;
	SWDiagonalCeiling;
	WWall;
	WExit;
	NWDiagonal;
	NWDiagonalUpper;
	NWDiagonalCeiling;
	VThinWall;
	HThinWall;
	Ceiling;
	
	OpenConnection;
	BlockConnection;
	AnyConnection;
}