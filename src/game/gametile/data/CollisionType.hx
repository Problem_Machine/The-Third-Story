package game.gametile.data;

enum CollisionType 
{
	Open;
	Block;
	DiagNE;
	DiagSE;
	DiagNW;
	DiagSW;
}