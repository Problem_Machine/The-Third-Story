package game.gametile.data;
import com.problemmachine.tools.file.FileTools;
import com.problemmachine.tools.math.IntMath;
import com.problemmachine.tools.xml.XMLTools;
import game.gametile.GameTile;

class TilesetData
{
	public var compatibility:Map<Int, Array<SideType>>;
	public var collision:Map<Int, CollisionType>;
	// a list of each tile on each side that the tiles are compatible with

	public function new()
	{
		compatibility = new Map<Int, Array<SideType>>();
		collision = new Map<Int, CollisionType>();
	}
	
	public function getTileByCollision(type:CollisionType, ?idStart:Int, ?idEnd:Int, random:Bool):GameTile
	{
		var preferred:Array<Int> = [];
		var other:Array<Int> = [];
		if (idStart == null)
		{
			idStart = 0xFFFFFF;
			for (i in collision.keys())
				idStart = IntMath.min(idStart, i);
		}
		if (idEnd == null)
		{
			idEnd = -1;
			for (i in collision.keys())
				idEnd = IntMath.max(idEnd, i);
		}
		for (i in collision.keys())
			if (i >= idStart && i < idEnd)
			{
				if (collision.get(i) == type)
				{
					if (random)
						preferred.push(i);
					else
						return new GameTile(i);
				}
				else if (canConvertCollision(collision.get(i), type))
					other.push(i);
			}
		
		if (preferred.length == 0 && other.length == 0)
			return null;
		if (preferred.length > 0)
			return new GameTile(preferred[Std.random(preferred.length)]);
		if (random)
			return convertCollision(other[Std.random(other.length)], type);
		else
			return convertCollision(other[0], type);
	}
	
	private function canConvertCollision(from:CollisionType, to:CollisionType):Bool
	{
		return ((from == CollisionType.DiagNE || from == CollisionType.DiagSE || from == CollisionType.DiagSW || from == CollisionType.DiagNW)
			&& (to == CollisionType.DiagNE || to == CollisionType.DiagSE || to == CollisionType.DiagSW || to == CollisionType.DiagNW));
	}
	private function convertCollision(id:Int, desiredCollision:CollisionType):GameTile
	{
		var t:GameTile = new GameTile(id);
		if (collision.get(id) == desiredCollision)
			return t;
		if (desiredCollision == Open || desiredCollision == Block)
			throw ("ERROR: Impossible to convert to " + Std.string(desiredCollision));
		switch(collision.get(id))
		{
			case DiagNE:
				if (desiredCollision == DiagSE)
					t.rot90 = 1;
				else if (desiredCollision == DiagSW)
					t.rot90 = 2;
				else
					t.rot90 = 3;
					
			case DiagSE:
				if (desiredCollision == DiagSW)
					t.rot90 = 1;
				else if (desiredCollision == DiagNW)
					t.rot90 = 2;
				else
					t.rot90 = 3;
					
			case DiagSW:
				if (desiredCollision == DiagNW)
					t.rot90 = 1;
				else if (desiredCollision == DiagNE)
					t.rot90 = 2;
				else
					t.rot90 = 3;
					
			case DiagNW:
				if (desiredCollision == DiagNE)
					t.rot90 = 1;
				else if (desiredCollision == DiagSE)
					t.rot90 = 2;
				else
					t.rot90 = 3;
					
			default:
				throw ("ERROR: Impossible to convert from " + Std.string(collision.get(id)));
		}
		return t;
	}
	
	public function getCompatibleTile(original:GameTile, north:GameTile, east:GameTile, south:GameTile, west:GameTile, 
		looseNorth:Bool, looseEast:Bool, looseSouth:Bool, looseWest:Bool, ?idStart:Int, ?idEnd:Int, random:Bool):GameTile
	{
		var col:CollisionType = collision.get(original.id);
		var candidates:Array<Int> = [];
		if (idStart == null)
		{
			idStart = 0xFFFFFF;
			for (i in compatibility.keys())
				idStart = IntMath.min(idStart, i);
		}
		if (idEnd == null)
		{
			idEnd = -1;
			for (i in compatibility.keys())
				idEnd = IntMath.max(idEnd, i);
		}
		// get the tile IDs we could replace the tile with without changing its collision
		for (i in collision.keys())
			if (i >= idStart && i < idEnd)
				if ((col == collision.get(i)) || canConvertCollision(collision.get(i), col))
					candidates.push(i);
					
		var confirmed:Array<GameTile> = [];
		for (i in 0...candidates.length)
		{
			var test:GameTile = new GameTile(candidates[i]);
			for (i in 0...16)
			{
				// iterate through all possible flip/rotation positions of tile
				// test each side against adjacent tiles sides
				//		If it fits, add version with flips and rotation to confirmed list
				test.rot90 = i % 4;
				test.hFlip = Math.floor(i / 4) == 1 || Math.floor(i / 4) == 3;
				test.vFlip = Math.floor(i / 4) >= 2;
				if (col != Block && col != Open)
				{
					var testCol:CollisionType = collision.get(test.id);
					for (j in 0...test.rot90)
						testCol = rotateCollisionRight(testCol);
					if (test.hFlip)
						testCol = flipCollisionHorizontally(testCol);
					if (test.vFlip)
						testCol = flipCollisionVertically(testCol);
					if (testCol != col)
						continue;
				}
				if (sideCompatible(test, north, 0, looseNorth) && sideCompatible(test, east, 1, looseEast) && 
						sideCompatible(test, south, 2, looseSouth) && sideCompatible(test, west, 3, looseWest))
				{
					if (random)
						confirmed.push(test.clone());
					else
						return test;
				}
			}
		}
		return confirmed[Std.random(confirmed.length)];
	}
	
	public function sideCompatible(a:GameTile, b:GameTile, side:Int, loose:Bool = false):Bool
	{
		if (a == null)
			return false;
		if (!compatibility.exists(a.id))
			loose = true;
		
		var aSide:Int = side;
		var bSide:Int = flipSide(aSide);
		if ((aSide == 0 || aSide == 2 && a.vFlip) || (aSide == 1 || aSide == 3 && a.hFlip))
			aSide = flipSide(aSide);
		for (i in 0...a.rot90)
			aSide = rotateSideLeft(aSide);
		var sa:SideType = compatibility.exists(a.id) ? compatibility.get(a.id)[aSide] : AnyConnection;
		if (loose)
			sa = makeSideTypeLoose(sa);
		else
		{
			if (a.hFlip)
				sa = flipSideTypeHorizontally(sa);
			if (a.vFlip)
				sa = flipSideTypeVertically(sa);
			for (i in 0...a.rot90)
				sa = rotateSideTypeRight(sa);
		}
			
		if (b == null)
			return sa == BlockConnection || sa == Ceiling;
		if (!compatibility.exists(b.id))
			loose = true;
		if ((bSide == 0 || bSide == 2 && b.vFlip) || (bSide == 1 || bSide == 3 && b.hFlip))
			bSide = flipSide(bSide);
		for (i in 0...b.rot90)
			bSide = rotateSideLeft(bSide);
		if (!loose && !compatibility.exists(b.id))
			return false;
		var sb:SideType = compatibility.exists(b.id) ? compatibility.get(b.id)[bSide] : AnyConnection;
		if (loose)
			sb = makeSideTypeLoose(sb);
		else
		{
			if (b.hFlip)
				sb = flipSideTypeHorizontally(sb);
			if (b.vFlip)
				sb = flipSideTypeVertically(sb);
			for (i in 0...b.rot90)
				sb = rotateSideTypeRight(sb);
		}
		
		var aCompatible:Bool = false;
		var bCompatible:Bool = false;
		switch(sa)
		{
			case SideType.OpenConnection:
				aCompatible = (collision.get(b.id) == CollisionType.Open);
			case SideType.BlockConnection:
				aCompatible = (collision.get(b.id) == CollisionType.Block);
			case SideType.AnyConnection:
				aCompatible = true;
			
			default:
				if (loose)
					aCompatible = true;
				else 
					aCompatible = (sb == sa);
		}
		switch(sb)
		{
			case SideType.OpenConnection:
				bCompatible = (collision.get(a.id) == CollisionType.Open);
			case SideType.BlockConnection:
				bCompatible = (collision.get(a.id) == CollisionType.Block);
			case SideType.AnyConnection:
				bCompatible = true;
			
			default:
				if (loose)
					bCompatible = true;
				else
					bCompatible = (sb == sa);
		}
		return (aCompatible && bCompatible);
	}
	
	private inline function flipSide(side:Int):Int
	{
		return switch(side)
			{
				case 0: 2;
				case 1: 3;
				case 2: 0;
				case 3: 1;
			default:
					throw "Integer " + side + " not recognized as a side (0-3 only)";
			}
	}
	
	private inline function rotateSideRight(side:Int):Int
	{	return ((side + 1) % 4);	}
	private inline function rotateSideLeft(side:Int):Int
	{
		if (--side < 0)
			side += 4;
		return side;	
	}
	static public inline function rotateCollisionRight(col:CollisionType):CollisionType
	{
		return switch(col)
		{
			case DiagNE:				DiagSE;
			case DiagSE:				DiagSW;
			case DiagSW:				DiagNW;
			case DiagNW:				DiagNE;
			default:						col;
		}
	}
	static public inline function rotateCollisionLeft(col:CollisionType):CollisionType
	{
		return switch(col)
		{
			case DiagNE:				DiagNW;
			case DiagNW:				DiagSW;
			case DiagSW:				DiagSE;
			case DiagSE:				DiagNE;
			default:						col;
		}
	}
	static public inline function flipCollisionHorizontally(col:CollisionType):CollisionType
	{
		return switch(col)
		{
			case DiagNE:				DiagNW;
			case DiagSE:				DiagSW;
			case DiagSW:				DiagSE;
			case DiagNW:				DiagNE;
			default:						col;
		}
	}
	static public inline function flipCollisionVertically(col:CollisionType):CollisionType
	{
		return switch(col)
		{
			case DiagNE:				DiagSE;
			case DiagNW:				DiagSW;
			case DiagSW:				DiagNW;
			case DiagSE:				DiagNE;
			default:						col;
		}
	}
	static public inline function rotateSideTypeRight(type:SideType):SideType
	{
		return switch(type)
		{
			case NWall:								EWall;
			case NExit:								EExit;
			case NEDiagonal:					SEDiagonal;
			case NEDiagonalUpper:			SEDiagonalUpper;
			case NEDiagonalCeiling:		SEDiagonalCeiling;
			case EWall:								SWall;
			case EExit:								SExit;
			case SEDiagonal:					SWDiagonal;
			case SEDiagonalUpper:			SWDiagonalUpper;
			case SEDiagonalCeiling:		SWDiagonalCeiling;
			case SWall:								WWall;
			case SExit:								WExit;
			case SWDiagonal:					NWDiagonal;
			case SWDiagonalUpper:			NWDiagonalUpper;
			case SWDiagonalCeiling:		NWDiagonalCeiling;
			case WWall:								NWall;
			case WExit:								NExit;
			case NWDiagonal:					NEDiagonal;
			case NWDiagonalUpper:			NEDiagonalUpper;
			case NWDiagonalCeiling:		NEDiagonalCeiling;
			case VThinWall:						HThinWall;
			case HThinWall:						VThinWall;
			
			default:									type;
		}
	}
	static public inline function rotateSideTypeLeft(type:SideType):SideType
	{
		return switch(type)
		{
			case NWall:								WWall;
			case NExit:								WExit;
			case NEDiagonal:					NWDiagonal;
			case NEDiagonalUpper:			NWDiagonalUpper;
			case NEDiagonalCeiling:		NWDiagonalCeiling;
			case EWall:								NWall;
			case EExit:								NExit;
			case SEDiagonal:					NEDiagonal;
			case SEDiagonalUpper:			NEDiagonalUpper;
			case SEDiagonalCeiling:		NEDiagonalCeiling;
			case SWall:								EWall;
			case SExit:								EExit;
			case SWDiagonal:					SEDiagonal;
			case SWDiagonalUpper:			SEDiagonalUpper;
			case SWDiagonalCeiling:		SEDiagonalCeiling;
			case WWall:								SWall;
			case WExit:								SExit;
			case NWDiagonal:					SWDiagonal;
			case NWDiagonalUpper:			SWDiagonalUpper;
			case NWDiagonalCeiling:		SWDiagonalCeiling;
			case VThinWall:						HThinWall;
			case HThinWall:						VThinWall;
			
			default:
				type;
		}
	}
	static public inline function flipSideTypeHorizontally(type:SideType):SideType
	{
		return switch(type)
		{
			case NEDiagonal:					NWDiagonal;
			case NEDiagonalUpper:			NWDiagonalUpper;
			case NEDiagonalCeiling:		NWDiagonalCeiling;
			case EWall:								WWall;
			case EExit:								WExit;
			case SEDiagonal:					SWDiagonal;
			case SEDiagonalUpper:			SWDiagonalUpper;
			case SEDiagonalCeiling:		SWDiagonalCeiling;
			case SWDiagonal:					SEDiagonal;
			case SWDiagonalUpper:			SEDiagonalUpper;
			case SWDiagonalCeiling:		SEDiagonalCeiling;
			case WWall:								EWall;
			case WExit:								EExit;
			case NWDiagonal:					NEDiagonal;
			case NWDiagonalUpper:			NEDiagonalUpper;
			case NWDiagonalCeiling:		NEDiagonalCeiling;
			
			default:									type;
		}
	}
	static public inline function flipSideTypeVertically(type:SideType):SideType
	{
		return switch(type)
		{
			case NWall:								SWall;
			case NExit:								SExit;
			case NEDiagonal:					SEDiagonal;
			case NEDiagonalUpper:			SEDiagonalUpper;
			case NEDiagonalCeiling:		SEDiagonalCeiling;
			case SEDiagonal:					NEDiagonal;
			case SEDiagonalUpper:			NEDiagonalUpper;
			case SEDiagonalCeiling:		NEDiagonalCeiling;
			case SWall:								NWall;
			case SExit:								NExit;
			case SWDiagonal:					NWDiagonal;
			case SWDiagonalUpper:			NWDiagonalUpper;
			case SWDiagonalCeiling:		NWDiagonalCeiling;
			case NWDiagonal:					SWDiagonal;
			case NWDiagonalUpper:			SWDiagonalUpper;
			case NWDiagonalCeiling:		SWDiagonalCeiling;
			
			default:									type;
		}
	}
	private inline function makeSideTypeLoose(type:SideType):SideType
	{
		// **NOTE**: I think the actual correct way to do this would be so each side has a collision compatibility and a graphics compatibility separate
		// Then loose test would just only use the collision compatibility
		// putting both into the same variable creates some weird situations, since they use fundamentally different logic
		// ie the collision check is based entirely on what the other tile is whereas the graphics check is based on how its particular edge lines up with ours
		// 		The Exit tiles and tiles meant to connect to diagonals are particular cases where this turns out strangely
		return switch(type)
		{
			case NWall, NEDiagonalUpper, NEDiagonalCeiling, EWall, SEDiagonalUpper, SEDiagonalCeiling,
						SWall, SWDiagonalUpper, SWDiagonalCeiling, WWall, NWDiagonalUpper, NWDiagonalCeiling, VThinWall, HThinWall, Ceiling:
				BlockConnection;
			case NEDiagonal, SEDiagonal, SWDiagonal, NWDiagonal, NExit, EExit, SExit, WExit:
				AnyConnection;
			
			default:									type;
		}
	}
	
	public function loadTileData(path:String, startAt:Int = 0):Void
	{
		var xml:Xml = Xml.parse(FileTools.immediateReadFrom(path).toString()).firstElement();
		for (x in xml.elementsNamed("tile"))
		{
			if (x.elementsNamed("type").hasNext())
				collision.set(startAt, Type.createEnum(CollisionType, XMLTools.getVal(x, "type")));
			else
				collision.set(startAt, CollisionType.Open);
				
			var arr:Array<SideType> = [AnyConnection, AnyConnection, AnyConnection, AnyConnection];
			if (x.elementsNamed("north").hasNext())
				arr[0] = Type.createEnum(SideType, XMLTools.getVal(x, "north"));
			if (x.elementsNamed("east").hasNext())
				arr[1] = Type.createEnum(SideType, XMLTools.getVal(x, "east"));
			if (x.elementsNamed("south").hasNext())
				arr[2] = Type.createEnum(SideType, XMLTools.getVal(x, "south"));
			if (x.elementsNamed("west").hasNext())
				arr[3] = Type.createEnum(SideType, XMLTools.getVal(x, "west"));
			
			compatibility.set(startAt, arr);
			
			++startAt;
		}
	}
	
	public function toXML():Xml
	{
		var xml:Xml = Xml.parse("<tilesetData/>").firstElement();
		for (i in compatibility.keys())
			xml.addChild(Xml.parse(
				"<compatibility id=\"" + i + "\">\n" +
				"	<north>" + Std.string(compatibility.get(i)[0]) + "</north>\n" + 
				"	<east>" + Std.string(compatibility.get(i)[1]) + "</east>\n" + 
				"	<south>" + Std.string(compatibility.get(i)[2]) + "</south>\n" + 
				"	<west>" + Std.string(compatibility.get(i)[3]) + "</west>\n" + 
				"</compatibility>").firstElement());
		for (i in collision.keys())
			xml.addChild(Xml.parse(
				"<collision id=\"" + i + "\">" + Std.string(collision.get(i)) + "</collision>").firstElement());
		return xml;
	}
	
	static public function fromXML(xml:Xml):TilesetData
	{
		var td:TilesetData = new TilesetData();
		for (x in xml.elementsNamed("compatibility"))
		{
			var arr:Array<SideType> = [Type.createEnum(SideType, XMLTools.getVal(x, "north")), Type.createEnum(SideType, XMLTools.getVal(x, "east")),
						Type.createEnum(SideType, XMLTools.getVal(x, "south")), Type.createEnum(SideType, XMLTools.getVal(x, "west"))];
			td.compatibility.set(Std.parseInt(x.get("id")), arr);
		}
		for (x in xml.elementsNamed("collision"))
			td.collision.set(Std.parseInt(x.get("id")), Type.createEnum(CollisionType, x.firstChild().nodeValue));
		return td;
	}
}