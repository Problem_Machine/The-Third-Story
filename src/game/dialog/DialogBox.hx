package game.dialog;

import flash.display.GradientType;
import flash.display.Sprite;
import flash.events.TimerEvent;
import flash.geom.Matrix;
import flash.Lib;
import flash.text.TextField;
import flash.text.TextFormat;
import flash.utils.Timer;
import haxe.ds.Vector.Vector;

class DialogBox extends Sprite
{
	private static inline var SPEED:Float = 15;
	private var mCurrentLines:Vector<String>;
	private var mDialogBuffer:Array<String>;
	private var mTextLines:Vector<TextField>;
	
	private var mDefaultTextFormat:TextFormat;
	private var mWaitingIcon:Sprite;
	private var mTimer:Timer;
	
	//private var mFastDisplaySound:Sound;
	//private var mDisplayCharSound:Sound;
	
	public var waiting:Bool = false;

	public function new(format:TextFormat) 
	{
		super();
		
		//mFastDisplaySound = Assets.getSound("sound/DialogSkip.wav");
		//mDisplayCharSound = Assets.getSound("sound/DialogChar.wav");
		
		mDialogBuffer = new Array<String>();
		var w:Float = 1024;//Lib.current.stage.width;
		var h:Float = 768 * 0.3;//Lib.current.stage.height * 0.3;
		
		mCurrentLines = new Vector<String>(3);
		mTextLines = new Vector<TextField>(3);
		for (i in 0...3)
		{
			mTextLines[i] = new TextField();
			mTextLines[i].selectable = false;
			mTextLines[i].mouseEnabled = false;
			mTextLines[i].defaultTextFormat = format;
			mTextLines[i].x = w * 0.05;
			mTextLines[i].y = (h * 0.075) + (i * h * 0.28);
			mTextLines[i].width = w * 0.9;
			mTextLines[i].height = h * 0.3;
			addChild(mTextLines[i]);
			mCurrentLines[i] = "";
		}
		
		var m:Matrix = new Matrix();
		m.rotate(-Math.PI * 0.5);
		m.scale(1, 0.2);
		graphics.beginGradientFill(GradientType.LINEAR, [0xFF000088, 0xFF000000], [0xFF, 0xFF], [0, 255], m);
		graphics.lineStyle(10, 0xFFFFFF, 0.6);
		graphics.drawRoundRect(0, 0, w, h, 10);
		graphics.endFill();
		graphics.lineStyle(5, 0xFFFFFF, 1);
		graphics.drawRoundRect(0, 0, w, h, 10);
		
		mWaitingIcon = new Sprite();
		mWaitingIcon.graphics.beginFill(0xFFFFFF);
		mWaitingIcon.graphics.lineStyle(5, 0xFFFFFF, 0.5);
		mWaitingIcon.graphics.moveTo( -w * 0.02, 0);
		mWaitingIcon.graphics.lineTo( w * 0.02, 0);
		mWaitingIcon.graphics.lineTo( 0, h * 0.05);
		mWaitingIcon.graphics.lineTo( -w * 0.02, 0);
		mWaitingIcon.graphics.endFill();
		mWaitingIcon.x = w * 0.5;
		mWaitingIcon.y = h * 0.9;
		mWaitingIcon.visible = false;
		addChild(mWaitingIcon);
		
		mTimer = new Timer(1 / SPEED);
		mTimer.addEventListener(TimerEvent.TIMER, timerListener);
		mTimer.start();
		this.visible = false;
	}
	
	public function queueDialog(dialog:String):Void
	{
		var lines:Array<String> = dialog.split("\n");
		while (lines.length > 0)
			mDialogBuffer.push(lines.shift());
		if (!this.visible)
			displayLines();
	}
	
	public function progress():Void
	{
		if (!waiting)
		{
			//if (!Mute.muted)
			//	mFastDisplaySound.play;
			for (i in 0...3)
				mTextLines[i].text = mCurrentLines[i];
			waiting = mWaitingIcon.visible = true;
			return;
		}
		if (mDialogBuffer.length == 0)
		{
			this.visible = false;
			waiting = false;
			return;
		}
		for (i in 0...3)
		{
			var line:String = mDialogBuffer.shift();
			if (line != null)
				mCurrentLines[i] = line;
			else
				mCurrentLines[i] = "";
			mTextLines[i].text = "";
		}
	}
	
	private function timerListener(e:TimerEvent):Void
	{
		var complete:Bool = true;
		for (i in 0...3)
		{
			if (mTextLines[i].text.length < mCurrentLines[i].length)
			{
				complete = false;
				//if (!Mute.muted)
				//	mDisplayCharSound.play();
				mTextLines[i].text += mCurrentLines[i].charAt(mTextLines[i].text.length);
				break;
			}
		}
		waiting = mWaitingIcon.visible = complete;
	}
	
	private function displayLines():Void
	{
		if (parent != null)
			parent.removeChild(this);
		Lib.current.stage.addChild(this);
		this.visible = true;
		progress();
	}
}