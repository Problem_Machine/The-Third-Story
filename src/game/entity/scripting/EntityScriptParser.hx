package game.entity.scripting;
import com.problemmachine.tools.math.IntMath;
import game.entity.GameEntity;
import game.entity.event.GameEntityEvent;

@:access(game.entity.GameEntity)
class EntityScriptParser
{
	static private inline var ID_CHARS:String = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_.";
	static private inline var ID_START_CHARS:String = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_";
	static private inline var OP_CHARS:String = "!%^*-+=/<>~";
	static private inline var NUM_CHARS:String = "0123456789.-";
	static private var KEYWORDS(get, never):Array<String>;
	
	private function new()
	{	}
	
	static private inline function get_KEYWORDS():Array<String>
	{
		return ["active", "break", "case", "catch", "continue", "default", "do", "else", "false", "for", 
			"global", "if", "in", "null", "return", "switch", "this", "throw", "trace", "true", "var", "while"];
	}

	static public function runScript(e:GameEntity, varName:String):String
	{
		
		var vars = e.mLocalVariables;
		var stack:Array<Dynamic> = [{source:vars, key:varName}];
		execute([], stack, new Map<String, Dynamic>());
		if (stack[stack.length - 1] == Exception)
		{
			var tInfo:Dynamic = null;
			switch(cast(stack.pop(), ScriptComm))
			{
				case Exception(info):		tInfo = info;
				default:
			}
			throw ("ERROR: Uncaught exception: " + Std.string(tInfo));
		}
		return stack.length > 0 ? Std.string(valueOf(stack.pop())) : "";
	}
	
	static private inline function execute(entVars:Array<Map<String, Dynamic>>, stack:Array<Dynamic>, ?variables:Map<String, Dynamic>):Void
	{
		sLastWasValue = false;
		var vars = variables;
		var value:Dynamic = stack.pop();
		var scopeChanged:Bool = false;
		if (entVars.length == 0 || (Reflect.isObject(value) && entVars[entVars.length - 1] != value.source))
		{
			scopeChanged = true;
			vars = new Map<String, Dynamic>();
			entVars.push(value.source);
		}
		var tokens:ScriptToken = tokenize(Std.string(valueOf(value)));
		interpretToken(entVars, tokens, stack, vars);
		if (stack[stack.length - 1] == Return)
			stack.pop();
		if (scopeChanged)
			entVars.pop();
	}
	
	static private function interpretToken(entVars:Array<Map<String, Dynamic>>, t:ScriptToken, stack:Array<Dynamic>, variables:Map<String, Dynamic>):Void
	{
		if (t == null)
			return;
		if (stack[stack.length - 1] == Exception)
		{
			if (Type.enumIndex(t) != Type.enumIndex(Catch(null)))
				return;
		}
		else if (stack[stack.length - 1] == Return)
			return;
			
		switch(t)
		{
			case Block(contents):
				for (token in contents)
				{
					interpretToken(entVars, token, stack, variables);
					if (stack[stack.length - 1] == Continue || stack[stack.length - 1] == Break || stack[stack.length - 1] == Return)
					{
						stack.pop();
						break;
					}
				}
				
			case VarDec(name):
				variables.set(name, null);
				
			case Conditional(blockA, blockB):
				var v = stack.pop();
				if (valueOf(v) == true)
					interpretToken(entVars, blockA, stack, variables)
				else if (valueOf(v) == false)
				{
					if (blockB != null)
						interpretToken(entVars, blockB, stack, variables);
				}
				else
					throw ("ERROR: Value " + Std.string(valueOf(v)) + " must be boolean");
					
			case Loop(block, test):
				while (valueOf(stack.pop()) == true)
				{
					interpretToken(entVars, block, stack, variables);
					var br:Bool = stack[stack.length - 1] == Break;
					if (stack[stack.length - 1] == Break || stack[stack.length - 1] == Continue)
						stack.pop();
					if (br)
						stack.push(false);
					else
						interpretToken(entVars, test, stack, variables);
				}
				
			case ForRangeOf(block, name, low, high):
				var varExisted:Bool = variables.exists(name);
				var varOldValue:Dynamic = variables.get(name);
				interpretToken(entVars, high, stack, variables);
				interpretToken(entVars, low, stack, variables);
				var it:Iterator<Dynamic> = valueOf(stack.pop())...valueOf(stack.pop());
				for (d in it)
				{
					variables.set(name, d);
					interpretToken(entVars, block, stack, variables);
					var br:Bool = stack[stack.length - 1] == Break;
					if (stack[stack.length - 1] == Break || stack[stack.length - 1] == Continue)
						stack.pop();
					if (br)
						break;
				}
				
				if (varExisted)
					variables.set(name, varOldValue);
				else
					variables.remove(name);
				
			case ForEachIn(block, name, container):
				var varExisted:Bool = variables.exists(name);
				var varOldValue:Dynamic = variables.get(name);
				interpretToken(entVars, container, stack, variables);
				var v = stack.pop();
				var it:Iterator<Dynamic> = valueOf(v).iterator();
				for (d in it)
				{
					variables.set(name, d);
					interpretToken(entVars, block, stack, variables);
					var br:Bool = stack[stack.length - 1] == Break;
					if (stack[stack.length - 1] == Break || stack[stack.length - 1] == Continue)
						stack.pop();
					if (br)
						break;
				}
				
				if (varExisted)
					variables.set(name, varOldValue);
				else
					variables.remove(name);
					
			case Switch(cases, defaultCase):
				var foundCase:Bool = false;
				var v = stack.pop();
				for (c in cases)
					if (c.values.indexOf(valueOf(v)) >= 0)
					{
						interpretToken(entVars, c.block, stack, variables);
						foundCase = true;
						break;
					}
				if (!foundCase && defaultCase != null)
					interpretToken(entVars, defaultCase, stack, variables);
				
			case Execute:
				execute(entVars, stack, variables);
				
			case Trace(block):
				interpretToken(entVars, block, stack, variables);
				trace(valueOf(stack.pop()));
				
			case Throw(block):
				interpretToken(entVars, block, stack, variables);
				stack.push(Exception(valueOf(stack.pop())));
				
			case Catch(block):
				if (stack[stack.length - 1] == Exception)
				{
					stack.pop();
					interpretToken(entVars, block, stack, variables);
				}
				
			case Resolve(path):
				var varPath:String = path.indexOf(".") > 0 ? path.substring(0, path.lastIndexOf(".")) : null;
				var target:String = varPath != null ? path.substring(path.lastIndexOf(".") + 1) : path;
				var resolveVars = varPath != null ? getVariables(varPath, entVars, variables) : variables;
				if (!resolveVars.exists(target))
					throw ("ERROR: Identifier " + target + " does not exist in " + varPath);
				stack.push({source:resolveVars, key:target});
				
			case Literal(val):
				stack.push(val);
				
			case Assign:
				var r = stack.pop();
				var l = stack[stack.length - 1];
				if (!Reflect.isObject(l))	throw ("ERROR: Cannot assign to literal value " + Std.string(l));
				l.source.set(l.key, valueOf(r));
				
			case Negative:
				stack.push( -valueOf(stack.pop()));
				
			case MathAdd:
				var r = valueOf(stack.pop());
				stack.push(valueOf(stack.pop()) + r);
				
			case MathSubtract:
				var r = valueOf(stack.pop());
				stack.push(valueOf(stack.pop()) - r);
				
			case MathDivide:
				var r = valueOf(stack.pop());
				stack.push(valueOf(stack.pop()) / r);
				
			case MathModulo:
				var r = valueOf(stack.pop());
				stack.push(valueOf(stack.pop()) % r);
				
			case MathMultiply:
				var r = valueOf(stack.pop());
				stack.push(valueOf(stack.pop()) * r);
				
			case BitAnd:
				var r = valueOf(stack.pop());
				stack.push(valueOf(stack.pop()) & r);
				
			case BitOr:
				var r = valueOf(stack.pop());
				stack.push(valueOf(stack.pop()) | r);
				
			case BitXOr:
				var r = valueOf(stack.pop());
				stack.push(valueOf(stack.pop()) ^ r);
				
			case BitShiftRight:
				var r = valueOf(stack.pop());
				stack.push(valueOf(stack.pop()) >> r);
				
			case BitShiftLeft:
				var r = valueOf(stack.pop());
				stack.push(valueOf(stack.pop()) << r);
				
			case BitShiftRightStrict:
				var r = valueOf(stack.pop());
				stack.push(valueOf(stack.pop()) >>> r);
				
			case BoolNot:
				stack.push(!valueOf(stack.pop()));
				
			case BoolEquals:
				var r = valueOf(stack.pop());
				stack.push(valueOf(stack.pop()) == r);
				
			case BoolNotEquals:
				var r = valueOf(stack.pop());
				stack.push(valueOf(stack.pop()) != r);
				
			case BoolGreater:
				var r:Float = cast valueOf(stack.pop());
				stack.push(valueOf(stack.pop()) > r);
				
			case BoolGreaterEquals:
				var r:Float = cast valueOf(stack.pop());
				stack.push(valueOf(stack.pop()) >= r);
				
			case BoolLess:
				var r:Float = cast valueOf(stack.pop());
				stack.push(valueOf(stack.pop()) < r);
				
			case BoolLessEquals:
				var r:Float = cast valueOf(stack.pop());
				stack.push(valueOf(stack.pop()) <= r);
				
			case BoolAnd:
				var r = valueOf(stack.pop());
				stack.push(valueOf(stack.pop()) && r);
				
			case BoolOr:
				var r = valueOf(stack.pop());
				stack.push(valueOf(stack.pop()) || r);
				
			case MathAddAssign:
				var r = valueOf(stack.pop());
				var l = stack[stack.length - 1];
				if (!Reflect.isObject(l))	throw ("ERROR: Cannot assign to literal value " + Std.string(l));
				l.source.set(l.key, valueOf(l) + r);
				
			case MathSubtractAssign:
				var r = valueOf(stack.pop());
				var l = stack[stack.length - 1];
				if (!Reflect.isObject(l))	throw ("ERROR: Cannot assign to literal value " + Std.string(l));
				l.source.set(l.key, valueOf(l) - r);
				
			case MathDivideAssign:
				var r = valueOf(stack.pop());
				var l = stack[stack.length - 1];
				if (!Reflect.isObject(l))	throw ("ERROR: Cannot assign to literal value " + Std.string(l));
				l.source.set(l.key, valueOf(l) / r);
				
			case MathModuloAssign:
				var r = valueOf(stack.pop());
				var l = stack[stack.length - 1];
				if (!Reflect.isObject(l))	throw ("ERROR: Cannot assign to literal value " + Std.string(l));
				l.source.set(l.key, valueOf(l) % r);
				
			case MathMultiplyAssign:
				var r = valueOf(stack.pop());
				var l = stack[stack.length - 1];
				if (!Reflect.isObject(l))	throw ("ERROR: Cannot assign to literal value " + Std.string(l));
				l.source.set(l.key, valueOf(l) * r);
				
			case BitAndAssign:
				var r = valueOf(stack.pop());
				var l = stack[stack.length - 1];
				if (!Reflect.isObject(l))	throw ("ERROR: Cannot assign to literal value " + Std.string(l));
				l.source.set(l.key, valueOf(l) & r);
				
			case BitOrAssign:
				var r = valueOf(stack.pop());
				var l = stack[stack.length - 1];
				if (!Reflect.isObject(l))	throw ("ERROR: Cannot assign to literal value " + Std.string(l));
				l.source.set(l.key, valueOf(l) | r);
				
			case BitXOrAssign:
				var r = valueOf(stack.pop());
				var l = stack[stack.length - 1];
				if (!Reflect.isObject(l))	throw ("ERROR: Cannot assign to literal value " + Std.string(l));
				l.source.set(l.key, valueOf(l) ^ r);
				
			case BitShiftRightAssign:
				var r = valueOf(stack.pop());
				var l = stack[stack.length - 1];
				if (!Reflect.isObject(l))	throw ("ERROR: Cannot assign to literal value " + Std.string(l));
				l.source.set(l.key, valueOf(l) >> r);
				
			case BitShiftLeftAssign:
				var r = valueOf(stack.pop());
				var l = stack[stack.length - 1];
				if (!Reflect.isObject(l))	throw ("ERROR: Cannot assign to literal value " + Std.string(l));
				l.source.set(l.key, valueOf(l) << r);
				
			case BitShiftRightStrictAssign:
				var r = valueOf(stack.pop());
				var l = stack[stack.length - 1];
				if (!Reflect.isObject(l))	throw ("ERROR: Cannot assign to literal value " + Std.string(l));
				l.source.set(l.key, valueOf(l) >>> r);
				
			case PreIncrement, PostIncrement, PreDecrement, PostDecrement:
				var v = stack.pop();
				if (!Reflect.isObject(v))	
					throw ("ERROR: Cannot " + ((t == PreIncrement || t == PostIncrement ) ? "increment" : "decrement") + " literal value " + v);
				var n = valueOf(v);
				if (t == PreIncrement || t == PreDecrement)
					stack.push(n);
				if (t == PreDecrement || t == PostDecrement)
					--n;
				else
					++n;
				v.source.set(v.key, n);
				if (t == PostIncrement || t == PostDecrement)
					stack.push(n);
				
			case Create(name):
				var e:GameEntity = new GameEntity(name);
				GameEntity.sDispatcher.dispatchEvent(new GameEntityEvent(e, GameEntityEvent.NEW_ENTITY, null));
				stack.push(e.name);
				
			case Delete(name):
				var e:GameEntity = GameEntity.getEntity(name);
				if (e != null)
				{
					GameEntity.sDispatcher.dispatchEvent(new GameEntityEvent(e, GameEntityEvent.DELETE_ENTITY, null));
					e.die();
					e.dispose();
				}
				
			case Pop:
				stack.pop();
		}
	}
	
	static private function getVariables(path:String, entVars:Array<Map<String, Dynamic>>, variables:Map<String, Dynamic>):Map<String, Dynamic>
	{
		var currentVariables:Map<String, Dynamic> = variables;
		var components:Array<String> = path.split(".");
		var i:Int = 0;
		if (components[0].toLowerCase() == "this")
		{
			currentVariables = entVars[entVars.length - 1];
			++i;
		}
		else if (components[0].toLowerCase() == "active")
		{
			currentVariables = entVars[0];
			++i;
		}
		else if (components[0] == "global")
		{
			currentVariables = GameEntity.sGlobalVariables;
			++i;
		}
		
		while (i < components.length)
		{
			var target:String = currentVariables.get(components[i]);
			if (target == null)
				throw ("ERROR: Target " + path + " unresolvable: Could not find property " + components[i]);
			var e:GameEntity = GameEntity.getEntity(target);
			if (e == null)
				throw ("ERROR: Target " + path + " unresolvable: Could not find entity named " + target);
			currentVariables = e.mLocalVariables;
			
			++i;
		}
		return currentVariables;
	}
	
	static private var sLastWasValue:Bool = false;
	static private function tokenize(s:String):ScriptToken
	{
		var i:Int = 0;
		var tokens:Array<ScriptToken> = [];
		var tempTokens:Array<Dynamic> = [];
		while (i < s.length)
		{
			// tempTokens contain all the tokens in the statement, along with parentheses, still in infix order
			// keywords are interpreted into token arrays so they can be rearranged properly and then broken down later
			
			var statementComplete:Bool = false;
			// the statement is complete once we either hit two values in a row or we reach a semicolon.
			// once the statement is complete we convert it from infix to reverse polish notation and add it to the token array
			
			var char:String = s.charAt(i);
			var word:String = null;
			if (ID_START_CHARS.indexOf(char) >= 0)
				word = s.substring(i, findWordEnd(s, i));
			if (word != null)
			{
				if (sLastWasValue)
				{
					statementComplete = true;
				}
				else
				{
					if (KEYWORDS.indexOf(word) >= 0)
						i = tokenizeKeyword(s, i, tempTokens);
					else
					{
						sLastWasValue = true;
						tempTokens.push(Resolve(word));
						i += word.length;
					}
				}
			}
			else if (OP_CHARS.indexOf(char) >= 0)
			{
				var op:String = char;
				while (OP_CHARS.indexOf(s.charAt(++i)) >= 0)
					op += s.charAt(i);
				var binOp:Bool = true;
				switch(op)
				{
					case "=":
						if (tempTokens[tempTokens.length - 1] != Resolve)
							throw ("ERROR: Can only assign to identifiers");
						tempTokens.push(Assign);
					
					case "++":
						if (sLastWasValue)
						{
							tempTokens.push(PostIncrement);
							if (tempTokens[tempTokens.length - 1] != Resolve)
								throw ("ERROR: Can only increment to identifiers");
						}
						else
							tempTokens.push(PreIncrement);
						binOp = false;
						
					case "--":
						if (sLastWasValue)
						{
							tempTokens.push(PostDecrement);
							if (tempTokens[tempTokens.length - 1] != Resolve)
								throw ("ERROR: Can only decrement to identifiers");
						}
						else
							tempTokens.push(PreDecrement);
						binOp = false;
					case "-":
						if (sLastWasValue)
							tempTokens.push(MathSubtract);
						else
						{
							tempTokens.push(Negative);
							binOp = false;
						}
						
					case "+":
						tempTokens.push(MathAdd);
					case "/":
						tempTokens.push(MathDivide);
					case "%":
						tempTokens.push(MathModulo);
					case "*":
						tempTokens.push(MathMultiply);
					case "+=":
						tempTokens.push(MathAddAssign);
					case "-=":
						tempTokens.push(MathSubtractAssign);
					case "/=":
						tempTokens.push(MathDivideAssign);
					case "%=":
						tempTokens.push(MathModuloAssign);
					case "*=":
						tempTokens.push(MathMultiplyAssign);
					case "&":
						tempTokens.push(BitAnd);
					case "|":
						tempTokens.push(BitOr);
					case "^":
						tempTokens.push(BitXOr);
					case ">>":
						tempTokens.push(BitShiftRight);
					case "<<":
						tempTokens.push(BitShiftLeft);
					case ">>>":
						tempTokens.push(BitShiftRightStrict);
					case "&=":
						tempTokens.push(BitAndAssign);
					case "|=":
						tempTokens.push(BitOrAssign);
					case "^=":
						tempTokens.push(BitXOrAssign);
					case ">>=":
						tempTokens.push(BitShiftRightAssign);
					case "<<=":
						tempTokens.push(BitShiftLeftAssign);
					case ">>>=":
						tempTokens.push(BitShiftRightStrictAssign);
					case "!":
						tempTokens.push(BoolNot);
						binOp = false;
					case "==":
						tempTokens.push(BoolEquals);
					case "!=":
						tempTokens.push(BoolNotEquals);
					case ">":
						tempTokens.push(BoolGreater);
					case ">=":
						tempTokens.push(BoolGreaterEquals);
					case "<":
						tempTokens.push(BoolLess);
					case "<=":
						tempTokens.push(BoolLessEquals);
					case "&&":
						tempTokens.push(BoolAnd);
					case "||":
						tempTokens.push(BoolOr);
					case "?":
						// TODO
					default:
						throw ("ERROR: Unrecognized operator \"" + op + "\"");
				}
				if (binOp)
				{
					if (!sLastWasValue)
						throw ("ERROR: Two binary operators in a row!");
					sLastWasValue = false;
				}
			}
			else
			{
				switch(char)
				{
					case " ", "\n", "\t", "\r":
						++i;
						
					case ";":
						statementComplete = true;
						++i;
					
					case "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "-", ".":
						var start:Int = i;
						var decimal:Bool = char == ".";
						while (NUM_CHARS.indexOf(char = s.charAt(++i)) >= 0)
						{
							if (char == "-")
								break;
							if (char == ".")
							{
								if (decimal)
									throw "ERROR: Malformed number";
								decimal = true;
							}
						}
						tempTokens.push(Literal(decimal ? Std.parseFloat(s.substring(start, i)) : Std.parseInt(s.substring(start, i))));
						sLastWasValue = true;
						
					case "\"", "\'":
						var end:Int = findClosing(s, i);
						tempTokens.push(Literal(s.substring(i + 1, end)));
						i = end + 1;
						sLastWasValue = true;
						
					case "{":
						var end:Int = findClosing(s, i);
						if (end < 0)
							throw ("ERROR: Unclosed brackets");
						tempTokens.push(tokenize(s.substring(i + 1, end)));
						i = end + 1;
						
					case "(":
						var end:Int = i + 1;
						while (StringTools.isSpace(s, end))
							++end;
						if (end >= s.length)
							throw("ERROR: No closing parenthesis found");
						if (s.charAt(end) != ")")
						{
							tempTokens.push("(");
							++i;
						}
						else
						{
							tempTokens.push(Execute);
							i = end + 1;
						}
					
					case ")":
						tempTokens.push(")");
						++i;
				}
			}
			
			if (i >= s.length)
				statementComplete = true;
			
			if (statementComplete)
			{
				// convert from infix to reverse polish and append to main token array
				// at the end of each statement the stack should only contain one value.
				var opStack:Array<Dynamic> = [];

				for (d in tempTokens)
				{
					if (d == Resolve || d == Literal)
					{
						tokens.push(d);
					}
					else if (Std.is(d, Array))
					{
						for (t in cast (d, Array<Dynamic>))
							tokens.push(cast t);
					}
					else if (d == "(")
					{
						opStack.push(d);
					}
					else if (d == ")")
					{
						var top = opStack.pop();
						while (top != "(")
						{
							tokens.push(cast top);
							top = opStack.pop();
						}
					}
					else
					{
						while ((opStack.length > 0) && opStack[opStack.length - 1] != "(" && (precedence(opStack[opStack.length - 1]) >= precedence(d)))
							tokens.push(opStack.pop());
						opStack.push(d);
					}
				}

				while (opStack.length > 0)
					tokens.push(opStack.pop());
				
				/*if (tempTokens.length > 0)
					tokens.push(Pop);*/
					
				tempTokens = [];
				statementComplete = false;
			}
		}
			/*else for (key in declared.keys())
			{
				if (s.substr(i, key.length) == key)
					
				// check if we can match to any declared variables
				// add this back in if we want compile-time checks on variable access and/or types
			}*/
		
		for (i in 0...tokens.length)
			if (tokens[tokens.length - 1 - i] == null)
				tokens.splice(tokens.length - 1 - i, 1);
		
		if (tokens.length == 1)
			return tokens[0];
		else if (tokens.length == 0)
			return null;
		else
			return Block(tokens);
	}
	
	static private function tokenizeKeyword(s:String, i:Int, tempTokens:Array<Dynamic>):Int
	{
		var word:String = s.substring(i, findWordEnd(s, i));
		i += word.length;
		var end:Int = i;
		switch(word)
		{
			case "break":
				tempTokens.push(Literal(Break));
				
			case "catch":
				end = s.indexOf(";", i);
				if (s.indexOf("{", i) < end)
				{
					i = s.indexOf("{", i);
					end = findClosing(s, i);
				}
				tempTokens.push(Catch(tokenize(s.substring(i + 1, end))));
			
			case "continue":
				tempTokens.push(Literal(Continue));
			
			case "do":
				end = s.indexOf(";", i);
				if (s.indexOf("{", i) < end)
				{
					i = s.indexOf("{", i);
					end = findClosing(s, i);
				}
				var block:String = s.substring(i + 1, end);
				i = s.indexOf("while", end);
				if (StringTools.trim(s.substring(end + 1, i)).length > 0)
					throw ("ERROR: Unexpected characters between \"do\" and \"while\"");
				i = s.indexOf("(", i + 5);
				end = findClosing(s, i);
				var test:String = s.substring(i + 1, end);
				tempTokens.push(tokenize(block));
				tempTokens.push(tokenize(test));
				tempTokens.push(Loop(tokenize(block), tokenize(test)));
				
			case "false":
				tempTokens.push(Literal(false));
			
			case "for":
				i = s.indexOf("(", end);
				end = findClosing(s, i);
				var nameStart:Int = i + 1;
				while (StringTools.isSpace(s, nameStart))
					++nameStart;
				var nameEnd:Int = findWordEnd(s, nameStart);
				var name:String = s.substring(nameStart, nameEnd);
				
				if (ID_START_CHARS.indexOf(s.charAt(nameStart)) < 0)
					throw ("ERROR: Name " + name + " is not valid variable declaration");	
				
				i = s.indexOf("in ", nameEnd) + 3;
				var dots:Int = s.indexOf("...", i);
				
				var blockStart:Int = end;
				end = s.indexOf(";", blockStart);
				if (s.indexOf("{", blockStart) < end)
					end = s.indexOf("}", blockStart);
				blockStart = s.indexOf("{", blockStart);
				var block:ScriptToken = tokenize(s.substring(blockStart, end));
				
				if (dots < 0 || dots > end)
				{
					while (StringTools.isSpace(s, i))
						++i;
					nameStart = i;
					nameEnd = findWordEnd(s, i);
					tempTokens.push(ForEachIn(block, name, Resolve(s.substring(nameStart, nameEnd))));
				}
				else
				{
					var low:String = StringTools.trim(s.substring(i, dots));
					var lToken:ScriptToken = (NUM_CHARS.indexOf(low.charAt(0)) >= 0 ? Literal(Std.parseInt(low)) : Resolve(low));
					i = dots + 3;
					while (NUM_CHARS.indexOf(s.charAt(i)) >= 0 || ID_CHARS.indexOf(s.charAt(i)) >= 0 || StringTools.isSpace(s, i))
						++i;
					var high:String = StringTools.trim(s.substring(dots + 3, i));
					var hToken:ScriptToken = (NUM_CHARS.indexOf(high.charAt(0)) >= 0 ? Literal(Std.parseInt(high)) : Resolve(high));
					tempTokens.push(ForRangeOf(block, name, lToken, hToken));
				}
				
			case "if":
				var look:Int = s.indexOf(")", end + 1);
				tempTokens.push(tokenize(s.substring(s.indexOf("(", end) + 1, look)));
				
				end = s.indexOf(";", look);
				if (s.indexOf("{", look) < end)
					end = findClosing(s, s.indexOf("{", look));
				var blockA:ScriptToken = tokenize(s.substring(look + 1, end));
				look = end + 1;
				
				end = s.indexOf(";", look);
				look = s.indexOf("else", look);
				var blockB:ScriptToken = null;
				if (look < end && look > 0 && ID_CHARS.indexOf(s.charAt(look + 4)) < 0)
				{
					look += 4;
					if (s.indexOf("{", look) < end)
					{
						look = s.indexOf("{", look);
						end = findClosing(s, look);
					}
					blockB = tokenize(s.substring(look, end));
				}
				tempTokens.push(Conditional(blockA, blockB));
			
			case "null":
				tempTokens.push(Literal(null));
				
			case "return":
				tempTokens.push(Literal(Return));
			
			case "switch":
				var look:Int = s.indexOf("(", i);
				var test:String = StringTools.trim(s.substring(look + 1, findClosing(s, look)));
				if (ID_START_CHARS.indexOf(test.charAt(0)) < 0)
					throw ("ERROR: Invalid characters found in identifier " + test);
				for (j in 0...test.length)
					if (ID_CHARS.indexOf(test.charAt(j)) < 0)
						throw ("ERROR: Invalid characters found in identifier " + test);
				tempTokens.push(Resolve(test));
				
				var cases:Array<ScriptCase> = [];
				
				look = s.indexOf("{", i);
				end = findClosing(s, look);
				var defaultCase:ScriptToken = null;
				look = s.indexOf("default");
				if (look >= 0 && look < end)
					defaultCase = tokenize(s.substring(s.indexOf(":", look) + 1, 
						IntMath.min(IntMath.min(s.indexOf("case", look), s.indexOf("default", look)), end)));
				look = i;
				while ((look = s.indexOf("case", look)) + 4 < end)
				{
					var vStrings:Array<String> = StringTools.trim(s.substring(look, s.indexOf(":", look))).split(",");
					var vals:Array<Dynamic> = [];
					for (str in vStrings)
					{
						str = StringTools.trim(str);
						if (str.charAt(0) == "\"" || str.charAt(0) == "\'")
							vals.push(str.substring(1, str.indexOf(str.charAt(0), 1)));
						else if (str.indexOf(".") >= 0)
							vals.push(Std.parseFloat(str));
						else
							vals.push(Std.parseInt(str));
					}
					var c:ScriptCase = {values:vals, 
						block:tokenize(s.substring(s.indexOf(":", look) + 1, 
							IntMath.min(IntMath.min(s.indexOf("case", look), s.indexOf("default", look)), end)))};
					cases.push(c);
				}
				tempTokens.push(Switch(cases, defaultCase));
			
			case "throw":
				var look:Int = s.indexOf("(", i);
				end = findClosing(s, look);
				tempTokens.push(Throw(tokenize(StringTools.trim(s.substring(look + 1, end++)))));
			
			case "trace":
				var look:Int = s.indexOf("(", i);
				end = findClosing(s, look);
				tempTokens.push(Trace(tokenize(StringTools.trim(s.substring(look + 1, end++)))));
			
			case "true":
				tempTokens.push(Literal(true));
			
			case "var":
				var nameStart:Int = end;
				while (StringTools.isSpace(s, nameStart))
					++nameStart;
				var nameEnd:Int = findWordEnd(s, nameStart);
				var name:String = s.substring(nameStart, nameEnd);
				if (ID_START_CHARS.indexOf(s.charAt(nameStart)) < 0)
					throw ("ERROR: Name " + name + " is not valid variable declaration");
				
				tempTokens.push(VarDec(name));
				// we're not moving the end out past the declaration because we want the statement (sans 'var' declaration)
				// 	to be parsed again as an access that loads the variable into value or as an assignment
			
			case "while":
				i = s.indexOf("(", i);
				end = findClosing(s, i);
				var test:String = s.substring(i + 1, end);
				i = end;
				end = s.indexOf(";", i);
				if (s.indexOf("{", i) < end)
				{
					i = s.indexOf("{", i);
					end = findClosing(s, i);
				}
				var block:ScriptToken = tokenize(s.substring(i + 1, end));
				tempTokens.push(Loop(block, tokenize(test)));
				
			case "create":
				// todo
				
			case "delete":
				// todo
				
			default:
				throw("Error: Unrecognized Keyword");
		}
		return end;
	}
	
	static private inline function valueOf(d:Dynamic):Dynamic
	{
		if (Reflect.isObject(d) && !Std.is(d, String))
			return d.source.get(d.key);
		else
			return d;
	}
	
	static private inline function findClosing(s:String, openIndex:Int):Int
	{
		var i:Int = openIndex;
		var opening:Int = s.charCodeAt(openIndex);
		var closing:Int = switch(opening)
			{
				case "{".code:		"}".code;
				case "(".code:		")".code;
				case "[".code:		"]".code;
				case "<".code:		">".code;
				case "\"".code:		"\"".code;
				case "\'".code:		"\'".code;
				case "`".code:		"`".code;
				default:	-1;
			}
		if (closing >= 0)
		{
			var nested:Int = 1;
			var char:Int = 0;
			// the order of the conditions in this while is important: If nested <= 0 the latter statement is never tested/executed
			while (nested > 0 && ++i < s.length)	
			{
				char = s.charCodeAt(i);
				if (char == opening && opening != closing)
					++nested;
				else if (char == closing)
					--nested;
			}
		}
		if (closing < 0 || i >= s.length)
			return -1
		else
			return i;
	}
	
	static private inline function findWordEnd(s:String, wordStartIndex:Int):Int
	{
		var i:Int = wordStartIndex;
		while (i < s.length && ID_CHARS.indexOf(s.charAt(i)) >= 0)
			++i;
		return i;
	}
	
	static private inline function precedence(op:ScriptToken):Int
	{
		return switch(op)
		{
			case Execute:											0;
			
			case PreIncrement:								1;
			case PostIncrement:								1;
			case PreDecrement:								1;
			case PostDecrement:								1;
			
			case Negative:										2;
			case BoolNot:											2;
			
			case MathDivide:									3;
			case MathModulo:									3;
			case MathMultiply:								3;
			
			case MathAdd:											4;
			case MathSubtract:								4;
			
			case BitShiftRight:								5;
			case BitShiftLeft:								5;
			case BitShiftRightStrict:					5;
			
			case BoolGreater:									6;
			case BoolGreaterEquals:						6;
			case BoolLess:										6;
			case BoolLessEquals:							6;
			
			case BoolEquals:									7;
			case BoolNotEquals:								7;
			
			case BitAnd:											8;
			case BitXOr:											9;
			case BitOr:												10;
			case BoolAnd:											11;
			case BoolOr:											12;
			
			case Assign:											13;
			case MathAddAssign:								13;
			case MathSubtractAssign:					13;
			case MathDivideAssign:						13;
			case MathModuloAssign:						13;
			case MathMultiplyAssign:					13;
			case BitAndAssign:								13;
			case BitOrAssign:									13;
			case BitXOrAssign:								13;
			case BitShiftRightAssign:					13;
			case BitShiftLeftAssign:					13;
			case BitShiftRightStrictAssign:		13;
			default:													13;
		}
	}
}

private enum ScriptToken
{
	Block(contents:Array<ScriptToken>);
	
	VarDec(name:String);
	
	Conditional(blockA:ScriptToken, blockB:ScriptToken);
	Loop(block:ScriptToken, test:ScriptToken);
	ForEachIn(block:ScriptToken, name:String, container:ScriptToken);
	ForRangeOf(block:ScriptToken, name:String, low:ScriptToken, high:ScriptToken);
	Switch(cases:Array<ScriptCase>, defaultCase:ScriptToken);
	
	Execute;
	Trace(block:ScriptToken);
	Throw(block:ScriptToken);
	Catch(block:ScriptToken);
	
	Resolve(path:String);
	Literal(val:Dynamic);
	
	Assign;
	
	Negative;
	MathAdd;
	MathSubtract;
	MathDivide;
	MathModulo;
	MathMultiply;
	BitAnd;
	BitOr;
	BitXOr;
	BitShiftRight;
	BitShiftLeft;
	BitShiftRightStrict;
	BoolNot;
	BoolEquals;
	BoolNotEquals;
	BoolGreater;
	BoolGreaterEquals;
	BoolLess;
	BoolLessEquals;
	BoolAnd;
	BoolOr;
	
	MathAddAssign;
	MathSubtractAssign;
	MathDivideAssign;
	MathModuloAssign;
	MathMultiplyAssign;
	BitAndAssign;
	BitOrAssign;
	BitXOrAssign;
	BitShiftRightAssign;
	BitShiftLeftAssign;
	BitShiftRightStrictAssign;
	
	PreIncrement;
	PostIncrement;
	PreDecrement;
	PostDecrement;
	
	Create(name:String);
	Delete(name:String);
	
	Pop;
}

private typedef ScriptCase =
{
	var values:Array<Dynamic>;
	var block:ScriptToken;
}

private enum ScriptComm
{
	Continue;
	Break;
	Return;
	Exception(info:Dynamic);
}

private typedef ScriptResolve = 
{
	var source:Map<String, Dynamic>;
	var key:String;
}