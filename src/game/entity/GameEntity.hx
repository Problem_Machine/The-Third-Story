package game.entity;
import com.problemmachine.tools.xml.XMLTools;
import flash.errors.Error;
import flash.geom.Point;
import game.animation.Animation;
import game.entity.event.GameEntityEvent;
import flash.events.EventDispatcher;
import flash.geom.Rectangle;
import game.entity.scripting.EntityScriptParser;
import haxe.xml.Fast;

class GameEntity
{
	static public inline var GLOBAL_ACCESS_CHARACTER:String = "^";
	
	static private inline var _NAME:String = "name";
	static private inline var _X:String = "x";
	static private inline var _Y:String = "y";
	static private inline var _WIDTH:String = "width";
	static private inline var _HEIGHT:String = "height";
	static private inline var _RADIUS:String = "radius";
	static private inline var _CIRCULAR_COLLISION:String = "circularCollision";
	static private inline var _SOLID:String = "solid";
	static private inline var _WEIGHT:String = "weight";
	static private inline var _UPDATE_RATE:String = "updateRate";
	static private inline var _ELAPSED_TIME:String = "elapsedTime";
	static private inline var _INIT:String = "init";
	static private inline var _INTERACT:String = "interact";
	static private inline var _UPDATE:String = "update";
	static private inline var _DIE:String = "die";
	
	static private var sAssetDirectory:String = "";
	
	static private var sAllGameEntities:Map<String, GameEntity> = new Map<String, GameEntity>();
	static private var sGlobalVariables:Map<String, String> = new Map<String, String>();
	
	static private var sDispatcher:EventDispatcher = new EventDispatcher();
	
	private var mLocalVariables:Map<String, String>;
	private var mUpdateCounter:Float;
	private var mCollisions:Array<GameEntity>;
	public var activeAnimation(default, null):Animation;
	public var name(get, set):String;
	
	public var x(get, set):Float;
	public var y(get, set):Float;
	public var width(get, set):Float;
	public var height(get, set):Float;
	public var radius(get, set):Float;
	public var circularCollision(get, set):Bool;
	
	public var solid(get, set):Bool;
	public var weight(get, set):Float;
	
	public var updateRate(get, set):Float;
	
	public var initScript(get, set):String;
	public var interactScript(get, set):String;
	public var updateScript(get, set):String;
	public var dieScript(get, set):String;
	
	public function new(?name:String, x:Float = 10, y:Float = 10, updateRate:Float = 0.1, solid:Bool = false) 
	{
		mLocalVariables = new Map<String, String>();
		if (name == null)
			name = "entity";
		this.name = name;
		
		this.x = x;
		this.y = y;
		this.solid = solid;
		this.weight = 1;
		this.circularCollision = false;
		this.width = this.height = 1;
		this.radius = 0.5;
		mCollisions = [];
		sGlobalVariables.set("TESTVAL1", "30");
		sGlobalVariables.set("TESTVAL2", "40");
		sGlobalVariables.set("TESTVAL3", "40");
		
		this.initScript = "";
		this.updateScript = "";
		this.interactScript = "";
		this.dieScript = "";
		this.updateRate = updateRate;
		mUpdateCounter = 0;
	}
	
	private inline function get_name():String
	{	return mLocalVariables.get(_NAME);																				}
	private function set_name(val:String):String
	{
		if (name == null || val.toLowerCase() != name.toLowerCase())
		{
			var s:String = getNextName(val);
			sAllGameEntities.set(s.toLowerCase(), this);
			sAllGameEntities.remove(s.toLowerCase());
			mLocalVariables.set(_NAME, s);
		}
		return val;
	}
	
	private inline function get_x():Float
	{	return Std.parseFloat(mLocalVariables.get(_X));														}
	private inline function set_x(val:Float):Float
	{	mLocalVariables.set(_X, Std.string(val));	return val;											}
	private inline function get_y():Float
	{	return Std.parseFloat(mLocalVariables.get(_Y));														}
	private inline function set_y(val:Float):Float
	{	mLocalVariables.set(_Y, Std.string(val));	return val;											}
	private inline function get_width():Float
	{	return Std.parseFloat(mLocalVariables.get(_WIDTH));												}
	private inline function set_width(val:Float):Float
	{	mLocalVariables.set(_WIDTH, Std.string(val));	return val;									}
	private inline function get_height():Float
	{	return Std.parseFloat(mLocalVariables.get(_HEIGHT));											}
	private inline function set_height(val:Float):Float
	{	mLocalVariables.set(_HEIGHT, Std.string(val));	return val;								}
	private inline function get_radius():Float
	{	return Std.parseFloat(mLocalVariables.get(_RADIUS));											}
	private inline function set_radius(val:Float):Float
	{	mLocalVariables.set(_RADIUS, Std.string(val));	return val;								}
	private inline function get_circularCollision():Bool
	{	return mLocalVariables.get(_CIRCULAR_COLLISION) == Std.string(true);			}
	private inline function set_circularCollision(val:Bool):Bool
	{	mLocalVariables.set(_CIRCULAR_COLLISION, Std.string(val));	return val;		}
	private inline function get_solid():Bool
	{	return mLocalVariables.get(_SOLID) == Std.string(true);										}
	private inline function set_solid(val:Bool):Bool
	{	mLocalVariables.set(_SOLID, Std.string(val));	return val;									}
	private inline function get_weight():Float
	{	return Std.parseFloat(mLocalVariables.get(_WEIGHT));											}
	private inline function set_weight(val:Float):Float
	{	mLocalVariables.set(_WEIGHT, Std.string(val));	return val;								}
	private inline function get_updateRate():Float
	{	return Std.parseFloat(mLocalVariables.get(_UPDATE_RATE));									}
	private inline function set_updateRate(val:Float):Float
	{
		mLocalVariables.set(_UPDATE_RATE, Std.string(val));	
		if (val > 0)			mLocalVariables.set(_ELAPSED_TIME, Std.string(val));
		return val;						
	}
	private inline function get_initScript():String
	{	return mLocalVariables.get(_INIT);																			}
	private inline function set_initScript(val:String):String
	{	mLocalVariables.set(_INIT, val);	return val;														}
	private inline function get_interactScript():String
	{	return mLocalVariables.get(_INTERACT);																			}
	private inline function set_interactScript(val:String):String
	{	mLocalVariables.set(_INTERACT, val);	return val;														}
	private inline function get_updateScript():String
	{	return mLocalVariables.get(_UPDATE);																			}
	private inline function set_updateScript(val:String):String
	{	mLocalVariables.set(_UPDATE, val);	return val;														}
	private inline function get_dieScript():String
	{	return mLocalVariables.get(_DIE);																			}
	private inline function set_dieScript(val:String):String
	{	mLocalVariables.set(_DIE, val);	return val;														}
	
	public inline function setVar(varName:String, val:String):Void
	{
		if (varName == _NAME)
			name = val;
		else if (varName == _UPDATE_RATE)
			updateRate = Std.parseFloat(val);
		else
			mLocalVariables.set(varName, val);
	}
	public inline function getVar(varName:String):String
	{
		return mLocalVariables.get(varName);
	}
	
	private static function getNextName(name:String):String
	{
		var allowedChars:String = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz#&() -";
		for (i in 0...name.length)
			if (allowedChars.indexOf(name.charAt(name.length - 1 - i)) < 0)
				name = name.substring(0, name.length - 1 - i) + name.substring(name.length - 1 - i + 1);
		while (sAllGameEntities.exists(name))
		{
			switch(name.charAt(name.length - 1))
			{
				case "0":
					name = name.substr(0, name.length - 1) + "1";
				case "1":
					name = name.substr(0, name.length - 1) + "2";
				case "2":
					name = name.substr(0, name.length - 1) + "3";
				case "3":
					name = name.substr(0, name.length - 1) + "4";
				case "4":
					name = name.substr(0, name.length - 1) + "5";
				case "5":
					name = name.substr(0, name.length - 1) + "6";
				case "6":
					name = name.substr(0, name.length - 1) + "7";
				case "7":
					name = name.substr(0, name.length - 1) + "8";
				case "8":
					name = name.substr(0, name.length - 1) + "9";
				case "9":
					name = name.substr(0, name.length) + "0";
				default:
					name = name + " 0";
			}
		}
		return name;
	}
	
	public static inline function getEntity(name:String):GameEntity
	{
		return sAllGameEntities.get(name);
	}
	
	public function clone():GameEntity
	{
		var e:GameEntity = new GameEntity();
		for (k in mLocalVariables.keys())
			e.mLocalVariables.set(k, mLocalVariables.get(k));
		if (activeAnimation != null)
		{
			e.activeAnimation = activeAnimation;
			sDispatcher.dispatchEvent(new GameEntityEvent(e, GameEntityEvent.ADD_ANIMATION, e.activeAnimation));
		}
		return e;
	}
	
	static function collisionTest(a:GameEntity, b:GameEntity):Void
	{
		var overlapX:Float = 0;
		var overlapY:Float = 0;
		if (a.circularCollision && b.circularCollision)
		{
			var angle:Point = new Point(a.x - b.x, a.y - b.y);
			var dist:Float = angle.length;
			if (angle.length < a.radius + b.radius)
			{
				angle.normalize(a.radius + b.radius - dist);
				overlapX = Math.abs(angle.x);
				overlapY = Math.abs(angle.y);
			}
		}
		else if (a.circularCollision || b.circularCollision)
		{
			if (b.circularCollision)
			{
				var temp:GameEntity = a;
				a = b;
				b = temp;
			}
			// test to see if the closest point to the entity is closer than the entity's radius
			var testX:Float = Math.min(Math.max(b.x - b.width * 0.5, a.x), b.y - b.height * 0.5);
			var testY:Float = Math.min(Math.max(b.y - b.height, a.y), b.y + b.height * 0.5);
				
			if ((testX - a.x) * (testX - a.x) + (testY - a.y) * (testY - a.y) < a.radius * a.radius)
			{
				var angle:Point = new Point(a.x - testX, a.y - testY);
				angle.normalize(angle.length - a.radius);
				
				overlapX += angle.x;
				overlapY += angle.y;
			}
		}
		else
		{
			var intersection:Rectangle = new Rectangle(a.x - a.width * 0.5, a.y - a.height * 0.5, a.width, a.height).intersection(
					new Rectangle(b.x - b.width * 0.5, b.y - b.height * 0.5, b.width, b.height));
			if (intersection.width != 0 && intersection.height != 0)
			{
				if (intersection.width < intersection.height)
					overlapX = intersection.width;
				else
					overlapY = intersection.height;
			}
		}
		
		if (overlapX != 0 && overlapX != 0)
		{
			a.mCollisions.push(b);
			b.mCollisions.push(a);
			if (a.solid && b.solid)
			{
				var ratio:Float = a.weight;
				if (a.weight < 0)
					ratio = 1;
				else if (b.weight < 0)
					ratio = 0;
				else
					a.weight / b.weight;
					
				if (a.weight > 0 && b.weight > 0)
				{
					a.x += (overlapX * ratio) * (a.x < b.x ? -1 : 1);
					a.y += (overlapY * ratio) * (a.y < b.y ? -1 : 1);
					b.x += (overlapX * 1 / ratio) * (a.x < b.x ? 1 : -1);
					b.y += (overlapY * 1 / ratio) * (a.y < b.y ? 1 : -1);
				}
			}
		}
	}
	
	public function dispose():Void
	{
		if (activeAnimation != null)
			activeAnimation.dispose();
		activeAnimation = null;
	}
	
	public function init():Void
	{
		var xml:Xml = null;
		try
		{
			xml = Xml.parse(initScript);
		}
		catch (d:Dynamic){trace(name + " - INIT: " + d); }
		parseScript(xml);
	}
	public function interact():Void
	{
		var xml:Xml = null;
		try
		{
			xml = Xml.parse(interactScript);
		}
		catch (d:Dynamic){trace(name + " - INTERACT: " + d); }
		parseScript(xml);
	}
	public function update(time:Float):Void
	{
		var xml:Xml = null;
		try
		{
			xml = Xml.parse(updateScript);
		}
		catch (d:Dynamic)		{	trace(name + " - UPDATE: " + d); }
		
		mUpdateCounter += time;
		if (updateRate < 0)
		{
			mLocalVariables.set(_ELAPSED_TIME, Std.string(time));
			if (xml != null)
				parseScript(xml);
			while (mCollisions.length > 0)
				mCollisions.pop();
		}
		else
		{
			while (mUpdateCounter > updateRate)
			{
				if (xml != null)
					parseScript(xml);
				mUpdateCounter -= updateRate;
				while (mCollisions.length > 0)
					mCollisions.pop();
			}
		}
	}
	public function die():Void
	{
		var xml:Xml = null;
		try
		{
			xml = Xml.parse(dieScript);
		}
		catch (d:Dynamic){ trace(name + " - DIE: " + d); }
		parseScript(xml);
	}
	
	public function getRect():Rectangle
	{
		return new Rectangle(x - width * 0.5, y - height * 0.5, width, height);
	}
	
	public function toXML():Xml
	{
		var xml:Xml = Xml.parse("<entity/>").firstElement();
		for (k in mLocalVariables.keys())
			xml.addChild(Xml.parse("<" + k + ">" + mLocalVariables.get(k) + "</" + k + ">").firstElement());
		return xml;
	}
	
	static public function fromXML(xml:Xml):GameEntity
	{
		var e:GameEntity = new GameEntity(XMLTools.getVal(xml, "name"), 0, 0, Std.parseFloat(XMLTools.getVal(xml, "updateRate")));
		for (x in xml.elements())
			e.mLocalVariables.set(x.nodeName, x.firstChild().nodeValue);
		return e;
	}
	
	private function parseScript(xml:Xml):Void
	{
		var fx:Fast = new Fast(xml);
		for (action in fx.nodes.action)
		{
			var varNames:Map<String, String> = new Map<String, String>();
			for (condVal in action.nodes.condVal)
				varNames.set(condVal.att.key, condVal.innerData);
				
			var dialog:String = "";
			var answers:Array<String> = [];
			
			for (el in action.elements)
			{
				trace (name + " --- " + el.name);
				switch (el.name.toLowerCase())
				{
					case "cond":
						var tokens:Array<String> = [];
						var statement:String = el.innerData;
						while (statement.length > 0)
						{
							var cutTo:Int = -1;
							switch(statement.charAt(0))
							{
								case " ", "\n":
									cutTo = 1;
								case "\"":
									cutTo = statement.indexOf("\"", 1) + 1;
									tokens.push(statement.substring(1, cutTo - 1));
								case ">", "<", "(", ")":
									cutTo = statement.charAt(1) == "=" ? 2 : 1;
									tokens.push(statement.substring(0, cutTo));	
								case "!", "=":
									if (statement.charAt(1) != "=")
										throw new Error("Invalid (Incomplete) Comparison: " + statement.charAt(0));
									cutTo = 2;
									tokens.push(statement.substring(0, cutTo));	
								case "&":
									if (statement.charAt(1) != "&")
										throw new Error("Invalid (Incomplete) Comparison: " + statement.charAt(0));
									cutTo = 2;
									tokens.push(statement.substring(0, cutTo));	
								case "|":
									if (statement.charAt(1) != "|")
										throw new Error("Invalid (Incomplete) Comparison: " + statement.charAt(0));
									cutTo = 2;
									tokens.push(statement.substring(0, cutTo));	
									
								default:
									cutTo = 1;
									while (" \n><()!=&|".indexOf(statement.charAt(cutTo)) < 0 && cutTo < statement.length)
										++cutTo;
									var name:String = statement.substring(0, cutTo);
									if (!varNames.exists(name))
										throw new Error ("Unrecognized Variable name " + name);
									tokens.push(varNames.get(name));	
							}
							
							statement = statement.substring(cutTo, statement.length);
						}
						var parentheses:Array<Int> = [];
						var i:Int = 0;
						if (tokens.length == 1)
							tokens[0] = sGlobalVariables.exists(tokens[0]) ? sGlobalVariables.get(tokens[0]) : "false";
						while (i < tokens.length)
						{
							if (tokens[i] == "(")
								parentheses.push(i);
							else if (tokens[i] == ")")
							{
								var open:Int = parentheses.pop();
								tokens.splice(open, 1);
								evalTokens(tokens, open, i - 2);
								tokens.splice(open + 1, 1);
								i = open + 1;
							}
							++i;
						}
						if (tokens.length > 1)
							evalTokens(tokens, 0, tokens.length - 1);
						if (tokens[0] == "false")
							break;
						
					case "loadanimation":
						if (activeAnimation != null)
						{
							sDispatcher.dispatchEvent(new GameEntityEvent(this, GameEntityEvent.REMOVE_ANIMATION, activeAnimation));
							activeAnimation.dispose();
							activeAnimation = null;
						}
						if (el.hasNode.animation)
						{
							activeAnimation = Animation.createFromXML(el.node.animation.x, false);
							sDispatcher.dispatchEvent(new GameEntityEvent(this, GameEntityEvent.ADD_ANIMATION, activeAnimation));
						}
						else
						{
							var s:String = StringTools.trim(el.innerData);
							while (s != null && !StringTools.startsWith(s, '\"') && !StringTools.startsWith(s, '\'') && !StringTools.startsWith(s, "<"))
								s = StringTools.trim(resolveVariable(s));
							
							if (s != null)
							{
								if (StringTools.startsWith(s, "<animation>"))
									activeAnimation = Animation.createFromXML(Xml.parse(s).firstElement(), false);
								else
								{
									s = StringTools.replace(s, "\"", "");
									s = StringTools.replace(s, "\'", "");
									activeAnimation = Animation.createFromPath(sAssetDirectory + s, false);
								}
								sDispatcher.dispatchEvent(new GameEntityEvent(this, GameEntityEvent.ADD_ANIMATION, activeAnimation));
							}
						}
						
					case "set":
						var data:Array<String> = [el.att.flag, el.innerData];
						sDispatcher.dispatchEvent(new GameEntityEvent(this, GameEntityEvent.SET_VALUE, data));
						if (el.att.flag.charAt(0) != GLOBAL_ACCESS_CHARACTER && mLocalVariables.exists(el.att.flag))
							mLocalVariables.set(el.att.flag, el.innerData);
						else if (el.att.flag.charAt(0) == GLOBAL_ACCESS_CHARACTER)
							sGlobalVariables.set(el.att.flag, el.innerData.substr(1));
						else
							sGlobalVariables.set(el.att.flag, el.innerData);
							
					case "setlocal":
						var data:Array<String> = [el.att.flag, el.innerData];
						sDispatcher.dispatchEvent(new GameEntityEvent(this, GameEntityEvent.SET_LOCAL_VALUE, data));
						mLocalVariables.set(el.att.flag, el.innerData);
					case "setglobal":
						if (el.att.flag.charAt(0) == GLOBAL_ACCESS_CHARACTER)
							el.att.flag = el.att.flag.substr(1);
						var data:Array<String> = [el.att.flag, el.innerData];
						sDispatcher.dispatchEvent(new GameEntityEvent(this, GameEntityEvent.SET_GLOBAL_VALUE, data));
						sGlobalVariables.set(el.att.flag, el.innerData);
					case "dialog":
						dialog += el.innerData;
					case "answer":
						answers.push(el.innerData);
						
					case "condval":
						// nothing
					default:
						trace ("Unknown command: " + el.name);
				}
			}
				
			if (dialog.length > 0)
			{
				var data:Array<String> = [dialog];
				for (s in answers)
					data.push(s);
				sDispatcher.dispatchEvent(new GameEntityEvent(this, GameEntityEvent.DIALOG, data));
			}
		}
	}
	
	private function evalTokens(tokens:Array<String>, start:Int, end:Int):Void
	{
		var i:Int = start;
		while (i < end)
		{
			var eval:Null<Bool> = null;
			switch (tokens[i])
			{
				case ">":
					eval = Std.parseFloat(resolveVariable(tokens[i - 1])) > Std.parseFloat(resolveVariable(tokens[i + 1]));
				case ">=":
					eval = Std.parseFloat(resolveVariable(tokens[i - 1])) >= Std.parseFloat(resolveVariable(tokens[i + 1]));
				case "<":
					eval = Std.parseFloat(resolveVariable(tokens[i - 1])) < Std.parseFloat(resolveVariable(tokens[i + 1]));
				case "<=":
					eval = Std.parseFloat(resolveVariable(tokens[i - 1])) <= Std.parseFloat(resolveVariable(tokens[i + 1]));
			}
			if (eval != null)
			{
				tokens.splice(i - 1, 3);
				tokens.insert(i - 1, Std.string(eval));
				end -= 2;
			}
			++i;
		}
		
		i = start;
		while (i < end)
		{
			var eval:Null<Bool> = null;
			if (tokens[i] == "==")
				eval = (tokens[i - 1] == "true" || (resolveVariable(tokens[i - 1]) != null && resolveVariable(tokens[i - 1]) != "false")) 
					== (tokens[i + 1] == "true" || (resolveVariable(tokens[i + 1]) != null && resolveVariable(tokens[i + 1]) != "false"));
			else if (tokens[i] == "!=")
				eval = (tokens[i - 1] == "true" || (resolveVariable(tokens[i - 1]) != null && resolveVariable(tokens[i - 1]) != "false")) 
					!= (tokens[i + 1] == "true" || (resolveVariable(tokens[i + 1]) != null && resolveVariable(tokens[i + 1]) != "false"));
			
			if (eval != null)
			{
				tokens.splice(i - 1, 3);
				tokens.insert(i - 1, Std.string(eval));
				end -= 2;
			}
			++i;
		}
		
		i = start;
		while (i < end)
		{
			var eval:Null<Bool> = null;
			if (tokens[i] == "&&")
				eval = (tokens[i - 1] == "true" || (resolveVariable(tokens[i - 1]) != null && resolveVariable(tokens[i - 1]) != "false")) 
					&& (tokens[i + 1] == "true" || (resolveVariable(tokens[i + 1]) != null && resolveVariable(tokens[i + 1]) != "false"));
			
			if (eval != null)
			{
				tokens.splice(i - 1, 3);
				tokens.insert(i - 1, Std.string(eval));
				end -= 2;
			}
			++i;
		}
		
		i = start;
		while (i < end)
		{
			var eval:Null<Bool> = null;
			if (tokens[i] == "||")
				eval = (tokens[i - 1] == "true" || (resolveVariable(tokens[i - 1]) != null && resolveVariable(tokens[i - 1]) != "false")) 
					|| (tokens[i + 1] == "true" || (resolveVariable(tokens[i + 1]) != null && resolveVariable(tokens[i + 1]) != "false"));
			
			if (eval != null)
			{
				tokens.splice(i - 1, 3);
				tokens.insert(i - 1, Std.string(eval));
				end -= 2;
			}
			++i;
		}
		
		if (end != start)
			throw new Error("Well this doesn't add up");
	}
	private inline function resolveVariable(name:String):String
	{
		if (name.charAt(0) == GLOBAL_ACCESS_CHARACTER)
			return sGlobalVariables.get(name.substr(1));
		else
			return mLocalVariables.exists(name) ? mLocalVariables.get(name) : sGlobalVariables.get(name);
	}
	
	static public function addEventListener(type:String, listener:Dynamic->Void, useCapture:Bool = false, priority:Int = 0, useWeakReference:Bool = false)
	{
		sDispatcher.addEventListener(type, listener, useCapture, priority, useWeakReference);
	}
	static public function removeEventListener(type:String, listener:Dynamic->Void, useCapture:Bool = false)
	{
		sDispatcher.removeEventListener(type, listener, useCapture);
	}
}