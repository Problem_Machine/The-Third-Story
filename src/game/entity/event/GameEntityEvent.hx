package game.entity.event;

import flash.events.Event;
import game.entity.GameEntity;

class GameEntityEvent extends Event
{
	static public inline var REMOVE_ANIMATION:String = "GameEntityEvent.RemoveAnimation";
	static public inline var ADD_ANIMATION:String = "GameEntityEvent.AddAnimation";
	static public inline var DIALOG:String = "GameEntityEvent.Dialog";
	static public inline var SET_VALUE:String = "GameEntityEvent.SetValue";
	static public inline var SET_LOCAL_VALUE:String = "GameEntityEvent.SetLocalValue";
	static public inline var SET_GLOBAL_VALUE:String = "GameEntityEvent.SetGlobalValue";
	static public inline var NEW_ENTITY:String = "GameEntityEvent.NewEntity";
	static public inline var DELETE_ENTITY:String = "GameEntityEvent.DeleteEntity";
	
	public var data(default, null):Dynamic;
	public var entity(default, null):GameEntity;

	public function new(entity:GameEntity, type:String, data:Dynamic) 
	{
		super(type);
		this.entity = entity;
		this.data = data;
	}
}