package game;

import com.problemmachine.bitmap.bitmapmanager.BitmapManager;
import com.problemmachine.tools.file.FileTools;
import com.problemmachine.tools.keyboard.KeyboardTools;
import com.problemmachine.tools.math.IntMath;
import com.problemmachine.tools.xml.XMLTools;
import flash.Lib;
import flash.desktop.Clipboard;
import flash.desktop.ClipboardFormats;
import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.Graphics;
import flash.display.Sprite;
import flash.errors.Error;
import flash.events.Event;
import flash.events.KeyboardEvent;
import flash.events.MouseEvent;
import flash.events.TimerEvent;
import flash.geom.Matrix;
import flash.geom.Point;
import flash.geom.Rectangle;
import flash.text.TextField;
import flash.text.TextFormat;
import flash.ui.Keyboard;
import flash.utils.Timer;
import game.animation.Animation;
import game.animation.AnimationContainer;
import game.dialog.DialogBox;
import game.editor.EntityEditor;
import game.editor.EntityToolbar;
import game.editor.LevelSelector;
import game.editor.TileSelector;
import game.entity.GameEntity;
import game.entity.event.GameEntityEvent;
import game.entity.scripting.EntityScriptParser;
import game.gametile.GameTile;
import game.gametile.data.CollisionType;
import game.gametile.data.TilesetData;
import game.gametile.manager.TileManager;
import game.gametile.manager.TileReference;
import game.room.GameRoom;
#if air
import flash.filesystem.File;
#elseif cpp
import sys.FileSystem;
#end

class Game extends Sprite
{
	static private inline var FPS_POLL_RATE:Float = 0.5;
	static public inline var TILE_SIZE:Int = 60;
	static public inline var RESOLUTION_WIDTH:Int = 1920;
	static public inline var RESOLUTION_HEIGHT:Int = 1080;
	static public inline var MAX_BRUSH_SIZE:Int = 10;
	static public inline var ENTITY_SELECT_RADIUS:Int = 30;
	static public inline var MAX_TILE_UNDO_STATES:Int = 100;
	static public inline var MAX_ENTITY_UNDO_STATES:Int = 100;

	static public inline var DEBUG_OPEN:Int = 0;
	static public inline var DEBUG_CLOSED:Int = 1;
	static public inline var DEBUG_NW:Int = 2;
	static public inline var DEBUG_NE:Int = 3;
	static public inline var DEBUG_SW:Int = 4;
	static public inline var DEBUG_SE:Int = 5;

	private var mAssetDirectory:String;
	private var mCamera:Point;
	private var mRooms:Array<GameRoom>;
	private var mCurrentRoom:GameRoom;
	private var mEntities:Array<GameEntity>;
	private var mAnimationContainer:AnimationContainer;
	private var mPlayer:GameEntity;
	private var mTimer:Timer;
	private var mLastUpdateTime:Int;
	private var mDialogBox:DialogBox;
	private var mTileReferences:Array<TileReference>;
	private var mTilesetData:TilesetData;

	private var mGameMode:GameMode;
	private var mOverlayLayer:Sprite;
	private var mLevelSelector:LevelSelector;
	private var mTileSelector:TileSelector;
	private var mMouseDown:Bool;
	private var mBrushSize:Int;
	private var mDragOrigin:Point;
	private var mSelection:Rectangle;
	private var mStampBrush:Bitmap;
	private var mTileReadout:TextField;
	private var mSelectedEntities:Array<GameEntity>;
	private var mDraggingEntities:Bool;
	private var mEntityEditor:EntityEditor;
	private var mEntityToolbar:EntityToolbar;
	
	private var mFPSReadout:TextField;
	private var mFrameTimes:Array<Float>;
	private var mFPSPollCounter:Float;
	
	private var mTilesUndoStack:Array<Array<Array<GameTile>>>;
	private var mTilesRedoStack:Array<Array<Array<GameTile>>>;
	private var mEntitiesUndoStack:Array<Array<GameEntity>>;
	private var mEntitiesRedoStack:Array<Array<GameEntity>>;

	@:access(game.gametile.manager.TileManager)
	@:access(game.entity.GameEntity.sAssetDirectory)
	public function new()
	{
		super();
		mGameMode = Play;
		mLastUpdateTime = Lib.getTimer();
		mTimer = new Timer(10);
		mTimer.addEventListener(TimerEvent.TIMER, update);
		mCamera = new Point();
		mAssetDirectory = crawlDirTreeFor("assets");
		BitmapManager.rootDirectory = mAssetDirectory;
		GameEntity.sAssetDirectory = mAssetDirectory;
		mStampBrush = new Bitmap(new BitmapData(RESOLUTION_WIDTH, RESOLUTION_HEIGHT, true, 0x00000000));
		mStampBrush.alpha = 0.6;

		KeyboardTools.startListeningForAnyKeyDown(keyboardListener);
		mMouseDown = false;
		mBrushSize = 1;
		addEventListener(MouseEvent.MOUSE_MOVE, mouseListener);
		addEventListener(MouseEvent.MOUSE_DOWN, mouseListener);
		addEventListener(MouseEvent.MOUSE_UP, mouseListener);
		addEventListener(MouseEvent.MOUSE_WHEEL, mouseListener);
		addEventListener(MouseEvent.RIGHT_CLICK, mouseListener);
		addEventListener(MouseEvent.RIGHT_MOUSE_DOWN, mouseListener);
		addEventListener(MouseEvent.RIGHT_MOUSE_UP, mouseListener);
		mDragOrigin = null;
		mSelection = null;

		mTilesetData = new TilesetData();
		mTileReferences = [];
		
		generateDebugTileset();
		
		var rects:Array<Rectangle> = [];
		for (y in 0...10)
			for (x in 0...10)
				rects.push(new Rectangle(x * TILE_SIZE, y * TILE_SIZE, TILE_SIZE, TILE_SIZE));
		var ref:TileReference = TileManager.addTilesByPath("TestTileset0.png", rects);
		mTileReferences.push(ref);
		for (i in 0...ref.tileIDs.length)
		{
			var col:CollisionType = switch (i)
			{
				case 23, 32, 33, 34, 43, 57, 66, 67, 68, 77:
					CollisionType.Open;
				case 22, 56:
					CollisionType.DiagNW;
				case 24, 58:
					CollisionType.DiagNE;
				case 42, 76:
					CollisionType.DiagSW;
				case 44, 78:
					CollisionType.DiagSE;
				default:
					CollisionType.Block;
			}
			mTilesetData.collision.set(ref.tileIDs[i], col);
		}
		
		mRooms = [mCurrentRoom = new GameRoom(GameRoom.getUnusedID(), null, RESOLUTION_WIDTH, RESOLUTION_HEIGHT, RESOLUTION_WIDTH, RESOLUTION_HEIGHT)];
		mCurrentRoom.setTile(63, 11, new GameTile(DEBUG_CLOSED));
		for (x in 0...64)
			for (y in 0...12)
			{
				if (y < 1)
					mCurrentRoom.setTile(x, y, new GameTile(ref.tileIDs[x > 0 && x < 63 ? 3 : 0]));
				else if (x < 2 || x >= 62 || y < 2 || y >= 10)
					mCurrentRoom.setTile(x, y, new GameTile(ref.tileIDs[13]));
				else
					mCurrentRoom.setTile(x, y, new GameTile(ref.tileIDs[33]));
			}
		
		addChild(mCurrentRoom);
		mCurrentRoom.setTile(2, 2, new GameTile(ref.tileIDs[22]));
		mCurrentRoom.setTile(2, 9, new GameTile(ref.tileIDs[42]));
		mCurrentRoom.setTile(61, 2, new GameTile(ref.tileIDs[24]));
		mCurrentRoom.setTile(61, 9, new GameTile(ref.tileIDs[44]));
		
		rects = [];
		for (y in 0...6)
			for (x in 0...9)
				rects.push(new Rectangle(x * TILE_SIZE, y * TILE_SIZE, TILE_SIZE, TILE_SIZE));
		var ref = TileManager.addTilesByPath("TileBrainstorm4.png", rects);
		mTileReferences.push(ref);
		for (i in 0...ref.tileIDs.length)
		{
			var col:CollisionType = switch (i)
			{
				case 0, 1, 2, 3, 4, 5, 6, 8, 9, 10, 11, 12, 13, 16, 17, 18, 19, 20, 24, 25, 26:
					CollisionType.Block;
				case 7, 15:
					CollisionType.DiagNE;
				default:
					CollisionType.Open;
			}
			mTilesetData.collision.set(ref.tileIDs[i], col);
		}
		mTilesetData.loadTileData(mAssetDirectory + "Default Tile Layout.xml", ref.tileIDs[0]);
	
		mPlayer = new GameEntity("Player", mCurrentRoom.tileWidth * 0.5 - 1, mCurrentRoom.tileWidth * 0.5 - 1, -1, true);
		mPlayer.initScript = XMLTools.tidyXML(Xml.parse("<action><loadAnimation>\"CharSpriteTest.png\"</loadAnimation></action>").firstElement());
		mPlayer.x = mCurrentRoom.tileWidth * 0.5 - 1;
		mPlayer.y = mCurrentRoom.tileHeight * 0.5 - 1;
		mEntities = [mPlayer];

		mDialogBox = new DialogBox(new TextFormat(null, 36, 0xFFFFFF, true));
		addChild(mDialogBox);
		GameEntity.addEventListener(GameEntityEvent.DIALOG, dialogListener);
		GameEntity.addEventListener(GameEntityEvent.ADD_ANIMATION, animationListener);
		GameEntity.addEventListener(GameEntityEvent.REMOVE_ANIMATION, animationListener);
		GameEntity.addEventListener(GameEntityEvent.NEW_ENTITY, entityListener);
		GameEntity.addEventListener(GameEntityEvent.DELETE_ENTITY, entityListener);
		var testEntity:GameEntity = new GameEntity("TestEntity");
		testEntity.width = 1;
		testEntity.height = 1;
		testEntity.initScript = XMLTools.tidyXML(Xml.parse("<action><loadAnimation>\"Running.Loop.Right.anx\"</loadAnimation></action>").firstElement());
		testEntity.interactScript = XMLTools.tidyXML(Xml.parse("<action><dialog>Hello World</dialog></action>\n" +
			"<action><cond>(\"TESTVAL2\" > \"TESTVAL1\") && (\"TESTVAL2\" == \"TESTVAL3\")</cond><dialog>SUCCESSFUL TEST</dialog></action>").firstElement());
		testEntity.setVar("testScript", "trace(\"2 + 2 = \" + (2 + 2));");
		trace ("running test script");
		EntityScriptParser.runScript(testEntity, "testScript");
		trace ("test script complete");
		mCurrentRoom.gameEntities.push(testEntity);
		mOverlayLayer = new Sprite();
		addChild(mOverlayLayer);
		addChild(mStampBrush);

		mLevelSelector = new LevelSelector(RESOLUTION_WIDTH * 0.25, RESOLUTION_WIDTH * 0.05, 0, 0xFFFFFF, 0x113311, 0.5);
		addChild(mLevelSelector);
		mTileSelector = new TileSelector(RESOLUTION_WIDTH * 0.2, RESOLUTION_HEIGHT, 0xFFFFFF, 0x113311);
		mTileSelector.x = RESOLUTION_WIDTH * 0.8;
		for (tr in mTileReferences)
			mTileSelector.addTileset(tr);
		mEntityEditor = new EntityEditor(RESOLUTION_WIDTH * 0.2, RESOLUTION_HEIGHT, 0xFFFFFF, 0x111133, 0x8888FF);
		mEntityEditor.x = RESOLUTION_WIDTH * 0.8;
		mEntityToolbar = new EntityToolbar(RESOLUTION_WIDTH * 0.8, RESOLUTION_HEIGHT * 0.1, 0xFFFFFF, 0x111133, 0x8888FF);
		mEntityToolbar.addEventListener(EntityToolbar.DRAG_ENTITY, toolbarListener);
		mSelectedEntities = [];
		mDraggingEntities = false;
		mTilesRedoStack = [];
		mTilesUndoStack = [];
		mEntitiesRedoStack = [];
		mEntitiesUndoStack = [];
		
		mTileReadout = new TextField();
		mTileReadout.mouseEnabled = false;
		mTileReadout.defaultTextFormat = new TextFormat(null, 28, 0xFFFFFF);
		mTileReadout.width = RESOLUTION_WIDTH;
		mTileReadout.height = 30;
		mTileReadout.y = 30;
		
		mAnimationContainer = new AnimationContainer();
		mAnimationContainer.renderBounds = new Rectangle(0, 0, RESOLUTION_WIDTH, RESOLUTION_HEIGHT);
		addChild(mAnimationContainer);
		
		mFPSReadout = new TextField();
		mFPSReadout.mouseEnabled = false;
		mFPSReadout.defaultTextFormat = new TextFormat("Consolas", 20, 0xFF0000);
		mFPSReadout.width = RESOLUTION_WIDTH;
		mFPSReadout.height = 30;
		mFPSPollCounter = 0;
		mFrameTimes = [];
		addChild(mFPSReadout);
		
		mPlayer.init();
		activateRoom(mCurrentRoom);
		mTimer.start();
	}
	
	private function keyboardListener(e:KeyboardEvent):Void
	{
		if (mGameMode == Play)
			switch (e.keyCode)
			{
				case Keyboard.SPACE:
					if (mDialogBox.visible)
					{
						mDialogBox.progress();
					}
				else
				for (ent in mEntities)
					if (ent != mPlayer && ent.getRect().intersects(mPlayer.getRect()))
					{
						ent.interact();
						break;
					}
				default:
			}
		else
		{
			if (e.keyCode == Keyboard.S && e.ctrlKey)
			{
				FileTools.immediateWriteTo(mAssetDirectory + "levels\\" + StringTools.hex(mCurrentRoom.id, 6), mCurrentRoom.toXML(), true);
			}
			else if (e.keyCode == Keyboard.LEFTBRACKET && e.ctrlKey)
			{
				// previous level
				// reset undo/redo stacks (or save them for later?? small objects mostly...)
			}
			else if (e.keyCode == Keyboard.RIGHTBRACKET && e.ctrlKey)
			{
				// next level
				// reset undo/redo stacks (or save them for later?? small objects mostly...)
			}

			if (mGameMode == EditTiles)
			{
				var currentTileX:Int = Math.floor((mouseX + mCamera.x) / TILE_SIZE);
				var currentTileY:Int = Math.floor((mouseY + mCamera.y) / TILE_SIZE);
				mTileSelector.keyboardControl(e);
				switch (e.keyCode)
				{
					case Keyboard.Z:
						if (e.ctrlKey)
							tileUndo();
					case Keyboard.Y:
						if (e.ctrlKey)
							tileUndo(true);
					case Keyboard.H:
						if (mTileSelector.rot90 == 0 || mTileSelector.rot90 == 2)
							mTileSelector.hFlip = !mTileSelector.hFlip;
						else
							mTileSelector.vFlip = !mTileSelector.vFlip;
						if (mSelection != null)
							drawStampBrush();
					case Keyboard.V:
						if (mTileSelector.rot90 == 0 || mTileSelector.rot90 == 2)
							mTileSelector.vFlip = !mTileSelector.vFlip;
						else
							mTileSelector.hFlip = !mTileSelector.hFlip;
						if (mSelection != null)
							drawStampBrush();
					case Keyboard.Q:
							mTileSelector.rot90 -= 1;
						if (mSelection != null)
							drawStampBrush();
					case Keyboard.E:
						mTileSelector.rot90 += 1;
						if (mSelection != null)
							drawStampBrush();
					case Keyboard.F:
						stackTileUndoState();
						floodFill(currentTileX, currentTileY, mTileSelector.selected);
					case Keyboard.DELETE:
						if (mSelection != null)
						{
							stackTileUndoState();
							for (x in Math.floor(mSelection.left)...Math.floor(mSelection.right))
								for (y in Math.floor(mSelection.top)...Math.floor(mSelection.bottom))
									if (x >= 0 && x < mCurrentRoom.tileWidth && y >= 0 && y < mCurrentRoom.tileHeight)
										mCurrentRoom.setTile(x, y, new GameTile(DEBUG_CLOSED));
							// mCurrentRoom.trim();
						}
					case Keyboard.ENTER, Keyboard.NUMPAD_ENTER:
						stackTileUndoState();
						if (mSelection != null)
						{
							if (mSelection.x < 0 || mSelection.y < 0)
							{
								mCurrentRoom.setTile(Math.floor(mSelection.x), Math.floor(mSelection.y), mTileSelector.selected);
								if (mSelection.x < 0)
								{
									mCamera.x -= mSelection.x * TILE_SIZE;
									mSelection.x = 0;
								}
								if (mSelection.y < 0)
								{
									mCamera.y -= mSelection.y * TILE_SIZE;
									mSelection.y = 0;
								}
							}
							for (x in Math.floor(mSelection.left)...Math.floor(mSelection.right))
								for (y in Math.floor(mSelection.top)...Math.floor(mSelection.bottom))
										mCurrentRoom.setTile(x, y, mTileSelector.selected);
						}
					case Keyboard.R:
						stackTileUndoState();
						createRoom(e.ctrlKey);
					//		R will create a room, with the walls properly formed -- CTRL + R will angle the corners
					
					//case Keyboard.I:
					//		I will invert the tiles, closed tiles becoming open and vice versa
					//case Keyboard.SLASH:
					//		/? will randomize to compatible tiles
					default:
				}
			}
			else if (mGameMode == EditEntities)
				switch (e.keyCode)
				{
					case Keyboard.NUMPAD_ADD, Keyboard.EQUAL:
						if (mSelectedEntities.length > 0)
							for (e in mSelectedEntities)
								mEntityToolbar.addEntity(e.clone());
					case Keyboard.NUMPAD_SUBTRACT, Keyboard.MINUS:
						mEntityToolbar.deleteEntity();
					case Keyboard.A:
						if (e.ctrlKey)
							mSelectedEntities = mCurrentRoom.gameEntities.copy();
					case Keyboard.C:
						if (e.ctrlKey && mSelectedEntities.length > 0)
						{
							Clipboard.generalClipboard.clear();
							var s:String = "";
							for (e in mSelectedEntities)
								s += e.toXML().toString();
							Clipboard.generalClipboard.setData(ClipboardFormats.TEXT_FORMAT, s);
						}
					case Keyboard.V:
						if (e.ctrlKey && StringTools.startsWith(Clipboard.generalClipboard.getData(ClipboardFormats.TEXT_FORMAT), "<entity>"))
						{
							var entities:Array<GameEntity> = [];
							var xml:Xml = null;
							try 
							{
								xml = Xml.parse(Clipboard.generalClipboard.getData(ClipboardFormats.TEXT_FORMAT)); 
							} 
							catch(d:Dynamic){}
							if (xml != null)
							{
								for (x in xml.elementsNamed("entity"))
								{
									entities.push(GameEntity.fromXML(x));
								}
								var avg:Point = new Point();								
								for (e in entities)
								{
									avg.x += e.x;
									avg.y += e.y;
									mCurrentRoom.gameEntities.push(e);
								}
								avg.x /= entities.length;
								avg.y /= entities.length;
								for (e in entities)
								{
									e.x = e.x - avg.x + (mouseX + mCamera.x) / TILE_SIZE;
									e.y = e.y - avg.y + (mouseY + mCamera.y) / TILE_SIZE;
								}
							}
						}
					case Keyboard.Z:
						if (e.ctrlKey)
							entityUndo();
					case Keyboard.Y:
						if (e.ctrlKey)
							entityUndo(true);
					default:
				}
		}

		if (e.keyCode == 192 /* ` ~ */)
		{
			if (mGameMode == Play)
				switchMode(EditTiles);
			else if (mGameMode == EditTiles)
				switchMode(EditEntities);
			else
				switchMode(Play);
		}
	}
	private function mouseListener(e:MouseEvent):Void
	{
		if (e.type == MouseEvent.MOUSE_DOWN)
			mMouseDown = true;
		else if (e.type == MouseEvent.MOUSE_UP)
			mMouseDown = false;

		if (mGameMode == Play)
		{
			switch (e.type)
			{
				default:
			}
		}
		else if (mGameMode == EditTiles)
		{
			var currentTileX:Int = Math.floor((mouseX + mCamera.x) / TILE_SIZE);
			var currentTileY:Int = Math.floor((mouseY + mCamera.y) / TILE_SIZE);
			switch (e.type)
			{
				case MouseEvent.MOUSE_DOWN:
					if (mLevelSelector.hitTestPoint(mouseX, mouseY) || mTileSelector.hitTestPoint(mouseX, mouseY))
						mMouseDown = false;
					if (e.altKey)
					{
						mMouseDown = false;
						var t:GameTile = mCurrentRoom.getTile(currentTileX, currentTileY);
						if (t != null)
							mTileSelector.selected = t.clone();
					}
					else if (mSelection != null)
					{
						stackTileUndoState();
						stampSelection(currentTileX, currentTileY, e.ctrlKey);
						mMouseDown = false;
					}
					if (mMouseDown)
						stackTileUndoState();
				case MouseEvent.RIGHT_MOUSE_DOWN:
					mDragOrigin = new Point(mouseX + mCamera.x, mouseY + mCamera.y);
				case MouseEvent.RIGHT_MOUSE_UP:
					mDragOrigin = null;
					if (mSelection.width == 0 || mSelection.height == 0)
						mSelection = null;
				case MouseEvent.MOUSE_WHEEL:
					if (e.delta > 0)
						mBrushSize = IntMath.min(mBrushSize + 1, MAX_BRUSH_SIZE);
					else
						mBrushSize = IntMath.max(mBrushSize - 1, 1);
				default:
			}
		}
		else if (mGameMode == EditEntities)
		{
			var currentMouseX:Float = (mouseX + mCamera.x) / TILE_SIZE;
			var currentMouseY:Float = (mouseY + mCamera.y) / TILE_SIZE;
			switch (e.type)
			{
				case MouseEvent.MOUSE_DOWN:
					if (!mLevelSelector.hitTestPoint(mouseX, mouseY) && !mEntityEditor.hitTestPoint(mouseX, mouseY) &&
						!mEntityToolbar.hitTestPoint(mouseX, mouseY))
					{
						var closest:GameEntity = null;
						var closestDist:Float = Math.POSITIVE_INFINITY;
						for (e in mCurrentRoom.gameEntities)
						{
							var dist:Float = (currentMouseX - e.x) * (currentMouseX - e.x) + (currentMouseY - e.y) * (currentMouseY - e.y);
							if (dist < closestDist)
							{
								closest = e;
								closestDist = dist;
							}
						}
						if (Math.sqrt(closestDist) * TILE_SIZE < ENTITY_SELECT_RADIUS)
						{
							if (mSelectedEntities.indexOf(closest) < 0)
							{
								mSelectedEntities = [closest];
								mEntityToolbar.unSelect();
							}
							mDraggingEntities = true;
							if (e.shiftKey)
								for (i in 0...mSelectedEntities.length)
								{
									mSelectedEntities[i] = mSelectedEntities[i].clone();
									for (e in mSelectedEntities)
										mCurrentRoom.gameEntities.push(e);
								}
						}
						mDragOrigin = new Point(currentMouseX, currentMouseY);
					}
					
				case MouseEvent.MOUSE_UP:
					mDragOrigin = null;
					mDraggingEntities = false;
					mSelection = null;
				
				case MouseEvent.RIGHT_CLICK:
					if (e.ctrlKey && mEntityToolbar.selected != null)
					{
						var e:GameEntity = mEntityToolbar.selected.clone();
						e.x = currentMouseX;
						e.y = currentMouseY;
						mCurrentRoom.gameEntities.push(e);
					}
					else
						mCurrentRoom.gameEntities.push(new GameEntity(null, currentMouseX, currentMouseY));
				default:
			}
		}
	}
	
	private function toolbarListener(e:Event):Void
	{
		if (e.type == EntityToolbar.DRAG_ENTITY)
		{
			var e:GameEntity = mEntityToolbar.getDraggingEntity();
			e.x = (mouseX + mCamera.x) / TILE_SIZE;
			e.y = (mouseY + mCamera.y) / TILE_SIZE;
			mCurrentRoom.gameEntities.push(e);
		}
		else if (e.type == Event.SELECT)
			mSelectedEntities = [mEntityToolbar.selected];
	}
	
	private function activateRoom(newRoom:GameRoom):Void
	{
		for (e in mEntities)
			{
				if (e.activeAnimation != null)
					mAnimationContainer.removeAnimation(e.activeAnimation);
				if (e != mPlayer)
					e.dispose();
			}
		mEntities = [mPlayer];
		if (mPlayer.activeAnimation != null)
			mAnimationContainer.addAnimation(mPlayer.activeAnimation);
		for (e in newRoom.gameEntities)
		{
			var ent = e.clone();
			mEntities.push(ent);
			ent.init();
		}
		if (newRoom != mCurrentRoom)
		{
			addChildAt(newRoom, getChildIndex(mCurrentRoom));
			removeChild(mCurrentRoom);
		}
		mCurrentRoom = newRoom;
	}

	private function dialogListener(e:GameEntityEvent):Void
	{
		mDialogBox.queueDialog(e.data[0]);
		//trace(e.data[0]);
	}
	
	private function animationListener(e:GameEntityEvent):Void
	{
		if (e.type == GameEntityEvent.ADD_ANIMATION)
			mAnimationContainer.addAnimation(cast e.data);
		else
			mAnimationContainer.removeAnimation(cast e.data);
	}
	
	private function entityListener(e:GameEntityEvent):Void
	{
		if (e.type == GameEntityEvent.NEW_ENTITY)
			mEntities.push(e.entity);
		else if (e.type == GameEntityEvent.DELETE_ENTITY)
			mEntities.remove(e.entity);
	}

	private function switchMode(gameMode:GameMode):Void
	{
		mGameMode = gameMode;
		removeChild(mLevelSelector);
		removeChild(mTileReadout);
		removeChild(mTileSelector);
		removeChild(mEntityEditor);
		removeChild(mEntityToolbar);
		switch (gameMode)
		{
			case Play:
				activateRoom(mCurrentRoom);
			case EditTiles:
				addChild(mLevelSelector);
				addChild(mTileReadout);
				addChild(mTileSelector);
				mLevelSelector.x = RESOLUTION_WIDTH * 0.8 - mLevelSelector.width;
				mLevelSelector.y = RESOLUTION_HEIGHT - mLevelSelector.height;
			case EditEntities:
				addChild(mLevelSelector);
				addChild(mEntityEditor);
				addChild(mEntityToolbar);
				mLevelSelector.x = RESOLUTION_WIDTH * 0.8 - mLevelSelector.width;
				mLevelSelector.y = RESOLUTION_HEIGHT - mLevelSelector.height;
		}
		if (gameMode != Play)
			for (e in mEntities)
			{
				if (e.activeAnimation != null)
					mAnimationContainer.removeAnimation(e.activeAnimation);
				if (e != mPlayer)
					e.dispose();
			}
	}

	private function update(e:TimerEvent):Void
	{
		var time:Float = (Lib.getTimer() - mLastUpdateTime) / 1000;
		mLastUpdateTime = Lib.getTimer();

		var g:Graphics = mOverlayLayer.graphics;
		g.clear();
		mStampBrush.visible = false;
		
		mFPSPollCounter += time;
		mFrameTimes.push(time);
		if (mFPSPollCounter >= FPS_POLL_RATE)
		{
			var avg:Float = 0;
			for (f in mFrameTimes)
				avg += f;
			avg /= mFrameTimes.length;
			mFPSReadout.text = "FPS: " + (Math.round(mFrameTimes.length * 10 / FPS_POLL_RATE) / 10) + " --- Average Frame Time: " + avg;
			mFPSPollCounter = 0;
			mFrameTimes = [];
		}
		
		if (mGameMode != Play && !mEntityEditor.currentlyEditing)
		{
			if (KeyboardTools.getKeyState(Keyboard.D) || KeyboardTools.getKeyState(Keyboard.RIGHT))
				mCamera.x += time * 1700;
			if (KeyboardTools.getKeyState(Keyboard.A) || KeyboardTools.getKeyState(Keyboard.LEFT))
				mCamera.x -= time * 1700;
			if (KeyboardTools.getKeyState(Keyboard.S) || KeyboardTools.getKeyState(Keyboard.DOWN))
				mCamera.y += time * 1700;
			if (KeyboardTools.getKeyState(Keyboard.W) || KeyboardTools.getKeyState(Keyboard.UP))
				mCamera.y -= time * 1700;
		}
		
		switch (mGameMode)
		{
			case Play:
				if (KeyboardTools.getKeyState(Keyboard.D) || KeyboardTools.getKeyState(Keyboard.RIGHT))
					mPlayer.x += time * 5;
				if (KeyboardTools.getKeyState(Keyboard.A) || KeyboardTools.getKeyState(Keyboard.LEFT))
					mPlayer.x -= time * 5;
				if (KeyboardTools.getKeyState(Keyboard.S) || KeyboardTools.getKeyState(Keyboard.DOWN))
					mPlayer.y += time * 5;
				if (KeyboardTools.getKeyState(Keyboard.W) || KeyboardTools.getKeyState(Keyboard.UP))
					mPlayer.y -= time * 5;
				
				for (e in mEntities)
					e.update(time);

				if (mCurrentRoom.tileWidth * TILE_SIZE > RESOLUTION_WIDTH)
					mCamera.x = Math.min(mCurrentRoom.tileWidth * TILE_SIZE - RESOLUTION_WIDTH,
					Math.max(0, mPlayer.x * TILE_SIZE - RESOLUTION_WIDTH * 0.5));
				else
					mCamera.x = (mCurrentRoom.tileWidth * TILE_SIZE - RESOLUTION_WIDTH) * 0.5;
				if (mCurrentRoom.tileHeight * TILE_SIZE > RESOLUTION_HEIGHT)
					mCamera.y = Math.min(mCurrentRoom.tileHeight * TILE_SIZE - RESOLUTION_HEIGHT,
					Math.max(0, mPlayer.y * TILE_SIZE - RESOLUTION_HEIGHT * 0.5));
				else
					mCamera.y = (mCurrentRoom.tileHeight * TILE_SIZE - RESOLUTION_HEIGHT) * 0.5;

				// TILES, not PIXELS
				var incursion:Point = null;
				try
				{
					incursion = mCurrentRoom.testCollision(mPlayer.x, mPlayer.y, mPlayer.width, mPlayer.height,
						mPlayer.circularCollision ? mPlayer.radius : null, mTilesetData.collision);
				}
				catch (msg:String)
				{
					mPlayer.x = mCurrentRoom.tileWidth * 0.5;
					mPlayer.y = mCurrentRoom.tileHeight * 0.5;
				}
				if (incursion != null)
				{
					mPlayer.x -= incursion.x;
					mPlayer.y -= incursion.y;
				}
					
				for (e in mEntities)
					if (e.activeAnimation != null)
					{
						e.activeAnimation.x = e.x * TILE_SIZE - mCamera.x;
						e.activeAnimation.y = e.y * TILE_SIZE - mCamera.y;
					}

				g.beginFill(0x0000FFFF, 0.5);
				for (e in mEntities)
				{
					if (e.circularCollision)
						g.drawCircle(e.x * TILE_SIZE - mCamera.x, e.y * TILE_SIZE - mCamera.y, e.radius * TILE_SIZE);
					else
						g.drawRect((e.x - e.width * 0.5) * TILE_SIZE - mCamera.x, (e.y - e.height * 0.5) * TILE_SIZE - mCamera.y, e.width * TILE_SIZE, e.height * TILE_SIZE);
				}

			case EditTiles:
				var currentTileX:Int = Math.floor((mouseX + mCamera.x) / TILE_SIZE);
				var currentTileY:Int = Math.floor((mouseY + mCamera.y) / TILE_SIZE);
				
				mTileReadout.text = "Tile: " + currentTileX + ", " + currentTileY;

				g.lineStyle(3, 0x000000, 0.5);
				for (x in 0...Math.floor(RESOLUTION_WIDTH / TILE_SIZE + 1))
				{
					g.moveTo(x * TILE_SIZE - (mCamera.x % TILE_SIZE), 0);
					g.lineTo(x * TILE_SIZE - (mCamera.x % TILE_SIZE), RESOLUTION_HEIGHT);
				}
				for (y in 0...Math.floor(RESOLUTION_HEIGHT / TILE_SIZE + 1))
				{
					g.moveTo(0, y * TILE_SIZE - (mCamera.y % TILE_SIZE));
					g.lineTo(RESOLUTION_WIDTH, y * TILE_SIZE - (mCamera.y % TILE_SIZE));
				}

				if (mSelection == null)
				{
					g.beginFill(0xFFFFFFFF, 0.5);
					g.drawRect(currentTileX * TILE_SIZE - mCamera.x, currentTileY * TILE_SIZE - mCamera.y, TILE_SIZE * mBrushSize, TILE_SIZE * mBrushSize);
				}
				else
				{
					g.beginFill(0xFFFFFFFF, 0.25);
					var w:Int = Math.floor(mSelection.width);
					var h:Int = Math.floor(mSelection.height);
					if (mTileSelector.rot90 == 1 || mTileSelector.rot90 == 3)
					{
						w = h;
						h = Math.floor(mSelection.width);
					}
					g.drawRect(currentTileX * TILE_SIZE - mCamera.x, currentTileY * TILE_SIZE - mCamera.y, TILE_SIZE * mBrushSize, TILE_SIZE * mBrushSize);
				}

				if (mDragOrigin != null)
				{
					if (mSelection == null)
						mSelection = new Rectangle();
					var oldSelection:Rectangle = mSelection.clone();
					if (mouseX + mCamera.x < mDragOrigin.x)
					{
						mSelection.left = Math.floor((mouseX + mCamera.x) / TILE_SIZE);
						mSelection.right = Math.floor(mDragOrigin.x / TILE_SIZE);
					}
					else
					{
						mSelection.left = Math.floor(mDragOrigin.x / TILE_SIZE);
						mSelection.right = Math.floor((mouseX + mCamera.x) / TILE_SIZE);
					}
					if (mouseY + mCamera.y < mDragOrigin.y)
					{
						mSelection.top = Math.floor((mouseY + mCamera.y) / TILE_SIZE);
						mSelection.bottom = Math.floor(mDragOrigin.y / TILE_SIZE);
					}
					else
					{
						mSelection.top = Math.floor(mDragOrigin.y / TILE_SIZE);
						mSelection.bottom = Math.floor((mouseY + mCamera.y) / TILE_SIZE);
					}
					if (!mSelection.equals(oldSelection))
						drawStampBrush();
				}

				if (mSelection != null)
				{
					mStampBrush.visible = true;
					mStampBrush.x = currentTileX * TILE_SIZE - mCamera.x;
					mStampBrush.y = currentTileY * TILE_SIZE - mCamera.y;
					mStampBrush.rotation = mTileSelector.rot90 * 90;
					if (mTileSelector.rot90 == 1)
						mStampBrush.x += mSelection.height * TILE_SIZE;
					else if (mTileSelector.rot90 == 2)
					{
						mStampBrush.x += mSelection.width * TILE_SIZE;
						mStampBrush.y += mSelection.height * TILE_SIZE;
					}
					if (mTileSelector.rot90 == 3)
						mStampBrush.y += mSelection.width * TILE_SIZE;
					g.endFill();
					g.lineStyle(5, 0xFF0000, 0.9);
					g.drawRect(mSelection.left * TILE_SIZE - mCamera.x, mSelection.top * TILE_SIZE - mCamera.y,
							   mSelection.width * TILE_SIZE, mSelection.height * TILE_SIZE);
				}
				
				if (mMouseDown && mDragOrigin == null && (mouseX >= 0 && mouseX < RESOLUTION_WIDTH && mouseY >= 0 && mouseY < RESOLUTION_HEIGHT))
				{
					if (currentTileX < 0 || currentTileY < 0)
					{
						mCurrentRoom.setTile(currentTileX, currentTileY,
								mSelection != null ? mCurrentRoom.getTile(Math.floor(mSelection.x), Math.floor(mSelection.y)):
								mTileSelector.selected.clone());
						if (currentTileX < 0)
						{
							mCamera.x -= currentTileX * TILE_SIZE;
							if (mSelection != null)
								mSelection.x -= currentTileX;
							currentTileX = 0;
						}
						if (currentTileY < 0)
						{
							mCamera.y -= currentTileY * TILE_SIZE;
							if (mSelection != null)
								mSelection.y -= currentTileY;
							currentTileY = 0;
						}
					}
					if (mSelection == null)
					{
						for (x in 0...mBrushSize)
							for (y in 0...mBrushSize)
								mCurrentRoom.setTile(currentTileX + x, currentTileY + y, mTileSelector.selected.clone());
					}
				}

				mTileSelector.updateOverlay();

			case EditEntities:
				var currentMouseX:Float = (mouseX + mCamera.x) / TILE_SIZE;
				var currentMouseY:Float = (mouseY + mCamera.y) / TILE_SIZE;
				
				if (mDragOrigin != null)
				{
					if (mDraggingEntities)
					{
						for (ent in mSelectedEntities)
						{
							ent.x += currentMouseX - mDragOrigin.x;
							ent.y += currentMouseY - mDragOrigin.y;
						}
						mDragOrigin.x = currentMouseX;
						mDragOrigin.y = currentMouseY;
					}
					else
					{
						if (mSelection == null)
							mSelection = new Rectangle();
						if (currentMouseX < mDragOrigin.x)
						{
							mSelection.left = currentMouseX;
							mSelection.right = mDragOrigin.x;
						}
						else
						{
							mSelection.left = mDragOrigin.x;
							mSelection.right = currentMouseX;
						}
						if (currentMouseY < mDragOrigin.y)
						{
							mSelection.top = currentMouseY;
							mSelection.bottom = mDragOrigin.y;
						}
						else
						{
							mSelection.top = mDragOrigin.y;
							mSelection.bottom = currentMouseY;
						}
						mSelectedEntities = [];
						for (e in mCurrentRoom.gameEntities)
							if (e.x > mSelection.left && e.x < mSelection.right && e.y > mSelection.top && e.y < mSelection.bottom)
								mSelectedEntities.push(e);
					}
				}
				
				var closest:GameEntity = null;
				var closestDist:Float = Math.POSITIVE_INFINITY;
				for (e in mCurrentRoom.gameEntities)
				{
					var dist:Float = (currentMouseX - e.x) * (currentMouseX - e.x) + (currentMouseY - e.y) * (currentMouseY - e.y);
					if (dist < closestDist)
					{
						closest = e;
						closestDist = dist;
					}
				}
				if (Math.sqrt(closestDist) * TILE_SIZE > ENTITY_SELECT_RADIUS)
					closest = null;
				for (e in mCurrentRoom.gameEntities)
				{
					if (e == closest)
					{
						g.lineStyle(5, 0xFFFF00, 1);
						g.beginFill(0xFF8800, 0.8);
					}
					else
					{
						g.lineStyle(3, 0x0000FF, 0.5);
						g.beginFill(0xFF0000, 0.2);
					}
					g.drawCircle(e.x * TILE_SIZE - mCamera.x, e.y * TILE_SIZE - mCamera.y, ENTITY_SELECT_RADIUS);
					g.endFill();
					if (mSelectedEntities.indexOf(e) >= 0)
					{
						g.lineStyle(5, 0xFFFFFF, 1);
						g.drawCircle(e.x * TILE_SIZE - mCamera.x, e.y * TILE_SIZE - mCamera.y, ENTITY_SELECT_RADIUS + 3);
					}
					// also draw the entity's default graphic?
				}
				if (mSelection != null)
				{
					g.endFill();
					g.lineStyle(5, 0xFF0000, 0.9);
					g.drawRect(mSelection.left * TILE_SIZE - mCamera.x, mSelection.top * TILE_SIZE - mCamera.y,
							   mSelection.width * TILE_SIZE, mSelection.height * TILE_SIZE);
				}
				if (mSelectedEntities.length == 1)
					mEntityEditor.entity = mSelectedEntities[0];
				else
					mEntityEditor.entity = null;
		}
		mCurrentRoom.offsetX = -mCamera.x;
		mCurrentRoom.offsetY = -mCamera.y;
		mAnimationContainer.update(time);
	}
	
	private function drawStampBrush():Void
	{
		var m:Matrix = new Matrix();
		m.translate( -(mSelection.x  * TILE_SIZE), -(mSelection.y * TILE_SIZE));
		if (mTileSelector.hFlip)
		{
			m.scale(-1, 1);
			m.translate(mSelection.width * TILE_SIZE, 0);
		}
		if (mTileSelector.vFlip)
		{
			m.scale(1, -1);
			m.translate(0, mSelection.height * TILE_SIZE);
		}
		
		var rect:Rectangle = new Rectangle(0, 0, mSelection.width * TILE_SIZE, mSelection.height * TILE_SIZE);
		mStampBrush.bitmapData.fillRect(new Rectangle(0, 0, mStampBrush.bitmapData.width, mStampBrush.bitmapData.height), 0x00000000);
		mStampBrush.bitmapData.draw(mCurrentRoom, m, null, null, rect);
	}
	
	private function stampSelection(targetX:Int, targetY:Int, allowSelfOverwrite:Bool):Void
	{
		var tileArray:Array<GameTile> = [];
		var w:Int = Math.floor(mSelection.width);
		var h:Int = Math.floor(mSelection.height);
		for (x in 0...w)
			for (y in 0...h)
				tileArray.push(mCurrentRoom.getTile(Math.floor(mSelection.x + x), Math.floor(mSelection.y + y)));
		for (x in 0...w)
			for (y in 0...h)
			{
				var xx:Int = x;
				var yy:Int = y;
				if (mTileSelector.hFlip || mTileSelector.rot90 == 2 || mTileSelector.rot90 == 3)
					xx = Math.floor(mSelection.width) - 1 - x;
				else 
					xx = x;
				if (mTileSelector.vFlip || mTileSelector.rot90 == 1 || mTileSelector.rot90 == 2)
					yy = Math.floor(mSelection.height) - 1 - y;
				else 
					yy = y;
				if (mTileSelector.rot90 == 1 || mTileSelector.rot90 == 3)
				{
					var tmp = yy;
					yy = xx;
					xx = tmp;
				}
				
				if (targetX + xx < mSelection.left || targetX + xx >= mSelection.right || 
					 targetY + yy < mSelection.top || targetY + yy >= mSelection.bottom || 
						allowSelfOverwrite)
				{
					var tile:GameTile = tileArray.shift();
					tile.hFlip = mTileSelector.hFlip;
					tile.vFlip = mTileSelector.vFlip;
					tile.rot90 = mTileSelector.rot90;
					mCurrentRoom.setTile(targetX + xx, targetY + yy, tile);
				}
			}
		if (allowSelfOverwrite)
			drawStampBrush();
	}
	
	private function createRoom(softCorners:Bool):Void
	{
		// For each step, rotate each tile as necessary. For subsequent steps that refer to orientations based on that tile, 
		//		rotate those orientations to match.
		
		var tileset:Int = -1;
		for (i in 0...mTileReferences.length)
			if (mTileReferences[i].tileIDs.indexOf(mTileSelector.selected.id) >= 0)
			{
				tileset = i;
				break;
			}
		var tsStart:Int = mTileReferences[tileset].tileIDs[0];
		var tsEnd:Int = tsStart + mTileReferences[tileset].tileIDs.length;
		var getTile = mCurrentRoom.getTile;
		var setTile = mCurrentRoom.setTile;
		var getCollision = mTilesetData.collision.get;
		var w:Int = Math.floor(mSelection.width);
		var h:Int = Math.floor(mSelection.height);
		var targetX:Int = Math.floor(mSelection.x);
		var targetY:Int = Math.floor(mSelection.y);
		
		// Expand with new walls if necessary
		if (targetX <= 0 || targetY <= 0)
		{
			setTile(targetX - 1, targetY - 1, new GameTile(DEBUG_CLOSED));
			if (targetX <= 0)
			{
				mCamera.x -= (targetX - 1) * TILE_SIZE;
				targetX = 1;
				mSelection.x = targetX;
			}
			if (targetY <= 0)
			{
				mCamera.y -= (targetY - 1) * TILE_SIZE;
				targetY = 1;
				mSelection.y = targetY;
			}
		}
		if (getTile(targetX + w, targetY + h) == null)
				setTile(targetX + w, targetY + h, new GameTile(DEBUG_CLOSED));
		
		// Place floors
		for (x in 0...w)
			for (y in 0...h)
			{
				if (getCollision(mTileSelector.selected.id) == CollisionType.Open)
					setTile(targetX + x, targetY + y, mTileSelector.selected)
				else
					setTile(targetX + x, targetY + y, mTilesetData.getTileByCollision(Open, tsStart, tsEnd, true));
			}
			
		// Place Diagonals in corners of new floors if requested and new floors form corners
		if (softCorners)
		{
			if (getCollision(getTile(targetX - 1, targetY).id) == Block &&
				getCollision(getTile(targetX, targetY - 1).id) == Block)
						setTile(targetX, targetY, mTilesetData.getTileByCollision(DiagNW, tsStart, tsEnd, true));
			if (getCollision(getTile(targetX + w, targetY).id) == Block &&
				getCollision(getTile(targetX + w - 1, targetY - 1).id) == Block)
						setTile(targetX + w - 1, targetY, mTilesetData.getTileByCollision(DiagNE, tsStart, tsEnd, true));
			if (getCollision(getTile(targetX - 1, targetY + h - 1).id) == Block &&
				getCollision(getTile(targetX, targetY + h).id) == Block)
						setTile(targetX, targetY + h - 1, mTilesetData.getTileByCollision(DiagSW, tsStart, tsEnd, true));
			if (getCollision(getTile(targetX + w, targetY + h - 1).id) == Block &&
				getCollision(getTile(targetX + w - 1, targetY + h).id) == Block)
						setTile(targetX + w - 1, targetY + h - 1, mTilesetData.getTileByCollision(DiagSE, tsStart, tsEnd, true));
		}
		
		// 3: Generate an array of all solid tiles touching the new floor, which probably need graphical correction
		var checkTiles:Array<{x:Int, y:Int, tile:GameTile}> = [];
		for (x in 0...w)
		{
			var t = generateCheckTile(targetX + x, targetY - 1);
			if (getCollision(t.tile.id) == Block)
				checkTiles.push(t);
			t = generateCheckTile(targetX + x, targetY + h);
			if (getCollision(t.tile.id) == Block)
				checkTiles.push(t);
		}
		for (y in 0...h)
		{
			var t = generateCheckTile(targetX - 1, targetY + y);
			if (getCollision(t.tile.id) == Block)
				checkTiles.push(t);
			t = generateCheckTile(targetX + w, targetY + y);
			if (getCollision(t.tile.id) == Block)
				checkTiles.push(t);
		}
		var t = generateCheckTile(targetX - 1, targetY - 1);
		if (getCollision(t.tile.id) == Block)
			checkTiles.push(t);
		t = generateCheckTile(targetX - 1, targetY + h);
		if (getCollision(t.tile.id) == Block)
			checkTiles.push(t);
		t = generateCheckTile(targetX + w, targetY - 1);
		if (getCollision(t.tile.id) == Block)
			checkTiles.push(t);
		t = generateCheckTile(targetX + w, targetY + h);
		if (getCollision(t.tile.id) == Block)
			checkTiles.push(t);
		var iterations:Int = 0;
		var alreadyChecked:Array<{x:Int, y:Int, tile:GameTile}> = [];
		var isAlreadyChecked = function(x:Int, y:Int):Bool 
			{
				for (o in alreadyChecked)
					if (o.x == x && o.y == y)
						return true;
				return false;
			};
		while (checkTiles.length > 0 && iterations++ < 10)
		{
			// for each tile in the list, replace it with a tile that fits with adjacent floor and floor-touching tiles 
			// UNLESS THOSE TILES ARE IN THE LIST AND HAVEN'T BEEN SET YET
			var tempCheck = checkTiles.copy();
			while(tempCheck.length > 0)
			{
				var t = tempCheck.shift();
				trace ("checking tile " + t.x + ", " + t.y);
				var north:GameTile = getTile(t.x, t.y - 1);
				var east:GameTile = getTile(t.x + 1, t.y);
				var south:GameTile = getTile(t.x, t.y + 1);
				var west:GameTile = getTile(t.x - 1, t.y);
				t.tile = mTilesetData.getCompatibleTile(t.tile, north, east, south, west, 
					tileIsInList(tempCheck, t.x, t.y - 1), tileIsInList(tempCheck, t.x + 1, t.y),
					tileIsInList(tempCheck, t.x, t.y + 1), tileIsInList(tempCheck, t.x - 1, t.y), tsStart, tsEnd, true);
				setTile(t.x, t.y, t.tile);
				alreadyChecked.push(t);
			}
			// once the list is finished, go through it again and add any touched tiles that don't fit to a new list which replaces the old list
			var newCheckTiles:Array<{x:Int, y:Int, tile:GameTile}> = [];
			for (t in checkTiles)
			{
				if (!isAlreadyChecked(t.x, t.y - 1) && mustChangeAdjacentTile(t.x, t.y, 0))
					newCheckTiles.push(generateCheckTile(t.x, t.y - 1));
				if (!isAlreadyChecked(t.x + 1, t.y) && mustChangeAdjacentTile(t.x, t.y, 1))
					newCheckTiles.push(generateCheckTile(t.x + 1, t.y));
				if (!isAlreadyChecked(t.x, t.y + 1) && mustChangeAdjacentTile(t.x, t.y, 2))
					newCheckTiles.push(generateCheckTile(t.x, t.y + 1));
				if (!isAlreadyChecked(t.x - 1, t.y) && mustChangeAdjacentTile(t.x, t.y, 3))
					newCheckTiles.push(generateCheckTile(t.x - 1, t.y));
			}
			checkTiles = newCheckTiles;
			// do this until the list is empty or until we iterate through 10 times
		}
	}
	private inline function generateCheckTile(x:Int, y:Int):{x:Int, y:Int, tile:GameTile}
	{
		return {x:x, y:y, tile:mCurrentRoom.getTile(x, y)};
	}
	private function tileIsInList(tileList:Array<{x:Int, y:Int, tile:GameTile}>, x:Int, y:Int):Bool
	{
		var inList:Bool = false;
		for (t in tileList)
			if (t.x == x && t.y == y)
			{
				inList = true;
				break;
			}
		return inList;
	}
	private function mustChangeAdjacentTile(x:Int, y:Int, side:Int):Bool
	{
		var tile:GameTile = mCurrentRoom.getTile(x, y);
		var otherTile:GameTile = switch(side)
			{
				case 0:
					mCurrentRoom.getTile(x, y - 1);
				case 1:
					mCurrentRoom.getTile(x + 1, y);
				case 2:
					mCurrentRoom.getTile(x, y + 1);
				case 3:
					mCurrentRoom.getTile(x - 1, y);
				default:
					throw ("ERROR: Side should be a value between 0 and 3 (North, East, South, West)");
			}
		if (otherTile == null)
			return false;
		if (!mTilesetData.compatibility.exists(tile.id))
			return false;
		if (!mTilesetData.compatibility.exists(otherTile.id))
			return false;
		return !mTilesetData.sideCompatible(tile, otherTile, side);
	}

	private function floodFill(x:Int, y:Int, fillWith:GameTile):Void
	{
		var replace:GameTile = mCurrentRoom.getTile(x, y);
		if (replace.equals(fillWith))
			return;
			
		var queue:Array<{x:Int, y:Int}> = [ { x:x, y:y } ]; // not points so I don't have to waste time on int/float conversions
		while (queue.length > 0)
		{
			var tx:Int = queue[queue.length - 1].x;
			var ty:Int = queue.pop().y;
			mCurrentRoom.setTile(tx, ty, fillWith);
			if (checkFillValid(tx + 1, ty, replace))
				queue.push( { x:tx + 1, y:ty } );
			if (checkFillValid(tx - 1, ty, replace))
				queue.push( { x:tx - 1, y:ty } );
			if (checkFillValid(tx, ty - 1, replace))
				queue.push( { x:tx, y:ty - 1 } );
			if (checkFillValid(tx, ty + 1, replace))
				queue.push( { x:tx, y:ty + 1 } );
		}
	}
	private inline function checkFillValid(x:Int, y:Int, replacing:GameTile):Bool
	{
		if (x < 0 || x >= mCurrentRoom.tileWidth || y < 0 || y >= mCurrentRoom.tileHeight)
			return false;
		else
			return mCurrentRoom.getTile(x, y).equals(replacing);
	}
	
	@:access(game.room.GameRoom.mTiles)
	@:access(game.room.GameRoom.updateDraw)
	private function tileUndo(redo:Bool = false):Void
	{
		var currentStack:Array<Array<Array<GameTile>>> = redo ? mTilesRedoStack : mTilesUndoStack;
		var otherStack:Array<Array<Array<GameTile>>> = redo ? mTilesUndoStack : mTilesRedoStack;
		if (currentStack.length > 0)
		{
			var tiles:Array<Array<GameTile>> = currentStack.pop();
			otherStack.push(mCurrentRoom.mTiles);
			mCurrentRoom.mTiles = tiles;
			mCurrentRoom.updateDraw();
		}
	}
	
	@:access(game.room.GameRoom.mTiles)
	private function stackTileUndoState():Void
	{
		mTilesRedoStack = [];
		var tiles:Array<Array<GameTile>> = [];
		for (arr in mCurrentRoom.mTiles)
		{
			var newColumn:Array<GameTile> = [];
			for (t in arr)
				newColumn.push(t.clone());
			tiles.push(newColumn);
		}
		mTilesUndoStack.push(tiles);
		if (mTilesUndoStack.length > MAX_TILE_UNDO_STATES)
			mTilesUndoStack.shift();
	}
	
	private function entityUndo(redo:Bool = false):Void
	{
		var currentStack:Array<Array<GameEntity>> = redo ? mEntitiesRedoStack : mEntitiesUndoStack;
		var otherStack:Array<Array<GameEntity>> = redo ? mEntitiesUndoStack : mEntitiesRedoStack;
		if (currentStack.length > 0)
		{
			var entities:Array<GameEntity> = currentStack.pop();
			otherStack.push(mCurrentRoom.gameEntities);
			mCurrentRoom.gameEntities = entities;
		}
	}
	
	private function stackEntityUndoState():Void
	{
		mEntitiesRedoStack = [];
		var entites:Array<GameEntity> = [];
		for (e in mCurrentRoom.gameEntities)
			entites.push(e.clone());
		
		mEntitiesUndoStack.push(entites);
		if (mEntitiesUndoStack.length > MAX_ENTITY_UNDO_STATES)
			mEntitiesUndoStack.shift();
	}

	private function generateDebugTileset():Void
	{
		var s:Sprite = new Sprite();
		var bmp:BitmapData = new BitmapData(TILE_SIZE * 2, TILE_SIZE * 3, true, 0x00000000);
		var rects:Array<Rectangle> = [];
		for (i in 0...6)
			rects.push(new Rectangle(TILE_SIZE * (i % 2), TILE_SIZE * Math.floor(i / 2), TILE_SIZE, TILE_SIZE));
		for (i in 0...Type.allEnums(CollisionType).length)
		{
			s.graphics.beginFill(0xFFFFFFFF, 0.7);
			s.graphics.lineStyle(4, 0xFF000000, 1);
			var m:Matrix = new Matrix();
			m.translate(rects[i].x, rects[i].y);
			switch (i)
			{
				case 0:
				case 1:
					s.graphics.drawRect(0, 0, Game.TILE_SIZE, Game.TILE_SIZE);
					s.graphics.endFill();
					s.graphics.moveTo(0, 0);
					s.graphics.lineTo(Game.TILE_SIZE, Game.TILE_SIZE);
					s.graphics.moveTo(Game.TILE_SIZE, 0);
					s.graphics.lineTo(0, Game.TILE_SIZE);
				case 2:
					s.graphics.moveTo(Game.TILE_SIZE, 0);
					s.graphics.lineTo(0, Game.TILE_SIZE);
					s.graphics.lineTo(0, 0);
					s.graphics.lineTo(Game.TILE_SIZE, 0);
				case 3:
					s.graphics.moveTo(0, 0);
					s.graphics.lineTo(Game.TILE_SIZE, Game.TILE_SIZE);
					s.graphics.lineTo(Game.TILE_SIZE, 0);
					s.graphics.lineTo(0, 0);
				case 4:
					s.graphics.moveTo(0, 0);
					s.graphics.lineTo(Game.TILE_SIZE, Game.TILE_SIZE);
					s.graphics.lineTo(0, Game.TILE_SIZE);
					s.graphics.lineTo(0, 0);
				case 5:
					s.graphics.moveTo(Game.TILE_SIZE, 0);
					s.graphics.lineTo(0, Game.TILE_SIZE);
					s.graphics.lineTo(Game.TILE_SIZE, Game.TILE_SIZE);
					s.graphics.lineTo(Game.TILE_SIZE, 0);
			}
			bmp.draw(s, m);
			s.graphics.clear();
		}
		var ref:TileReference = TileManager.addTilesByData(bmp, rects);
		mTileReferences.push(ref);
		mTilesetData.collision.set(ref.tileIDs[0], Open);
		mTilesetData.collision.set(ref.tileIDs[1], Block);
		mTilesetData.collision.set(ref.tileIDs[2], DiagNW);
		mTilesetData.collision.set(ref.tileIDs[3], DiagNE);
		mTilesetData.collision.set(ref.tileIDs[4], DiagSW);
		mTilesetData.collision.set(ref.tileIDs[5], DiagSE);
	}
	#if air
	static private function crawlDirTreeFor(folderName:String):String
	{
		var f:File = new File(File.applicationDirectory.nativePath);
		while (f != null)
			if (f.resolvePath(folderName).exists)
				return f.resolvePath(folderName).nativePath + "\\";
			else
				f = f.resolvePath("..\\");
		return null;
	}
	#else
	static private function crawlDirTreeFor(folderName:String):String
	{
		var path:String = Sys.getCwd();
		var loop:Bool = true;
		do
		{
			var curDir:Array<String> = null;
			try
			{	curDir = FileSystem.readDirectory(path);		}
			catch (e:Error)
			{	return null;	}
			for (s in curDir)
				if (s.toLowerCase().indexOf(folderName) != -1 && FileSystem.isDirectory(path + s))
					return FileSystem.fullPath(path + s + "\\");

			loop = path != FileSystem.fullPath(path + "..\\");
			path = FileSystem.fullPath(path + "..\\");
		}
		while (loop);
		return null;
	}
	#end
}
private enum GameMode
{
	Play;
	EditTiles;
	EditEntities;
}