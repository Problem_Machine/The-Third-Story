package game.room;
import com.problemmachine.bitmap.bitmapmanager.BitmapManager;
import com.problemmachine.bitmap.bitmapmanager.BitmapManagerEvent;
import com.problemmachine.bitmap.bitmapmanager.BitmapManagerQuery;
import com.problemmachine.tools.xml.XMLTools;
import flash.display.Bitmap;
import flash.display.Sprite;
import flash.geom.Point;
import game.gametile.data.CollisionType;
import game.gametile.GameTile;
import game.gametile.data.TilesetData;
import game.gametile.manager.TileManager;
import game.entity.GameEntity;
import openfl.display.Tile;
import openfl.display.Tilemap;

class GameRoom extends Sprite
{
	static private var sRoomList:Map<Int, GameRoom> = new Map<Int, GameRoom>();
	
	public var id(default, null):Int;
	public var gameEntities:Array<GameEntity>;
	public var artPath(default, set):String;
	public var tileWidth(get, set):Int;
	public var tileHeight(get, set):Int;
	public var offsetX(default, set):Float;
	public var offsetY(default, set):Float;
	private var mTiles:Array<Array<GameTile>>;
	
	private var mTileDisplay(default, null):Tilemap;
	private var mArtBitmap:Bitmap;

	public function new(id:Int, ?artPath:String, tileWidth:Int, tileHeight:Int, renderWidth:Int, renderHeight:Int) 
	{
		super();
		if (sRoomList.exists(id))
			throw "ERROR: ID " + Std.string(id) + " ALREADY EXISTS!";
		sRoomList.set(id, this);
		
		mArtBitmap = new Bitmap();
		mTileDisplay = TileManager.generateTilemap(renderWidth, renderHeight, false);
		addChild(mArtBitmap);
		addChild(mTileDisplay);
		this.id = id;
		this.gameEntities = [];
		this.artPath = artPath;
		this.mTiles = [[]];
	}
	private inline function get_tileWidth():Int
	{
		return mTiles.length;
	}
	private inline function set_tileWidth(val:Int):Int
	{
		while (mTiles.length > val)
			mTiles.pop();
		while (mTiles.length < val)
		{
			var x:Int = mTiles.length;
			for (y in 0...tileHeight)
				setTile(x, y, new GameTile(Game.DEBUG_CLOSED));
		}
		return val;
	}
	private inline function get_tileHeight():Int
	{
		return mTiles[0].length;
	}
	private inline function set_tileHeight(val:Int):Int
	{
		while (mTiles[0].length > val)
			for (arr in mTiles)
				arr.pop();
		while (mTiles[0].length < val)
			for (arr in mTiles)
				arr.push(new GameTile(Game.DEBUG_CLOSED));
		return val;
	}
	
	private inline function set_offsetX(val:Float):Float
	{
		if (val != offsetX)
		{
			offsetX = val;
			for (x in 0...mTiles.length)
				for (y in 0...mTiles[0].length)
					mTileDisplay.getTileAt(y + x * tileHeight).x = (x + 0.5) * Game.TILE_SIZE + offsetX;
		}
		return val;
	}
	private inline function set_offsetY(val:Float):Float
	{
		if (val != offsetY)
		{
			offsetY = val;
			for (x in 0...mTiles.length)
				for (y in 0...mTiles[0].length)
					mTileDisplay.getTileAt(y + x * tileHeight).y = (y + 0.5) * Game.TILE_SIZE + offsetY;
		}
		return val;
	}
	
	private inline function set_artPath(val:String):String
	{
		if (val != artPath)
		{
			if (artPath != null && artPath != "")
			{
				BitmapManager.stopListeningForCompletion(artPath, artListener);
				BitmapManager.unlock(artPath, this);
				mArtBitmap.bitmapData = null;
			}
			artPath = val;
			if (BitmapManager.query(artPath) != BitmapManagerQuery.Ready)
			{
				BitmapManager.loadAndLock(artPath, this);
				BitmapManager.startListeningForCompletion(artPath, artListener);
			}
			else
				mArtBitmap.bitmapData = BitmapManager.request(artPath);
		}
		return val;
	}
	private function artListener(e:BitmapManagerEvent):Void
	{
		BitmapManager.stopListeningForCompletion(artPath, artListener);
		mArtBitmap.bitmapData = BitmapManager.request(artPath);
	}
	
	public function testCollision(centerX:Float, centerY:Float, ?width:Float, ?height:Float, ?radius:Float, collisions:Map<Int, CollisionType>):Point
	{
		if (width == null && height == null)	
			width = height = radius;
		else if (width == null)
			width = height;
		else if (height == null)
			height = width;
		if (width == null && height == null && radius == null)
			throw "ERROR: testCollision requires either width or height or radius";
		var incursionX:Float = 0;
		var incursionY:Float = 0;
		
		var testLeft:Bool = true;
		var testTop:Bool = true;
		var testRight:Bool = true;
		var testBottom:Bool = true;
		
		var l:Float = centerX - width * 0.5;
		var r:Float = centerX + width * 0.5;
		var t:Float = centerY - height * 0.5;
		var b:Float = centerY + height * 0.5;
		
		// Test room bounds
		if (t < 0)
			incursionY = t;
		else if (b > tileHeight)
			incursionY = b - tileHeight;
		if (l < 0)
			incursionX = l;
		if (r > tileWidth - 1)
			incursionX = r - tileWidth;
			
		l = centerX - width * 0.5 - incursionX;
		r = centerX + width * 0.5 - incursionX;
		t = centerY - height * 0.5 - incursionY;
		b = centerY + height * 0.5 - incursionY;
		
		// CIRCULAR COLLISION DETECTION
		if (radius != null)
		{
			for (x in Math.floor(l)...Math.ceil(r))
				for (y in Math.floor(t)...Math.ceil(b))
				{
					var c:CollisionType = getCollision(x, y, collisions);
					if (c != Open)
					{
						var cx:Float = centerX - incursionX;
						var cy:Float = centerY - incursionY;
						// test to see if the closest point to the entity is closer than the entity's radius
						if (c == Block || 
							(c == DiagNW && cx < x + 0.5 && cy < y + 0.5) ||
							(c == DiagNE && cx > x + 0.5 && cy < y + 0.5) ||
							(c == DiagSW && cx < x + 0.5 && cy > y + 0.5) ||
							(c == DiagSE && cx > x + 0.5 && cy > y + 0.5))
						{
							// Find the closest point in the block to the center of the entity
							var testX:Float = Math.min(Math.max(x, cx), x + 1);
							var testY:Float = Math.min(Math.max(y, cy), y + 1);
							
							if ((testX - cx) * (testX - cx) + (testY - cy) * (testY - cy) < radius * radius)
							{
								// block collision
								var angle:Point = new Point(cx - testX, cy - testY);
								angle.normalize(angle.length - radius);
								
								incursionX += angle.x;
								incursionY += angle.y;
							}
						}
						else
						{
							// tx is the x coordinate of the tile
							// ty is the y coordinate of the tile
							// NW -> y = -x + (ty + 1 + tx)
							// b = ty + tx + 1
							// NE -> y = x + ty - tx
							// b = ty - tx
							// SE and NW are the same; NE and SW are the same.
							// 
							// NW opposite line y = x + cy - cx
							// NE opposite line y = -x + cy + cx
							//
							// NW -> y = -1x + (ty + 1 + tx), y = x + cy - cx
							// x = (ty + 1 + tx - cy + cx) / 2
							// y = x + cy - cx
							// NE -> y = x + ty - tx, y = -x + cy + cx
							// x = (cy + cx - ty + tx) / 2
							// y = -x + cy + cx
							var testPoint:Point = new Point(0, 0);
							if (c == DiagNW || c == DiagSE)
							{
								testPoint.x = (y + 1 + x - cy + cx) / 2;
								testPoint.y = testPoint.x + cy - cx;
							}
							else
							{
								testPoint.x = (cy + cx - y + x) / 2;
								testPoint.y = -testPoint.x + cy + cx;
							}
								
							var dist:Float = Math.sqrt((testPoint.x - cx) * (testPoint.x - cx) + (testPoint.y - cy) * (testPoint.y - cy));
							if (dist < radius)
							{
								testPoint.x = testPoint.x - cx;
								testPoint.y = testPoint.y - cy;
								
								testPoint.normalize(radius - dist);
								incursionX += testPoint.x;
								incursionY += testPoint.y;
							}
						}
					}
				}
		}	
		// RECTANGLE COLLISION DETECTION
		else
		{
			// test diagonals
			// first, check to see if the corner is past the angle of the diagonal (cx < tx || cx > tx + 1 || cy < ty || cy > ty + 1)
			//	If so then treat it as a block collision, since only corners can properly interact with diagonals
			// Now get the incursion depth: 1 + tx + ty - cx - cy
			// 	We know this is the depth because the incursion has to be equal to the difference between the length of the legs (1)
			//	and the length of the legs of the triangles at x, cy and cx, y (cy - ty and cx - tx)
			//	The sum is 1 - (cx - tx) - (cy - ty) OR 1 + tx + ty - cx - cy
			// If this value is below 0 there's no collision. If it's above zero, correct these values to the appropriate signs for the diagonal
			var c:CollisionType = getCollision(Math.floor(l), Math.floor(t), collisions);
			if (c != CollisionType.Open)
			{
				//TODO: Because this is the first collision calculated, we can slide under diagonal tiles
				if (c != DiagNW)
				{
					// treat as block tile
					// check to see if horizontal/vertical sides of block are other blocks to avoid getting caught on tile edges
					// if both sides are open, lowest depth incursion is used
					if ((getCollision(Math.floor(l + 1), Math.floor(t), collisions) != Open) || 
						(Math.abs((t % 1) - 1) < Math.abs((l % 1) - 1) && (getCollision(Math.floor(l), Math.floor(t + 1), collisions) == Open)))
						incursionY += (t % 1) - 1;
					else
						incursionX += (l % 1) - 1;
					testLeft = testTop = false;
				}
				else
				{
					var depth:Float = Math.floor(l) - l + Math.floor(t) - t + 1;
					if (depth > 0)
					{
						incursionY -= depth * 0.5;
						incursionX -= depth * 0.5;
						testLeft = testTop = false;
					}
				}
				l = centerX - width * 0.5 - incursionX;
				r = centerX + width * 0.5 - incursionX;
				t = centerY - height * 0.5 - incursionY;
				b = centerY + height * 0.5 - incursionY;
			}
			c = getCollision(Math.floor(r), Math.floor(t), collisions);
			if (c != CollisionType.Open)
			{
				if (c != DiagNE)
				{
					// treat as block tile
					if ((getCollision(Math.floor(r - 1), Math.floor(t), collisions) != Open) || 
					(Math.abs((t % 1) - 1) < Math.abs(r % 1) && (getCollision(Math.floor(r), Math.floor(t + 1), collisions) == Open)))
						incursionY += (t % 1) - 1;
					else
						incursionX += r % 1;
					testTop = testRight = false;
				}
				else
				{
					var depth:Float = r - Math.floor(r) - t + Math.floor(t);
					if (depth > 0)
					{
						incursionX += depth * 0.5;
						incursionY -= depth * 0.5;
						testLeft = testTop = false;
					}
				}
				l = centerX - width * 0.5 - incursionX;
				r = centerX + width * 0.5 - incursionX;
				t = centerY - height * 0.5 - incursionY;
				b = centerY + height * 0.5 - incursionY;
			}
			c = getCollision(Math.floor(l), Math.floor(b), collisions);
			if (c != CollisionType.Open)
			{
				if (c != DiagSW)
				{
					// treat as block tile
					if ((getCollision(Math.floor(l + 1), Math.floor(b), collisions) != Open) || 
						(Math.abs(b % 1) < Math.abs((l % 1) - 1) && (getCollision(Math.floor(l), Math.floor(b - 1), collisions) == Open)))
						incursionY += b % 1;
					else
						incursionX += (l % 1) - 1;
					testLeft = testBottom = false;
				}
				else
				{
					var depth:Float = Math.floor(l) - l + b - Math.floor(b);
					if (depth > 0)
					{
						incursionX -= depth * 0.5;
						incursionY += depth * 0.5;
						testLeft = testTop = false;
					}
				}
				l = centerX - width * 0.5 - incursionX;
				r = centerX + width * 0.5 - incursionX;
				t = centerY - height * 0.5 - incursionY;
				b = centerY + height * 0.5 - incursionY;
			}
			c = getCollision(Math.floor(r), Math.floor(b), collisions);
			if (c != CollisionType.Open)
			{
				if (c != DiagSE)
				{
					// treat as block tile
					if ((getCollision(Math.floor(r - 1), Math.floor(b), collisions) != Open) || 
						(Math.abs(b % 1) < Math.abs(r % 1) && (getCollision(Math.floor(r), Math.floor(b - 1), collisions) == Open)))
						incursionY += b % 1;
					else
						incursionX += r % 1;
					testRight = testBottom = false;
				}
				else
				{
					var depth:Float = -1 - Math.floor(r) + r - Math.floor(b) + b;
					if (depth > 0)
					{
						incursionX += depth * 0.5;
						incursionY += depth * 0.5;
						testLeft = testTop = false;
					}
				}
				l = centerX - width * 0.5 - incursionX;
				r = centerX + width * 0.5 - incursionX;
				t = centerY - height * 0.5 - incursionY;
				b = centerY + height * 0.5 - incursionY;
			}
			
			// test sides
			if (testTop)
				for (x in Math.floor(l + 1)...Math.ceil(r - 1))
					if (getCollision(x, Math.floor(t), collisions) != Open)
						incursionY = (t % 1) - 1;
			if (testBottom)		
				for (x in Math.floor(l + 1)...Math.ceil(r - 1))
					if (getCollision(x, Math.floor(b), collisions) != Open)
						incursionY = b % 1;
						
			if (testLeft)
				for (y in Math.floor(t + 1)...Math.ceil(b - 1))
					if (getCollision(Math.floor(l), y, collisions) != Open)
						incursionX = (l % 1) - 1;
					
			if (testRight)
				for (y in Math.floor(t + 1)...Math.ceil(b - 1))
					if (getCollision(Math.floor(r), y, collisions) != Open)
						incursionX = r % 1;
		}
					
		// return a point object containing the x and y corrections
		return new Point(incursionX, incursionY);
		
		// incursions should be simple for solid tiles. For angled tiles, pull from EverEnding code
		// if we have incursions on top&bottom or on left&right, see if we can salvage the situation by shifting left/right
		// 		if we can't, return null
	}
	
	private function getCollision(x:Int, y:Int, collisions:Map<Int, CollisionType>):CollisionType
	{
		if (x < 0 || y < 0 || x >= tileWidth || y >= tileHeight)
			throw ("ERROR: Tile " + x + ", " + y + " outside of bounds");
		var tile:GameTile = mTiles[x][y];
		var c:CollisionType = collisions.get(tile.id);
		
		if (tile.hFlip)
			c = TilesetData.flipCollisionHorizontally(c);
		if (tile.vFlip)
			c = TilesetData.flipCollisionVertically(c);
		for (i in 0...tile.rot90)
			c = TilesetData.rotateCollisionRight(c);
		return c;
	}
	
	private function updateDraw():Void
	{
		mTileDisplay.removeTiles();
		for (x in 0...mTiles.length)
			for (y in 0...mTiles[0].length)
			{
				var t:GameTile = mTiles[x][y];
				mTileDisplay.addTile(new Tile(t.id, (x + 0.5 ) * Game.TILE_SIZE + offsetX, (y + 0.5 ) * Game.TILE_SIZE + offsetY, 
					t.hFlip ? -1:1, t.vFlip ? -1:1, t.rot90 * 90, Game.TILE_SIZE * 0.5, Game.TILE_SIZE * 0.5));
			}
	}
	
	public function getTile(x:Int, y:Int):GameTile
	{
		if (x < 0 || x >= tileWidth || y < 0 || y >= tileHeight)
			return null;
		else
			return mTiles[x][y].clone();
	}
	public function setTile(x:Int, y:Int, tile:GameTile):Void
	{
		if (tile == null)
			tile = new GameTile(Game.DEBUG_CLOSED);
		var w:Int = tileWidth;
		var h:Int = tileHeight;
		while (x > mTiles.length - 1)
		{
			var arr:Array<GameTile> = [];
			while (arr.length < h)
				arr.push(new GameTile(Game.DEBUG_CLOSED));
			mTiles.push(arr); 
		}
		while (x < 0)
		{
			var arr:Array<GameTile> = [];
			while (arr.length < h)
				arr.push(new GameTile(Game.DEBUG_CLOSED));
			mTiles.unshift(arr); 
			++x;
		}
		while (y > mTiles[0].length - 1)
		{
			for (arr in mTiles)
				arr.push(new GameTile(Game.DEBUG_CLOSED));
		}
		while (y < 0)
		{
			for (arr in mTiles)
				arr.unshift(new GameTile(Game.DEBUG_CLOSED));
			++y;
		}
		mTiles[x][y].id = tile.id;
		mTiles[x][y].hFlip = tile.hFlip;
		mTiles[x][y].vFlip = tile.vFlip;
		mTiles[x][y].rot90 = tile.rot90;
		updateDraw();
	}
	
	static public function getUnusedID():Int
	{
		var i:Int = 0;
		while (sRoomList.exists(i))
			++i;
		return i;
	}
	
	public function toXML():Xml
	{
		var xml:Xml = Xml.parse(
			"<room>\n" +
			"	<id>" + Std.string(id) + "</id>\n" +
			"	<width>" + Std.string(tileWidth) + "</width>\n" +
			"	<height>" + Std.string(tileWidth) + "</height>\n" +
			"	<artPath>" + artPath + "</artPath>\n" +
			"</room>").firstElement();
		for (e in gameEntities)
			xml.addChild(e.toXML());
		for (arr in mTiles)
			for (t in arr)
				xml.addChild(t.toXML());
		return xml;
	}
	
	static public function fromXML(xml:Xml):GameRoom
	{
		var gr:GameRoom = new GameRoom(Std.parseInt(XMLTools.getVal(xml, "id")), XMLTools.getVal(xml, "artPath"),
			Std.parseInt(XMLTools.getVal(xml, "width")), Std.parseInt(XMLTools.getVal(xml, "height")), flash.Lib.current.stage.stageWidth, flash.Lib.current.stage.stageHeight);
		var x:Int = 0;
		var y:Int = 0;
		for (tx in xml.elementsNamed("tile"))
		{
			gr.setTile(x++, y, GameTile.fromXML(tx));
			if (x >= gr.tileWidth)
			{
				x = 0;
				++y;
			}
		}
		for (ex in xml.elementsNamed("entity"))
			gr.gameEntities.push(GameEntity.fromXML(ex));
		return gr;
	}
}