package game.editor;

import com.problemmachine.ui.scrollwindow.ScrollWindow;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.FocusEvent;
import flash.events.KeyboardEvent;
import flash.text.TextField;
import flash.text.TextFieldType;
import flash.text.TextFormat;
import flash.ui.Keyboard;
import game.entity.GameEntity;

class EntityVariableEditor extends Sprite
{
	public var entity(default, set):GameEntity;
	
	private var mScrollWindow:ScrollWindow;
	private var mVarNodes:Array<VariableInputNode>;
	
	private var mTextColor:UInt;
	private var mWidth:Float;
	private var mHeight:Float;
	
	public function new(width:Float, height:Float, borderColor:UInt, backgroundColor:UInt, textColor:UInt) 
	{
		super();
		mWidth = width;
		mHeight = height;
		mTextColor = textColor;
		mScrollWindow = new ScrollWindow(width, height, borderColor, backgroundColor, width * 0.05);
		mScrollWindow.enableHorizontalResize = mScrollWindow.enableMovement = mScrollWindow.enableVerticalResize = false;
		addChild(mScrollWindow.panel);
		mVarNodes = [];
	}
	
	private inline function set_entity(val:GameEntity):GameEntity
	{
		if (val != entity)
		{
			clearInputs();
			entity = val;
			populateInputs();
		}
		return val;
	}
	
	private function clearInputs():Void
	{
		var n:VariableInputNode = null;
		while((n = mVarNodes.pop()) != null)
		{
			n.removeEventListener(VariableInputNode.EVENT_MODIFIED, variableModifiedListener);
			removeChild(n);
		}
	}
	
	@:access(game.entity.GameEntity.mLocalVariables)
	private function populateInputs():Void
	{
		var h:Float = 0;
		for (k in entity.mLocalVariables.keys())
		{
			var n:VariableInputNode = new VariableInputNode(k, entity.mLocalVariables.get(k), mTextColor, 20, mWidth);
			n.y = h;
			addChild(n);
			h += n.height;
			n.addEventListener(VariableInputNode.EVENT_MODIFIED, variableModifiedListener);
		}
	}
	
	@:access(game.entity.GameEntity.mLocalVariables)
	private function variableModifiedListener(e:Event):Void
	{
		var n:VariableInputNode = cast e.target;
		entity.mLocalVariables.remove(n.originalName);
		entity.mLocalVariables.set(n.nameInput.text, n.valueInput.text);
		n.originalName = n.nameInput.text;
		n.originalValue = n.valueInput.text;
		dispatchEvent(new Event(Event.CHANGE));
	}
}


private class VariableInputNode extends Sprite
{
	static public inline var EVENT_MODIFIED:String = "varModified";
	
	public var originalName:String;
	public var originalValue:String;
	public var nameInput:TextField;
	public var valueInput:TextField;
	
	public function new(name:String, value:String, textColor:UInt, height:Float, width:Float)
	{
		super();
		
		originalName = name;
		originalValue = value;
		
		nameInput = new TextField();
		nameInput.defaultTextFormat = new TextFormat(null, 16, textColor);
		nameInput.text = name;
		nameInput.border = true;
		nameInput.borderColor = textColor;
		nameInput.type = TextFieldType.INPUT;
		nameInput.height = height;
		nameInput.width = width * 0.2;
		nameInput.addEventListener(FocusEvent.FOCUS_OUT, loseFocusListener);
		nameInput.addEventListener(KeyboardEvent.KEY_DOWN, enterListener);
		addChild(nameInput);
		
		valueInput = new TextField();
		valueInput.defaultTextFormat = new TextFormat(null, 16, textColor);
		valueInput.text = value;
		valueInput.border = true;
		valueInput.borderColor = textColor;
		valueInput.type = TextFieldType.INPUT;
		valueInput.height = height;
		valueInput.width = width * 0.78;
		valueInput.x = width * 0.22;
		valueInput.addEventListener(FocusEvent.FOCUS_OUT, loseFocusListener);
		valueInput.addEventListener(KeyboardEvent.KEY_DOWN, enterListener);
		addChild(valueInput);
	}
	
	private function enterListener(e:KeyboardEvent):Void
	{
		if (e.keyCode == Keyboard.ENTER)
			stage.focus = stage;
		if (e.keyCode == Keyboard.TAB)
			stage.focus = (stage.focus == nameInput) ? valueInput : nameInput;
	}
	
	private function loseFocusListener(e:FocusEvent):Void
	{
		if (stage.focus != nameInput && stage.focus != valueInput)
			if (valueInput.text != originalValue || nameInput.text != originalName)
				dispatchEvent(new Event(EVENT_MODIFIED));
	}
}