package game.editor;

import com.problemmachine.tools.math.IntMath;
import com.problemmachine.ui.scrollwindow.ScrollWindow;
import flash.display.DisplayObject;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.FocusEvent;
import flash.events.KeyboardEvent;
import flash.events.MouseEvent;
import flash.text.TextField;
import flash.text.TextFieldType;
import flash.text.TextFormat;
import flash.ui.Keyboard;
import game.entity.GameEntity;

@:access(game.entity.GameEntity.mLocalVariables)
class EntityEditor extends Sprite
{
	public var entity(default, set):GameEntity;
	public var currentlyEditing(get, never):Bool;
	
	private var mVariablesWindow:ScrollWindow;
	private var mVarNodes:Array<VariableInputNode>;
	
	private var mSelectedVariable:String;
	
	private var mEditingWindow:ScrollWindow;
	private var mEditableText:TextField;
	private var mTextColor:UInt;
	private var mWidth:Float;
	private var mHeight:Float;

	public function new(width:Float, height:Float, borderColor:UInt, backgroundColor:UInt, textColor:UInt)
	{
		super();
		
		mWidth = width;
		mHeight = height;
		mTextColor = textColor;
		
		mEditingWindow = new ScrollWindow(mWidth, mHeight * 0.5, borderColor, backgroundColor, width * 0.05);
		mEditingWindow.enableHorizontalResize =mEditingWindow.enableMovement = mEditingWindow.enableVerticalResize = false;
		addChild(mEditingWindow.panel);
		mVariablesWindow = new ScrollWindow(mWidth, mHeight * 0.5, borderColor, backgroundColor, width * 0.05);
		mVariablesWindow.enableHorizontalResize = mVariablesWindow.enableMovement = mVariablesWindow.enableVerticalResize = false;
		addChild(mVariablesWindow.panel);
		mVariablesWindow.y = mHeight * 0.5;
		mVarNodes = [];
		
		mEditableText = new TextField();
		mEditableText.multiline = true;
		mEditableText.type = TextFieldType.INPUT;
		mEditableText.defaultTextFormat = new TextFormat(null, 20, 0xFFFFFF);
		mEditableText.mouseWheelEnabled = false;
		mEditableText.addEventListener(KeyboardEvent.KEY_DOWN, tabListener);
		mEditableText.addEventListener(Event.CHANGE, changeTextListener);
		mEditingWindow.addChild(mEditableText);
		
		entity = null;
	}
	
	@:access(game.entity.GameEntity._INIT)
	private inline function set_entity(val:GameEntity):GameEntity
	{
		if (val != entity)
		{
			clearVariables();
			entity = val;
			mSelectedVariable = GameEntity._INIT;
			populateVariables();
			mEditableText.text = (entity != null ? entity.mLocalVariables.get(mSelectedVariable) : "");
			updateTextDisplay();
		}
		mEditableText.mouseEnabled = (entity != null);		
		return val;
	}
	
	private inline function get_currentlyEditing():Bool
	{
		return (stage != null && stage.focus == mEditableText);
	}
	
	private function clearVariables():Void
	{
		var n:VariableInputNode = null;
		while((n = mVarNodes.pop()) != null)
		{
			n.removeEventListener(VariableInputNode.EVENT_MODIFIED, variableModifiedListener);
			n.removeEventListener(MouseEvent.CLICK, variableSelectedListener);
			mVariablesWindow.removeChild(n);
		}
	}
	
	@:access(game.entity.GameEntity.mLocalVariables)
	private function populateVariables():Void
	{
		var h:Float = 0;
		if (entity != null)
			for (k in entity.mLocalVariables.keys())
			{
				var n:VariableInputNode = new VariableInputNode(k, entity.mLocalVariables.get(k), mTextColor, 20, mWidth);
				n.y = h;
				mVariablesWindow.addChild(n);
				h += n.height;
				n.addEventListener(VariableInputNode.EVENT_MODIFIED, variableModifiedListener);
				n.addEventListener(MouseEvent.CLICK, variableSelectedListener);
				mVarNodes.push(n);
			}
	}
	
	@:access(game.entity.GameEntity.mLocalVariables)
	private function variableModifiedListener(e:Event):Void
	{
		var n:VariableInputNode = cast e.target;
		entity.mLocalVariables.remove(n.originalName);
		entity.mLocalVariables.set(n.nameInput.text, n.valueInput.text);
		n.originalName = n.nameInput.text;
		n.originalValue = n.valueInput.text;
		mSelectedVariable = n.originalName;
		updateTextDisplay();
		dispatchEvent(new Event(Event.CHANGE));
	}
	
	private function variableSelectedListener(e:Event):Void
	{
		var n:VariableInputNode = cast e.currentTarget;
		mSelectedVariable = n.originalName;
		mEditableText.text = (entity != null ? entity.mLocalVariables.get(mSelectedVariable) : "");
		updateTextDisplay();
	}
	
	private function changeTextListener(?e:Event):Void
	{
		mEditableText.width = Math.max(mWidth * 0.9, mEditableText.textWidth);
		mEditableText.height = Math.max(mHeight * 0.45, mEditableText.textHeight);
		updateTextDisplay();
		entity.mLocalVariables.set(mSelectedVariable, mEditableText.text);
		for (n in mVarNodes)
			if (n.originalName == mSelectedVariable)
			{
				n.originalValue = n.valueInput.text = mEditableText.text;
				break;
			}
	}
	private function updateTextDisplay():Void
	{
		try
		{
			Xml.parse(mEditableText.text);
			mEditableText.textColor = 0xAAAAAA;
			//highlightKeywords();
			//highlightVariables();
			//highlightGlobalVariables();
		}
		catch (d:Dynamic)
		{
			mEditableText.textColor = 0xFF0000;
		}
	}
	
	private function tabEater(e:FocusEvent):Void
	{
		e.preventDefault();
	}
	
	private function tabListener(e:KeyboardEvent):Void
	{
		if (e.keyCode == Keyboard.TAB)
		{
			var i:Int = mEditableText.selectionBeginIndex;
			if (e.shiftKey)
			{
				tabScriptText(true);
				// move all selected lines, or current line, back one tab
			}
			else if (mEditableText.selectionEndIndex == i)
			{
				mEditableText.text = mEditableText.text.substring(0, i) + "\t" + mEditableText.text.substring(i, mEditableText.text.length);
				mEditableText.setSelection(i + 1, i + 1);
			}
			else
			{
				tabScriptText();
				// move all selected lines forward one tab
			}
		}
	}
	
	private function tabScriptText(backwards:Bool = false):Void
	{
		var newLines:Array<Int> = [];
		newLines.push(mEditableText.text.substr(0, mEditableText.selectionBeginIndex).lastIndexOf("\n"));
		var i:Int = IntMath.max(newLines[0], 0);
		if (newLines[0] < 0)
			newLines.pop();
		while ((i = mEditableText.text.indexOf("\n", i + 1)) >= 0)
		{
			if (i >= mEditableText.selectionEndIndex)
				break;
			newLines.push(i);
		}
		for (j in 0...newLines.length)
		{
			var index:Int = newLines[newLines.length - 1 - j] + 1;
			if (backwards)
			{
				if (mEditableText.text.charAt(index) == "\t")
					mEditableText.text = mEditableText.text.substring(0, index) + mEditableText.text.substring(index + 1, mEditableText.text.length);
			}
			else
			{
				mEditableText.text = mEditableText.text.substring(0, index) + "\t" + mEditableText.text.substring(index, mEditableText.text.length);
			}
		}
	}
	
	private function highlightKeywords():Void
	{
		var keyWords:TextFormat = mEditableText.defaultTextFormat.clone();
		keyWords.color = 0xFFFFFF;
		highlightString(mEditableText, "init", keyWords);
		highlightString(mEditableText, "interact", keyWords);
		highlightString(mEditableText, "update", keyWords);
		highlightString(mEditableText, "die", keyWords);
	}
	
	@:access(game.entity.GameEntity.mLocalVariables)
	private function highlightVariables():Void
	{
		var varLight:TextFormat = mEditableText.defaultTextFormat.clone();
		varLight.color = 0x88EEEE;
		for (k in entity.mLocalVariables.keys())
			highlightString(mEditableText, k, varLight);
	}
	
	@:access(game.entity.GameEntity.sGlobalVariables)
	private function highlightGlobalVariables():Void
	{
		var varLight:TextFormat = mEditableText.defaultTextFormat.clone();
		varLight.color = 0x88FF88;
		for (k in GameEntity.sGlobalVariables.keys())
			highlightString(mEditableText, k, varLight);
	}
	
	private function highlightString(textField:TextField, string:String, highlightFormat:TextFormat)
	{
		var i:Int = 0;
		while ((i = textField.text.indexOf(string, i)) >= 0)
		{
			textField.setTextFormat(highlightFormat, i, i + string.length);
			i += string.length;
		}
	}
	
}


private class VariableInputNode extends Sprite
{
	static public inline var EVENT_MODIFIED:String = "varModified";
	
	public var originalName:String;
	public var originalValue:String;
	public var nameInput:TextField;
	public var valueInput:TextField;
	
	public function new(name:String, value:String, textColor:UInt, height:Float, width:Float)
	{
		super();
		
		originalName = name;
		originalValue = value;
		
		nameInput = new TextField();
		nameInput.defaultTextFormat = new TextFormat(null, 16, textColor);
		nameInput.text = name;
		nameInput.border = true;
		nameInput.borderColor = textColor;
		nameInput.type = TextFieldType.INPUT;
		nameInput.height = height;
		nameInput.width = width * 0.2;
		nameInput.addEventListener(FocusEvent.FOCUS_OUT, loseFocusListener);
		nameInput.addEventListener(KeyboardEvent.KEY_DOWN, enterListener);
		addChild(nameInput);
		
		valueInput = new TextField();
		valueInput.defaultTextFormat = new TextFormat(null, 16, textColor);
		valueInput.text = value;
		valueInput.border = true;
		valueInput.borderColor = textColor;
		valueInput.type = TextFieldType.INPUT;
		valueInput.height = height;
		valueInput.width = width * 0.78;
		valueInput.x = width * 0.22;
		valueInput.addEventListener(FocusEvent.FOCUS_OUT, loseFocusListener);
		valueInput.addEventListener(KeyboardEvent.KEY_DOWN, enterListener);
		addChild(valueInput);
	}
	
	private function enterListener(e:KeyboardEvent):Void
	{
		if (e.keyCode == Keyboard.ENTER)
			stage.focus = stage;
		if (e.keyCode == Keyboard.TAB)
			stage.focus = (stage.focus == nameInput) ? valueInput : nameInput;
	}
	
	private function loseFocusListener(e:FocusEvent):Void
	{
		if (stage.focus != nameInput && stage.focus != valueInput)
			if (valueInput.text != originalValue || nameInput.text != originalName)
				dispatchEvent(new Event(EVENT_MODIFIED));
	}
}