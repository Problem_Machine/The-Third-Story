package game.editor;

import com.problemmachine.ui.scrollwindow.ScrollWindow;
import flash.display.Sprite;
import flash.events.Event;
import flash.text.TextField;
import flash.text.TextFormat;
import game.entity.GameEntity;
import openfl.text.TextFieldType;

class EntityScriptEditor extends Sprite
{
	public var entity(default, set):GameEntity;
	public var currentlyEditing(get, never):Bool;
	
	private var mScrollWindow:ScrollWindow;
	private var mEditableText:TextField;
	
	private var mEditorWidth:Float;
	private var mEditorHeight:Float;

	public function new(width:Float, height:Float, color1:UInt, color2:UInt) 
	{
		super();
		
		mEditorWidth = width;
		mEditorHeight = height;
		
		mScrollWindow = new ScrollWindow(mEditorWidth, mEditorHeight, color1, color2, width * 0.05);
		mScrollWindow.enableHorizontalResize = mScrollWindow.enableMovement = mScrollWindow.enableVerticalResize = false;
		addChild(mScrollWindow.panel);
		
		mEditableText = new TextField();
		mEditableText.multiline = true;
		mEditableText.type = TextFieldType.INPUT;
		mEditableText.defaultTextFormat = new TextFormat(null, 20, 0xFFFFFF);
		mEditableText.addEventListener(Event.CHANGE, changeTextListener);
		mScrollWindow.addChild(mEditableText);
		
		entity = null;
	}
	
	private inline function set_entity(val:GameEntity):GameEntity
	{
		if (val != entity)
		{
			entity = val;
			mEditableText.text = entity.script;
			changeTextListener();
		}
		mEditableText.mouseEnabled = (entity != null);		
		return val;
	}
	
	private inline function get_currentlyEditing():Bool
	{
		return stage.focus == mEditableText;
	}
	
	private function changeTextListener(?e:Event):Void
	{
		mEditableText.width = Math.max(mEditorWidth * 0.9, mEditableText.textWidth);
		mEditableText.height = Math.max(mEditorHeight * 0.9, mEditableText.textHeight);
		try
		{
			Xml.parse(mEditableText.text);
			mEditableText.textColor = 0xAAAAAA;
			highlightKeywords();
			highlightVariables();
			highlightGlobalVariables();
		}
		catch (d:Dynamic)
		{
			mEditableText.textColor = 0xFF0000;
		}
	}
	
	private function highlightKeywords():Void
	{
		var keyWords:TextFormat = mEditableText.defaultTextFormat.clone();
		keyWords.color = 0xFFFFFF;
		highlightString(mEditableText, "init", keyWords);
		highlightString(mEditableText, "interact", keyWords);
		highlightString(mEditableText, "update", keyWords);
		highlightString(mEditableText, "die", keyWords);
	}
	
	@:access(game.entity.GameEntity.mLocalVariables)
	private function highlightVariables():Void
	{
		var varLight:TextFormat = mEditableText.defaultTextFormat.clone();
		varLight.color = 0x88EEEE;
		for (k in entity.mLocalVariables.keys())
			highlightString(mEditableText, k, varLight);
	}
	
	@:access(game.entity.GameEntity.sGlobalVariables)
	private function highlightGlobalVariables():Void
	{
		var varLight:TextFormat = mEditableText.defaultTextFormat.clone();
		varLight.color = 0x88FF88;
		for (k in GameEntity.sGlobalVariables.keys())
			highlightString(mEditableText, k, varLight);
	}
	
	private function highlightString(textField:TextField, string:String, highlightFormat:TextFormat)
	{
		var i:Int = 0;
		while ((i = textField.text.indexOf(string, i)) >= 0)
		{
			textField.setTextFormat(highlightFormat, i, i + string.length);
			i += string.length;
		}
	}
	
}