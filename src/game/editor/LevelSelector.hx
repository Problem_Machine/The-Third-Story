package game.editor;

import com.problemmachine.tools.text.TextTools;
import com.problemmachine.ui.text.NumberInput;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.text.TextField;

class LevelSelector extends Sprite
{
	static public inline var PREVIOUS_LEVEL:String = "LevelSelector.PreviousLevel";
	static public inline var NEXT_LEVEL:String = "LevelSelector.NextLevel";
	static public inline var SELECT_LEVEL:String = "LevelSelector.SelectLevel";
	static public inline var SAVE_LEVEL:String = "LevelSelector.SaveLevel";
	static public inline var LOAD_LEVEL:String = "LevelSelector.LoadLevel";
	
	public var level(get, set):Int;
	
	private var mLeftArrow:Sprite;
	private var mRightArrow:Sprite;
	private var mLevelInput:NumberInput;
	private var mLoadButton:Sprite;
	private var mLoadText:TextField;
	private var mSaveButton:Sprite;
	private var mSaveText:TextField;
	
	public function new(width:Float, height:Float, currentLevel:Int, color1:UInt, color2:UInt, backOpacity:Float) 
	{
		super();
		
		mLeftArrow = new Sprite();
		addChild(mLeftArrow);
		mLeftArrow.addEventListener(MouseEvent.CLICK, function(e:MouseEvent):Void
			{
				dispatchEvent(new Event(PREVIOUS_LEVEL));
			});
			
		mRightArrow = new Sprite();
		addChild(mRightArrow);
		mRightArrow.addEventListener(MouseEvent.CLICK, function(e:MouseEvent):Void
			{
				dispatchEvent(new Event(NEXT_LEVEL));
			});
			
		mLevelInput = new NumberInput(currentLevel, width * 0.2, height, null, 0, true, true, color1, color2, true);
		addChild(mLevelInput);
		mLevelInput.addEventListener(NumberInput.VALUE_CHANGE, function(e:MouseEvent):Void
			{
				dispatchEvent(new Event(SELECT_LEVEL));
			});
			
		mLoadButton = new Sprite();
		mLoadText = new TextField();
		addChild(mLoadButton);
		mLoadText.mouseEnabled = false;
		mLoadText.text = "Load";
		mLoadText.textColor = color1;
		mLoadButton.addChild(mLoadText);
		mLeftArrow.addEventListener(MouseEvent.CLICK, function(e:MouseEvent):Void
			{
				dispatchEvent(new Event(PREVIOUS_LEVEL));
			});
			
		mSaveButton = new Sprite();
		addChild(mSaveButton);
		mSaveText = new TextField();
		mSaveText.text = "Save";
		mSaveText.textColor = color1;
		mSaveText.mouseEnabled = false;
		mSaveButton.addChild(mSaveText);
		mLeftArrow.addEventListener(MouseEvent.CLICK, function(e:MouseEvent):Void
			{
				dispatchEvent(new Event(PREVIOUS_LEVEL));
			});
			
			
		mSaveButton.useHandCursor = mLoadButton.useHandCursor = mLeftArrow.useHandCursor = mRightArrow.useHandCursor = true;
		
		resize(width, height, color1, color2, 0.5);
		level = currentLevel;
	}
	
	private function get_level():Int
	{
		return Std.int(mLevelInput.value);
	}
	private function set_level(val:Int):Int
	{
		mLevelInput.value = val;
		return val;
	}
	
	public function resize(width:Float, height:Float, lineColor:UInt, bgColor:UInt, bgOpacity:Float):Void
	{
		graphics.clear();
		graphics.lineStyle(5, 0xFFFFFF, 1);
		graphics.beginFill(bgColor, bgOpacity);
		graphics.drawRect(2.5, 2.5, width - 2.5, height - 2.5);
		
		drawButtons(Math.min(width * 0.2, height - 5), lineColor, bgColor);
		positionButtons();
	}
	
	private function drawButtons(size:Float, color1:UInt, color2:UInt):Void
	{
		var l:Float = size * 0.05;
		var hl:Float = l * 0.5;
		
		mRightArrow.graphics.clear();
		mRightArrow.graphics.lineStyle(l, color1);
		mRightArrow.graphics.beginFill(color2, 0.5);
		mRightArrow.graphics.moveTo(hl, hl);
		mRightArrow.graphics.lineTo(size - hl, (size - hl) * 0.5);
		mRightArrow.graphics.lineTo(hl, size - hl);
		mRightArrow.graphics.lineTo(hl, hl);
		
		mLeftArrow.graphics.clear();
		mLeftArrow.graphics.lineStyle(l, color1);
		mLeftArrow.graphics.beginFill(color2, 0.5);
		mLeftArrow.graphics.moveTo(size - hl, hl);
		mLeftArrow.graphics.lineTo(hl, (size - hl) * 0.5);
		mLeftArrow.graphics.lineTo(size - hl, size - hl);
		mLeftArrow.graphics.lineTo(size - hl, hl);
		
		mSaveButton.graphics.clear();
		mSaveButton.graphics.lineStyle(l, color1, color2);
		mSaveButton.graphics.beginFill(color2, 0.5);
		mSaveButton.graphics.drawRect(hl, hl, size - l, size - l);
		mSaveText.width = mSaveText.height = size;
		TextTools.scaleTextToFit(mSaveText);
		
		mLoadButton.graphics.clear();
		mLoadButton.graphics.lineStyle(l, color1, color2);
		mLoadButton.graphics.beginFill(color2, 0.5);
		mLoadButton.graphics.drawRect(hl, hl, size - l, size - l);
		mLoadText.width = mLoadText.height = size;
		TextTools.scaleTextToFit(mLoadText);
	}
	
	private function positionButtons():Void
	{
		mSaveButton.x = 2.5;
		mLeftArrow.x = 2.5 + (width - 5) *  0.2;
		mLevelInput.x = 2.5 + (width - 5) *  0.4;
		mRightArrow.x = 2.5 + (width - 5) *  0.6;
		mLoadButton.x = 2.5 + (width - 5) *  0.8;
		
		mSaveButton.y = mLoadButton.y = mLeftArrow.y = mRightArrow.y = mLevelInput.y = 2.5;
	}
}