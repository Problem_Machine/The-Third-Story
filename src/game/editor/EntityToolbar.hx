package game.editor;

import com.problemmachine.tools.text.TextTools;
import com.problemmachine.ui.scrollwindow.ScrollWindow;
import flash.display.Graphics;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.text.TextField;
import flash.text.TextFormat;
import game.entity.GameEntity;

class EntityToolbar extends Sprite
{
	static public inline var DRAG_ENTITY:String = "dragEntity";
	
	private var mScrollWindow:ScrollWindow;
	private var mEntities:Array<GameEntity>;
	private var mEntityDisplayNodes:Array<EntityDisplayNode>;
	public var selected(default, null):GameEntity;
	private var mDraggingEntityNode(default, null):EntityDisplayNode;
	
	private var mWidth:Float;
	private var mHeight:Float;
	private var mTextColor:UInt;

	public function new(width:Float, height:Float, borderColor:UInt, backgroundColor:UInt, textColor:UInt) 
	{
		super();
		
		mWidth = width;
		mHeight = height;
		mTextColor = textColor;
		mScrollWindow = new ScrollWindow(width, height, borderColor, backgroundColor, height * 0.1);
		mScrollWindow.enableHorizontalResize = mScrollWindow.enableMovement = mScrollWindow.enableVerticalResize = false;
		addChild(mScrollWindow.panel);
		mEntities = [];
		mEntityDisplayNodes = [];
		selected = null;
		mDraggingEntityNode = null;
	}
	
	public function addEntity(entity:GameEntity):Void
	{
		mEntities.push(entity.clone());
		var n:EntityDisplayNode = new EntityDisplayNode(entity, mHeight * 0.9, mHeight * 0.9);
		if (mEntityDisplayNodes.length > 0)
			n.x = mEntityDisplayNodes[mEntityDisplayNodes.length - 1].x + mEntityDisplayNodes[mEntityDisplayNodes.length - 1].width;
		else
			n.x = 5;
		n.y = 5;
		n.addEventListener(MouseEvent.ROLL_OVER, nodeMouseListener);
		n.addEventListener(MouseEvent.ROLL_OUT, nodeMouseListener);
		n.addEventListener(MouseEvent.CLICK, nodeMouseListener);
		n.addEventListener(MouseEvent.MOUSE_DOWN, nodeMouseListener);
		mEntityDisplayNodes.push(n);
		mScrollWindow.addChild(n);
	}
	
	public function deleteEntity():Void
	{
		var deleteEntity:GameEntity = null;
		if (selected != null)
		{
			deleteEntity = selected;
			selected = null;
		}
		else 
		{
			for (n in mEntityDisplayNodes)
				if (n.highlit)
				{
					deleteEntity = n.entity;
					break;
				}
		}
		if (deleteEntity != null)
		{
			mEntities.remove(deleteEntity);
			var index:Int = 0;
			for (i in 0...mEntityDisplayNodes.length)
				if (mEntityDisplayNodes[i].entity == deleteEntity)
				{
					index = i;
					break;
				}
			mScrollWindow.removeChild(mEntityDisplayNodes[index]);
			var x:Float = mEntityDisplayNodes[index].x + mEntityDisplayNodes[index].width;
			mEntityDisplayNodes.splice(index, 1);
			
			for (i in index...mEntityDisplayNodes.length)
			{
				mEntityDisplayNodes[i].x = x;
				x += mEntityDisplayNodes[i].width;
			}
		}
	}
	
	public function unSelect():Void
	{
		selected = null;
		for (n in mEntityDisplayNodes)
			n.selected = false;
	}
	
	public function getDraggingEntity():GameEntity
	{
		if (mDraggingEntityNode != null)
			return mDraggingEntityNode.entity.clone();
		else
			return null;
	}
	
	private function nodeMouseListener(e:MouseEvent):Void
	{
		if (e.type == MouseEvent.ROLL_OVER)
		{
			for (n in mEntityDisplayNodes)
				n.highlit = n == e.target;
		}
		else if (e.type == MouseEvent.ROLL_OUT)
		{
			cast (e.target, EntityDisplayNode).highlit = false;
		}
		else if (e.type == MouseEvent.MOUSE_DOWN)
		{
			var n:EntityDisplayNode = cast e.target;
			for (node in mEntityDisplayNodes)
				node.selected = node.highlit = false;
			if (selected != n.entity)
			{
				n.selected = true;
				selected = n.entity;
				dispatchEvent(new Event(Event.SELECT));
			}
			mDraggingEntityNode = n.clone();
			mDraggingEntityNode.x = stage.mouseX - n.mouseX;
			mDraggingEntityNode.y = stage.mouseY - n.mouseY;
			addChild(mDraggingEntityNode);
			mDraggingEntityNode.startDrag();
			stage.addEventListener(MouseEvent.MOUSE_UP, nodeMouseListener);
		}
		else if (e.type == MouseEvent.MOUSE_UP)
		{
			stage.removeEventListener(MouseEvent.MOUSE_UP, nodeMouseListener);
			if (mouseY > mHeight)
				dispatchEvent(new Event(DRAG_ENTITY));
			removeChild(mDraggingEntityNode);
			mDraggingEntityNode.stopDrag();
			mDraggingEntityNode = null;
		}
	}
	
}

private class EntityDisplayNode extends Sprite
{
	public var entity:GameEntity;
	public var highlit(default, set):Bool;
	public var selected(default, set):Bool;
	
	public var nameTextField:TextField;
	
	private var mWidth:Float;
	private var mHeight:Float;
	
	public function new(entity:GameEntity, width:Float, height:Float)
	{
		super();
		this.entity = entity;
		
		mWidth = width;
		mHeight = height;
		
		nameTextField = new TextField();
		nameTextField.text = entity.name;
		nameTextField.defaultTextFormat = new TextFormat(null, null, 0xAAAAAA);
		nameTextField.width = mWidth * 0.9;
		nameTextField.height = mHeight * 0.9;
		nameTextField.x = mWidth * 0.05;
		nameTextField.y = mHeight * 0.05;
		nameTextField.mouseEnabled = false;
		TextTools.scaleTextToFit(nameTextField);
		addChild(nameTextField);
		draw();
	}
	
	private function set_highlit(val:Bool):Bool
	{
		if (val != highlit)
		{
			highlit = val;
			draw();
		}
		return val;
	}
	private function set_selected(val:Bool):Bool
	{
		if (val != selected)
		{
			highlit = val;
			draw();
		}
		return val;
	}
	
	public function clone():EntityDisplayNode
	{
		return new EntityDisplayNode(entity, mWidth, mHeight); 
	}
	
	private function draw():Void
	{
		graphics.clear();
		graphics.lineStyle(selected ? 7 : 3, selected ? 0xFFFFFF : 0xAAAAAA);
		graphics.beginFill(highlit ? 0x444488 : 0x333377);
		graphics.drawRoundRect(0, 0, mWidth, mHeight, mWidth * 0.05);
		nameTextField.textColor = selected ? 0xFFFFFF : 0xAAAAAA;
	}
}