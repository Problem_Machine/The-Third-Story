package game.editor;
import com.problemmachine.tools.text.TextTools;
import com.problemmachine.ui.scrollwindow.ScrollWindow;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.KeyboardEvent;
import flash.events.MouseEvent;
import flash.geom.Point;
import flash.geom.Rectangle;
import flash.text.TextField;
import game.gametile.GameTile;
import game.gametile.manager.TileManager;
import game.gametile.manager.TileReference;
import openfl.display.Tile;
import openfl.display.Tilemap;

class TileSelector extends Sprite
{
	public static inline var SELECTED_TILE:String = "TileSelector.SelectedTile";
	
	public var selected(default, set):GameTile;
	public var hFlip(get, set):Bool;
	public var vFlip(get, set):Bool;
	public var rot90(get, set):Int;
	
	private var mScrollWindow:ScrollWindow;
	private var mTilemap:Tilemap;
	private var mOverlay:Sprite;
	
	private var mHorizontalFlipButton:Sprite;
	private var mHorizontalFlipText:TextField;
	private var mVerticalFlipButton:Sprite;
	private var mVerticalFlipText:TextField;
	private var mRotateButton:Sprite;
	private var mRotationText:TextField;
	
	private var mTileReferences:Array<TileReference>;
	private var mTiles:Array<Tile>;

	public function new(width:Float, height:Float, color1:UInt, color2:UInt) 
	{
		super();
		
		mScrollWindow = new ScrollWindow(width, height, color1, color2, width * 0.05);
		mScrollWindow.enableHorizontalResize = mScrollWindow.enableMovement = mScrollWindow.enableVerticalResize = false;
		addChild(mScrollWindow.panel);
		
		mTilemap = TileManager.generateTilemap(Math.floor(width * 0.95), Math.floor(height), false);
		mScrollWindow.addChild(mTilemap);
		mScrollWindow.panel.addEventListener(MouseEvent.CLICK, mouseListener);
		
		mOverlay = new Sprite();
		mScrollWindow.addChild(mOverlay);
		
		mHorizontalFlipButton = new Sprite();
		mHorizontalFlipText = new TextField();
		addChild(mHorizontalFlipButton);
		mHorizontalFlipButton.x = -80;
		mHorizontalFlipText.width = 80;
		mHorizontalFlipText.height = 20;
		TextTools.scaleTextToFit(mHorizontalFlipText);
		mHorizontalFlipText.mouseEnabled = false;
		mHorizontalFlipText.text = "hFlip";
		mHorizontalFlipButton.useHandCursor = true;
		mHorizontalFlipButton.addChild(mHorizontalFlipText);
		mHorizontalFlipButton.addEventListener(MouseEvent.CLICK, buttonsListener);
		
		mVerticalFlipButton = new Sprite();
		mVerticalFlipText = new TextField();
		addChild(mVerticalFlipButton);
		mVerticalFlipButton.x = -80;
		mVerticalFlipButton.y = 20;
		mVerticalFlipText.width = 80;
		mVerticalFlipText.height = 20;
		TextTools.scaleTextToFit(mVerticalFlipText);
		mVerticalFlipText.mouseEnabled = false;
		mVerticalFlipText.text = "vFlip";
		mVerticalFlipButton.useHandCursor = true;
		mVerticalFlipButton.addChild(mVerticalFlipText);
		mVerticalFlipButton.addEventListener(MouseEvent.CLICK, buttonsListener);
		
		mRotateButton = new Sprite();
		mRotationText = new TextField();
		addChild(mRotateButton);
		mRotateButton.x = -80;
		mRotateButton.y = 40;
		mRotationText.width = 80;
		mRotationText.height = 20;
		TextTools.scaleTextToFit(mRotationText);
		mRotationText.mouseEnabled = false;
		mRotationText.textColor = 0x33FF33;
		mRotateButton.useHandCursor = true;
		mRotateButton.addChild(mRotationText);
		mRotateButton.addEventListener(MouseEvent.CLICK, buttonsListener);		
		mRotateButton.graphics.clear();
		mRotateButton.graphics.lineStyle(5, 0x33FF33, 1);
		mRotateButton.graphics.beginFill(0x113311);
		mRotateButton.graphics.drawRect(0, 0, 80, 20);
		
		mTileReferences = [];
		mTiles = [];
		Reflect.setField(this, "selected", new GameTile(Game.DEBUG_OPEN));
	}
	
	private inline function get_hFlip():Bool
	{	return selected.hFlip;	}
	private inline function set_hFlip(val:Bool):Bool
	{
		if (val != selected.hFlip)
		{
			selected.hFlip = val;
			arrangeTiles();
			updateOverlay();
			dispatchEvent(new Event(SELECTED_TILE));
		}
		return val;
	}
	private inline function get_vFlip():Bool
	{	return selected.vFlip;	}
	private inline function set_vFlip(val:Bool):Bool
	{
		if (val != selected.vFlip)
		{
			selected.vFlip = val;
			arrangeTiles();
			updateOverlay();
			dispatchEvent(new Event(SELECTED_TILE));
		}
		return val;
	}
	
	private inline function get_rot90():Int
	{	return selected.rot90;	}
	private inline function set_rot90(val:Int):Int
	{
		if (val != selected.rot90)
		{
			selected.rot90 = val;
			arrangeTiles();
			updateOverlay();
			dispatchEvent(new Event(SELECTED_TILE));
		}
		return val;
	}
	
	private inline function set_selected(val:GameTile):GameTile
	{
		if (!val.equals(selected))
		{
			selected = val.clone();
			arrangeTiles();
			updateOverlay();
			dispatchEvent(new Event(SELECTED_TILE));
		}
		return val;
	}
	
	private function buttonsListener(e:MouseEvent):Void
	{
		if (e.target == mHorizontalFlipButton)
			hFlip = !hFlip;
		else if (e.target == mVerticalFlipButton)
			vFlip = !vFlip;
		else
			++rot90;
	}
	
	public function keyboardControl(e:KeyboardEvent):Void
	{
		
	}
	
	private function mouseListener(e:MouseEvent):Void
	{
		if (e.type == MouseEvent.CLICK)
		{
			var tileIndex:Int = -1;
			var position:Point = new Point(mTilemap.mouseX, mTilemap.mouseY);
			for (i in 0...mTiles.length)
			{
				var tile:Tile = mTiles[i];
				var l:Float = tile.x - (Game.TILE_SIZE * 0.25);
				var r:Float = tile.x + (Game.TILE_SIZE * 0.25);
				var t:Float = tile.y - (Game.TILE_SIZE * 0.25);
				var b:Float = tile.y + (Game.TILE_SIZE * 0.25);
				if (l <= position.x && r > position.x && t <= position.y && b > position.y)
				{
					tileIndex = i;
					break;
				}
			}
			if (tileIndex >= 0)
				selected.id = tileIndex;
		}
	}
	
	public function addTileset(tr:TileReference):Void
	{
		if (mTileReferences.indexOf(tr) >= 0)
			return;
		mTileReferences.push(tr);
		for (id in tr.tileIDs)
			mTiles.push(new Tile(id, 0, 0, 0.5, 0.5, 0, Game.TILE_SIZE * 0.5, Game.TILE_SIZE * 0.5));
		arrangeTiles(tr.bitmapRect);
	}
	
	private function arrangeTiles(addRect:Rectangle = null):Void
	{
		if (addRect != null)
		{
			mScrollWindow.removeChild(mTilemap);
			mScrollWindow.removeChild(mOverlay);
			mTilemap = new Tilemap(Math.ceil(Math.max(mTilemap.width, addRect.width * 0.5)), 
				Math.ceil(mTilemap.height + addRect.height * 0.5), mTilemap.tileset, false);
			mScrollWindow.addChild(mTilemap);
			mScrollWindow.addChild(mOverlay);
			for (t in mTiles)
				mTilemap.addTile(t);
		}
		
		var targetY:Float = 0;
		for (ref in mTileReferences)
		{
			var i:Int = 0;
			for (ti in 0...mTiles.length)
				if (mTiles[ti].id == ref.tileIDs[0])
				{
					i = ti;
					break;
				}
				
			var w:Int = Math.floor(ref.bitmapRect.width / Game.TILE_SIZE);
			var h:Int = Math.floor(ref.bitmapRect.height / Game.TILE_SIZE);
			
			var maxY:Float = 0;
			for (y in 0...h)
				for (x in 0...w)
				{
					var xx:Int = ((hFlip || rot90 == 2 || rot90 == 3) && !(hFlip && (rot90 == 2 || rot90 == 3))) ? w - 1 - x : x;
					var yy:Int = ((vFlip || rot90 == 1 || rot90 == 2) && !(vFlip && (rot90 == 1 || rot90 == 2))) ? h - 1 - y : y;
					
					if (rot90 == 1 || rot90 == 3)
					{
						var tmp = yy;
						yy = xx;
						xx = tmp;
					}
					var t:Tile = mTiles[i + y * w + x];
					t.scaleX = hFlip ? -0.5 : 0.5;
					t.scaleY = vFlip ? -0.5 : 0.5;
					t.rotation = rot90 * 90;
					t.x = (xx + 0.5) * Game.TILE_SIZE * 0.5;
					t.y = ((yy + 0.5) * Game.TILE_SIZE * 0.5) + targetY;
					maxY = Math.max(maxY, t.y - Game.TILE_SIZE * 0.25);
				}
			targetY = maxY + Game.TILE_SIZE * 0.5;
		}
	}
	
	private function getReferenceFromID(id:Int):TileReference
	{
		for (tr in mTileReferences)
			if (tr.tileIDs.indexOf(id) >= 0)
				return tr;
		return null;
	}
	
	public function updateOverlay():Void
	{
		updateButtons();
		var ox:Float = -Game.TILE_SIZE * 0.25;
		var oy:Float = -Game.TILE_SIZE * 0.25;
		mOverlay.graphics.clear();
		mOverlay.graphics.lineStyle(4, 0x0055AA, 0.7);
		
		var tileIndex:Int = -1;
		for (i in 0...mTiles.length)
			if (mTiles[i].id == selected.id)
			{
				tileIndex = i;
				break;
			}
		
		mOverlay.graphics.drawRect(mTiles[tileIndex].x + ox, mTiles[tileIndex].y + oy, Game.TILE_SIZE * 0.5, Game.TILE_SIZE * 0.5);
		var ref:TileReference = getReferenceFromID(selected.id);
		var offset:Int = ref.tileIDs.indexOf(selected.id);
		var rect:Rectangle = new Rectangle(mTiles[tileIndex].x, mTiles[tileIndex].y, ref.bitmapRect.width * 0.5, ref.bitmapRect.height * 0.5);
		if (rot90 == 1 || rot90 == 3)
		{
			var tmp:Float = rect.width;
			rect.width = rect.height;
			rect.height = tmp;
		}
		for (i in 0...ref.tileIDs.length)
		{
			var t:Tile = mTiles[tileIndex - offset + i];
			rect.x = Math.min(rect.x, t.x);
			rect.y = Math.min(rect.y, t.y);
		}
		mOverlay.graphics.drawRect(rect.x + ox, rect.y + oy, rect.width, rect.height);
		mOverlay.graphics.lineStyle();
			
		var position:Point = new Point(mTilemap.mouseX, mTilemap.mouseY);
		for (i in 0...mTiles.length)
		{
			var tile:Tile = mTiles[i];
			var l:Float = tile.x - (Game.TILE_SIZE * 0.25);
			var r:Float = tile.x + (Game.TILE_SIZE * 0.25);
			var t:Float = tile.y - (Game.TILE_SIZE * 0.25);
			var b:Float = tile.y + (Game.TILE_SIZE * 0.25);
			if (l <= position.x && r > position.x && t <= position.y && b > position.y)
			{
				tileIndex = i;
				break;
			}
		}
		if (tileIndex >= 0)
		{
			mOverlay.graphics.lineStyle();
			mOverlay.graphics.beginFill(0xFFFFFF, 0.5);
			mOverlay.graphics.drawRect(mTiles[tileIndex].x + ox, mTiles[tileIndex].y + oy, Game.TILE_SIZE * 0.5, Game.TILE_SIZE * 0.5);
			
			mOverlay.graphics.beginFill(0xFFFFFF, 0.2);
			ref = getReferenceFromID(mTiles[tileIndex].id);
			offset = ref.tileIDs.indexOf(mTiles[tileIndex].id);
			rect = new Rectangle(mTiles[tileIndex].x, mTiles[tileIndex].y, ref.bitmapRect.width * 0.5, ref.bitmapRect.height * 0.5);
			if (rot90 == 1 || rot90 == 3)
			{
				var tmp:Float = rect.width;
				rect.width = rect.height;
				rect.height = tmp;
			}
			for (i in 0...ref.tileIDs.length)
			{
				var t:Tile = mTiles[tileIndex - offset + i];
				rect.x = Math.min(rect.x, t.x);
				rect.y = Math.min(rect.y, t.y);
			}
			mOverlay.graphics.drawRect(rect.x + ox, rect.y + oy, rect.width, rect.height);
		}
	}
	
	private function updateButtons():Void
	{
		mHorizontalFlipButton.graphics.clear();
		mHorizontalFlipButton.graphics.lineStyle(5, hFlip ? 0xFF0000 : 0xAA3333, 1);
		mHorizontalFlipButton.graphics.beginFill(0x331111);
		mHorizontalFlipButton.graphics.drawRect(0, 0, 80, 20);
		mHorizontalFlipText.textColor = hFlip ? 0xFF0000 : 0xAA3333;
		
		mVerticalFlipButton.graphics.clear();
		mVerticalFlipButton.graphics.lineStyle(5, vFlip ? 0x0000FF : 0x3333AA, 1);
		mVerticalFlipButton.graphics.beginFill(0x111133);
		mVerticalFlipButton.graphics.drawRect(0, 0, 80, 20);
		mVerticalFlipText.textColor = vFlip ? 0x0000FF : 0x3333AA;
		
		mRotationText.text = rot90 == 0 ? "0" : Std.string(rot90 * 90);
	}
}